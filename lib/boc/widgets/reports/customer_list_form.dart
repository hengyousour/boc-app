import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:number_paginator/number_paginator.dart';
import 'package:provider/provider.dart';
import '../../models/reports/customer-list/customer_list_form_report_model.dart';
import '../../models/reports/customer-list/customer_list_report_model.dart';
import '/boc/widgets/components/report_content_header.dart';
import '/core/widgets/share_widget.dart';
import '../../../core/utils/core_convert_date_time.dart';
import '../../models/reports/report_filter_model.dart';
import '../../providers/reports/customer_list_report_provider.dart';
import '../../utils/custom_form_builder_style.dart';
import '../../utils/static_options.dart';
import '../components/report_filter.dart';
import '../components/report_header.dart';

class CustomerListForm extends StatelessWidget {
  const CustomerListForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    CustomerListReportProvider _readProvider =
        context.read<CustomerListReportProvider>();
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Theme(
                data: theme.copyWith(
                    colorScheme: ColorScheme.light(
                      primary: theme.colorScheme.primary,
                      onPrimary: theme.primaryColor,
                      onSurface: theme.iconTheme.color!.withOpacity(0.7),
                    ),
                    appBarTheme: theme.appBarTheme.copyWith(
                      iconTheme: IconThemeData(color: theme.primaryColor),
                    )),
                child: Expanded(
                  child: FormBuilderDateRangePicker(
                    initialValue: DateTimeRange(
                        start: DateTime.now(), end: DateTime.now()),
                    name: 'dateRange',
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    decoration: fbTextFieldStyle(
                      label: "From Date - To Date",
                      theme: theme,
                    ),
                    firstDate: DateTime(1970),
                    lastDate: DateTime(2100),
                    format: DateFormat('dd-MM-yyyy'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(),
                    ]),
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: FormBuilderDropdown(
                  name: 'customerStatus',
                  decoration: fbSelectOptionFieldStyle(
                    label: "Status",
                    theme: theme,
                  ),
                  initialValue: 'Active',
                  allowClear: true,
                  hint: const Text('Select Customer Status'),
                  // validator: FormBuilderValidators.compose(
                  //     [FormBuilderValidators.required(context)]),
                  items: StaticOptions.customerStatusOption
                      .map((opt) => DropdownMenuItem(
                            value: opt.value,
                            child: Text(opt.label),
                          ))
                      .toList(),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20.0),
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: [
                ShareWidget(
                  shareReady: (value) {
                    context
                        .read<CustomerListReportProvider>()
                        .setEnablePagination(value: value);
                  },
                  child: Column(
                    children: [
                      Consumer<CustomerListReportProvider>(
                        builder: (_, state, child) => ReportHeader(
                          khTitle: 'របាយការណ៍អតិថិជន',
                          enTitle: 'Customer List Report',
                          fromDate: state.customerListFilter.fromDate != null
                              ? CoreConvertDateTime.formatTimeStampToString(
                                  state.customerListFilter.fromDate!, false)
                              : null,
                          toDate: state.customerListFilter.toDate != null
                              ? CoreConvertDateTime.formatTimeStampToString(
                                  state.customerListFilter.toDate!, false)
                              : null,
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      Consumer<CustomerListReportProvider>(
                        builder: (_, state, child) => ReportFilter(
                            alignment: WrapAlignment.end,
                            reportFilterList: [
                              ReportFilterModel(
                                  label: 'Status',
                                  value: state.customerListFilter.status),
                            ]),
                      ),
                      const SizedBox(height: 10.0),
                      StreamBuilder(
                        stream: context
                            .read<CustomerListReportProvider>()
                            .streamCustomerList,
                        builder: ((context,
                            AsyncSnapshot<List<CustomerListReportModel>>
                                snapshot) {
                          if (!snapshot.hasData) {
                            return Table(
                              defaultVerticalAlignment:
                                  TableCellVerticalAlignment.middle,
                              columnWidths: const {
                                0: FixedColumnWidth(38.0),
                              },
                              children: [
                                tableHeader(theme),
                              ],
                            );
                          }
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return const CircularProgressIndicator();
                            default:
                              if (snapshot.hasError) {
                                return Text(snapshot.error.toString());
                              } else {
                                return Column(
                                  children: [
                                    Table(
                                      defaultVerticalAlignment:
                                          TableCellVerticalAlignment.middle,
                                      columnWidths: const {
                                        0: FixedColumnWidth(38.0),
                                      },
                                      children: [
                                        tableHeader(theme),
                                        for (int item = 0;
                                            item < snapshot.data!.length;
                                            item++)
                                          TableRow(
                                              decoration: BoxDecoration(
                                                color: theme.primaryColor
                                                    .withOpacity(0.5),
                                                border: Border(
                                                  bottom: BorderSide(
                                                      color: theme
                                                          .scaffoldBackgroundColor),
                                                ),
                                              ),
                                              children: [
                                                ReportContentHeader(
                                                  text: '${item + 1}',
                                                ),
                                                ReportContentHeader(
                                                  text:
                                                      snapshot.data![item].name,
                                                ),
                                                ReportContentHeader(
                                                  text: snapshot
                                                      .data![item].gender,
                                                ),
                                                ReportContentHeader(
                                                  text: CoreConvertDateTime
                                                      .formatTimeStampToString(
                                                          snapshot
                                                              .data![item].date,
                                                          false),
                                                ),
                                                ReportContentHeader(
                                                  text:
                                                      '${snapshot.data![item].age}',
                                                ),
                                                ReportContentHeader(
                                                  text: snapshot
                                                      .data![item].status,
                                                ),
                                              ])
                                      ],
                                    ),
                                    Selector<CustomerListReportProvider, bool>(
                                      selector: (_, state) =>
                                          state.enablePagination,
                                      builder: (_, enablePagination, child) =>
                                          enablePagination &&
                                                  snapshot.data!.isNotEmpty
                                              ? NumberPaginator(
                                                  numberPages:
                                                      _readProvider.totalPage,
                                                  onPageChange: (int index) {
                                                    int _currentPage =
                                                        index += 1;
                                                    DateTimeRange _date =
                                                        FormBuilder.of(context)!
                                                            .fields[
                                                                'dateRange']!
                                                            .value;
                                                    DateTime startOfDay =
                                                        _date.start.startOfDay;
                                                    DateTime endOfDay =
                                                        _date.end.endOfDay;
                                                    String status = FormBuilder
                                                            .of(context)!
                                                        .fields[
                                                            'customerStatus']!
                                                        .value;

                                                    CustomerListFormReportModel
                                                        data =
                                                        CustomerListFormReportModel(
                                                            fromDate:
                                                                startOfDay,
                                                            toDate: endOfDay,
                                                            status: status,
                                                            page: _currentPage);
                                                    _readProvider.submit(
                                                        formDoc: data);
                                                  },
                                                  buttonSelectedBackgroundColor:
                                                      theme.colorScheme.primary,
                                                  buttonUnselectedForegroundColor:
                                                      theme.colorScheme.primary,
                                                )
                                              : const SizedBox(),
                                    ),
                                  ],
                                );
                              }
                          }
                        }),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  TableRow tableHeader(ThemeData theme) {
    return TableRow(
      decoration: BoxDecoration(
        color: theme.primaryColor,
        border: Border(
          bottom: BorderSide(color: theme.scaffoldBackgroundColor),
        ),
      ),
      children: const [
        ReportContentHeader(
          text: 'N\u00BA',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Name',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Gender',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Date',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Age',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Status',
          bold: true,
        ),
      ],
    );
  }
}
