import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../models/reports/biorc/biorc_data_content_report_model.dart';
import '../../models/reports/biorc/biorc_data_report_model.dart';
import '../../models/reports/biorc/biorc_report_model.dart';
import '../../providers/reports/biorc_report_provider.dart';
import '/boc/widgets/components/report_content_header.dart';
import '/core/widgets/share_widget.dart';
import '../../../core/utils/core_convert_date_time.dart';
import '../../utils/custom_form_builder_style.dart';
import '../components/report_header.dart';

class BiorcForm extends StatelessWidget {
  const BiorcForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: FormBuilderDateRangePicker(
                  initialValue:
                      DateTimeRange(start: DateTime.now(), end: DateTime.now()),
                  name: 'dateRange',
                  initialEntryMode: DatePickerEntryMode.calendarOnly,
                  decoration: fbTextFieldStyle(
                    label: "From Date - To Date",
                    theme: theme,
                  ),
                  firstDate: DateTime(1970),
                  lastDate: DateTime(2100),
                  format: DateFormat('dd-MM-yyyy'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                  ]),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20.0),
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: [
                ShareWidget(
                  shareReady: (value) {},
                  child: Column(
                    children: [
                      Consumer<BiorcReportProvider>(
                        builder: (_, state, child) => ReportHeader(
                          khTitle: '',
                          enTitle: 'Brief Information Of Referral Case Report',
                          fromDate: state.biorcFilter.fromDate != null
                              ? CoreConvertDateTime.formatTimeStampToString(
                                  state.biorcFilter.fromDate!, false)
                              : null,
                          toDate: state.biorcFilter.toDate != null
                              ? CoreConvertDateTime.formatTimeStampToString(
                                  state.biorcFilter.toDate!, false)
                              : null,
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      StreamBuilder(
                        stream: context.read<BiorcReportProvider>().streamBiorc,
                        builder: ((context,
                            AsyncSnapshot<BiorcReportModel> snapshot) {
                          if (!snapshot.hasData) {
                            return Table(
                              defaultVerticalAlignment:
                                  TableCellVerticalAlignment.middle,
                              children: [
                                tableHeader(theme),
                                tableHeader2(theme),
                              ],
                            );
                          }
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return const CircularProgressIndicator();
                            default:
                              if (snapshot.hasError) {
                                return Text(snapshot.error.toString());
                              } else {
                                List<BiorcDataReportModel> _tempDataContent =
                                    snapshot.data!.data;
                                List<BiorcDataContentReportModel> _dataContent =
                                    [
                                  //Refer By Consult
                                  BiorcDataContentReportModel(
                                      title: 'Refer By Consult',
                                      rfcTotal:
                                          _tempDataContent[0].consRFCTotal,
                                      rfcMale: _tempDataContent[0].consRFCMale,
                                      rfcFemale:
                                          _tempDataContent[0].consRFCFemale,
                                      aosTotal:
                                          _tempDataContent[0].consAOSTotal,
                                      aosMale: _tempDataContent[0].consAOSMale,
                                      aosFemale:
                                          _tempDataContent[0].consAOSFemale,
                                      acceptedTotal:
                                          _tempDataContent[0].consAcceptedTotal,
                                      acceptedMale:
                                          _tempDataContent[0].consAcceptedMale,
                                      acceptedFemale: _tempDataContent[0]
                                          .consAcceptedFemale,
                                      notAcceptedTotal: _tempDataContent[0]
                                          .consNotAcceptedTotal,
                                      notAcceptedMale: _tempDataContent[0]
                                          .consNotAcceptedMale,
                                      notAcceptedFemale: _tempDataContent[0]
                                          .consNotAcceptedFemale,
                                      missingTotal:
                                          _tempDataContent[0].consMissingTotal,
                                      missingMale:
                                          _tempDataContent[0].consMissingMale,
                                      missingFemale: _tempDataContent[0]
                                          .consMissingFemale),
                                  //Refer After Surgery
                                  BiorcDataContentReportModel(
                                      title: 'Refer By Surgery',
                                      rfcTotal: _tempDataContent[0].surRFCTotal,
                                      rfcMale: _tempDataContent[0].surRFCMale,
                                      rfcFemale:
                                          _tempDataContent[0].surRFCFemale,
                                      aosTotal: _tempDataContent[0].surAOSTotal,
                                      aosMale: _tempDataContent[0].surAOSMale,
                                      aosFemale:
                                          _tempDataContent[0].surAOSFemale,
                                      acceptedTotal:
                                          _tempDataContent[0].surAcceptedTotal,
                                      acceptedMale:
                                          _tempDataContent[0].surAcceptedMale,
                                      acceptedFemale:
                                          _tempDataContent[0].surAcceptedFemale,
                                      notAcceptedTotal: _tempDataContent[0]
                                          .surNotAcceptedTotal,
                                      notAcceptedMale: _tempDataContent[0]
                                          .surNotAcceptedMale,
                                      notAcceptedFemale: _tempDataContent[0]
                                          .surNotAcceptedFemale,
                                      missingTotal:
                                          _tempDataContent[0].surMissingTotal,
                                      missingMale:
                                          _tempDataContent[0].surMissingMale,
                                      missingFemale:
                                          _tempDataContent[0].surMissingFemale),
                                  //Refer Vai Field
                                  BiorcDataContentReportModel(
                                      title: 'Refer Vai Field',
                                      rfcTotal:
                                          _tempDataContent[0].fieldRFCTotal,
                                      rfcMale: _tempDataContent[0].fieldRFCMale,
                                      rfcFemale:
                                          _tempDataContent[0].fieldRFCFemale,
                                      aosTotal:
                                          _tempDataContent[0].fieldAOSTotal,
                                      aosMale: _tempDataContent[0].fieldAOSMale,
                                      aosFemale:
                                          _tempDataContent[0].fieldAOSFemale,
                                      acceptedTotal: _tempDataContent[0]
                                          .fieldAcceptedTotal,
                                      acceptedMale:
                                          _tempDataContent[0].fieldAcceptedMale,
                                      acceptedFemale: _tempDataContent[0]
                                          .fieldAcceptedFemale,
                                      notAcceptedTotal: _tempDataContent[0]
                                          .fieldNotAcceptedTotal,
                                      notAcceptedMale: _tempDataContent[0]
                                          .fieldNotAcceptedMale,
                                      notAcceptedFemale: _tempDataContent[0]
                                          .fieldNotAcceptedFemale,
                                      missingTotal:
                                          _tempDataContent[0].fieldMissingTotal,
                                      missingMale:
                                          _tempDataContent[0].fieldMissingMale,
                                      missingFemale: _tempDataContent[0]
                                          .fieldMissingFemale)
                                ];
                                return Table(
                                  defaultVerticalAlignment:
                                      TableCellVerticalAlignment.middle,
                                  children: [
                                    tableHeader(theme),
                                    tableHeader2(theme),
                                    for (int item = 0;
                                        item < _dataContent.length;
                                        item++)
                                      dynamicTableContent(
                                          data: _dataContent[item],
                                          theme: theme),
                                    tableStaticContent(
                                        title: 'Direct', theme: theme),
                                    tableStaticContent(
                                        title: 'Total',
                                        setAllBoxBg: true,
                                        theme: theme)
                                  ],
                                );
                              }
                          }
                        }),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  TableRow tableHeader(ThemeData theme) {
    return TableRow(
      decoration: BoxDecoration(
        color: theme.colorScheme.primary,
        // border: Border(
        //   bottom: BorderSide(color: theme.scaffoldBackgroundColor),
        // ),
      ),
      children: [
        const ReportContentHeader(
          text: '',
          bold: true,
          fullBorder: true,
        ),
        ReportContentHeader(
          text: 'Number Of Refraction',
          textColor: theme.primaryColor,
          alignment: Alignment.center,
          bold: true,
          fullBorder: true,
        ),
        ReportContentHeader(
          text: 'Refer From Clinic',
          textColor: theme.primaryColor,
          alignment: Alignment.center,
          bold: true,
          fullBorder: true,
        ),
        ReportContentHeader(
          text: 'Attended Optical Shop',
          textColor: theme.primaryColor,
          alignment: Alignment.center,
          bold: true,
          fullBorder: true,
        ),
        ReportContentHeader(
          text: 'Accepted',
          textColor: theme.primaryColor,
          alignment: Alignment.center,
          bold: true,
          fullBorder: true,
        ),
        ReportContentHeader(
          text: 'Not Accepted',
          textColor: theme.primaryColor,
          alignment: Alignment.center,
          bold: true,
          fullBorder: true,
        ),
        ReportContentHeader(
          text: 'Missing',
          textColor: theme.primaryColor,
          alignment: Alignment.center,
          bold: true,
          fullBorder: true,
        ),
      ],
    );
  }

  TableRow tableHeader2(ThemeData theme) {
    return TableRow(
      decoration: BoxDecoration(
        color: theme.colorScheme.primary,
        // border: Border(
        //   bottom: BorderSide(color: theme.scaffoldBackgroundColor),
        // ),
      ),
      children: [
        const SizedBox(
          height: 40.0,
          child: ReportContentHeader(
            text: '',
            bold: true,
            fullBorder: true,
          ),
        ),
        for (int item = 0; item < 6; item++)
          SizedBox(
            height: 40.0,
            child: Row(
              children: [
                Expanded(
                  child: ReportContentHeader(
                    text: 'M',
                    bold: true,
                    alignment: Alignment.center,
                    textColor: theme.primaryColor,
                    padding: const EdgeInsets.all(0.0),
                    fullBorder: true,
                  ),
                ),
                Expanded(
                    child: ReportContentHeader(
                  text: 'F',
                  bold: true,
                  alignment: Alignment.center,
                  textColor: theme.primaryColor,
                  padding: const EdgeInsets.all(0.0),
                  fullBorder: true,
                )),
                Expanded(
                  child: ReportContentHeader(
                    text: 'Total',
                    alignment: Alignment.center,
                    textColor: theme.primaryColor,
                    padding: const EdgeInsets.all(0.0),
                    bold: true,
                    fullBorder: true,
                  ),
                )
              ],
            ),
          )
      ],
    );
  }

  TableRow dynamicTableContent(
      {required BiorcDataContentReportModel data, required ThemeData theme}) {
    double _boxHeight = 40.0;
    return TableRow(
      decoration: BoxDecoration(
        color: theme.primaryColor.withOpacity(0.5),
        // border: Border.all(
        //   color: theme.iconTheme.color!,
        // ),
      ),
      children: [
        ReportContentHeader(
          text: data.title,
          bold: true,
          fullBorder: true,
        ),
        SizedBox(
          height: _boxHeight,
          child: Row(
            children: [
              const Expanded(
                child: ReportContentHeader(
                  text: '',
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(0.0),
                  fullBorder: true,
                ),
              ),
              const Expanded(
                  child: ReportContentHeader(
                text: '',
                alignment: Alignment.center,
                padding: EdgeInsets.all(0.0),
                fullBorder: true,
              )),
              Expanded(
                child: ReportContentHeader(
                  text: '',
                  textColor: theme.primaryColor,
                  alignment: Alignment.center,
                  bgColor: theme.colorScheme.primary,
                  padding: const EdgeInsets.all(0.0),
                  bold: true,
                  fullBorder: true,
                ),
              ),
            ],
          ),
        ),
        //RFC
        SizedBox(
          height: _boxHeight,
          child: Row(
            children: [
              Expanded(
                child: ReportContentHeader(
                  text: data.rfcMale != 0 ? '${data.rfcMale}' : '-',
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(0.0),
                  fullBorder: true,
                ),
              ),
              Expanded(
                  child: ReportContentHeader(
                text: data.rfcFemale != 0 ? '${data.rfcFemale}' : '-',
                alignment: Alignment.center,
                padding: const EdgeInsets.all(0.0),
                fullBorder: true,
              )),
              Expanded(
                child: ReportContentHeader(
                  text: data.rfcTotal != 0 ? '${data.rfcTotal}' : '-',
                  textColor: theme.primaryColor,
                  alignment: Alignment.center,
                  bgColor: theme.colorScheme.primary,
                  padding: const EdgeInsets.all(0.0),
                  bold: true,
                  fullBorder: true,
                ),
              ),
            ],
          ),
        ),
        //AOS
        SizedBox(
          height: _boxHeight,
          child: Row(
            children: [
              Expanded(
                child: ReportContentHeader(
                  text: data.aosMale != 0 ? '${data.aosMale}' : '-',
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(0.0),
                  fullBorder: true,
                ),
              ),
              Expanded(
                  child: ReportContentHeader(
                text: data.aosFemale != 0 ? '${data.aosFemale}' : '-',
                alignment: Alignment.center,
                padding: const EdgeInsets.all(0.0),
                fullBorder: true,
              )),
              Expanded(
                child: ReportContentHeader(
                  text: data.aosTotal != 0 ? '${data.aosTotal}' : '-',
                  textColor: theme.primaryColor,
                  alignment: Alignment.center,
                  bgColor: theme.colorScheme.primary,
                  padding: const EdgeInsets.all(0.0),
                  bold: true,
                  fullBorder: true,
                ),
              ),
            ],
          ),
        ),
        //Accepted
        SizedBox(
          height: _boxHeight,
          child: Row(
            children: [
              Expanded(
                child: ReportContentHeader(
                  text: data.acceptedMale != 0 ? '${data.acceptedMale}' : '-',
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(0.0),
                  fullBorder: true,
                ),
              ),
              Expanded(
                  child: ReportContentHeader(
                text: data.acceptedFemale != 0 ? '${data.acceptedFemale}' : '-',
                alignment: Alignment.center,
                padding: const EdgeInsets.all(0.0),
                fullBorder: true,
              )),
              Expanded(
                child: ReportContentHeader(
                  text: data.acceptedTotal != 0 ? '${data.acceptedTotal}' : '-',
                  textColor: theme.primaryColor,
                  alignment: Alignment.center,
                  bgColor: theme.colorScheme.primary,
                  padding: const EdgeInsets.all(0.0),
                  bold: true,
                  fullBorder: true,
                ),
              ),
            ],
          ),
        ),
        //Not Accepted
        SizedBox(
          height: _boxHeight,
          child: Row(
            children: [
              Expanded(
                child: ReportContentHeader(
                  text: data.notAcceptedMale != 0
                      ? '${data.notAcceptedMale}'
                      : '-',
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(0.0),
                  fullBorder: true,
                ),
              ),
              Expanded(
                  child: ReportContentHeader(
                text: data.notAcceptedFemale != 0
                    ? '${data.notAcceptedFemale}'
                    : '-',
                alignment: Alignment.center,
                padding: const EdgeInsets.all(0.0),
                fullBorder: true,
              )),
              Expanded(
                child: ReportContentHeader(
                  text: data.notAcceptedTotal != 0
                      ? '${data.notAcceptedTotal}'
                      : '-',
                  textColor: theme.primaryColor,
                  alignment: Alignment.center,
                  bgColor: theme.colorScheme.primary,
                  padding: const EdgeInsets.all(0.0),
                  bold: true,
                  fullBorder: true,
                ),
              ),
            ],
          ),
        ),
        //Missing
        SizedBox(
          height: _boxHeight,
          child: Row(
            children: [
              Expanded(
                child: ReportContentHeader(
                  text: data.missingMale != 0 ? '${data.missingMale}' : '-',
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(0.0),
                  fullBorder: true,
                ),
              ),
              Expanded(
                  child: ReportContentHeader(
                text: data.missingFemale != 0 ? '${data.missingFemale}' : '-',
                alignment: Alignment.center,
                padding: const EdgeInsets.all(0.0),
                fullBorder: true,
              )),
              Expanded(
                child: ReportContentHeader(
                  text: data.missingTotal != 0 ? '${data.missingTotal}' : '-',
                  textColor: theme.primaryColor,
                  alignment: Alignment.center,
                  bgColor: theme.colorScheme.primary,
                  padding: const EdgeInsets.all(0.0),
                  fullBorder: true,
                  bold: true,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  TableRow tableStaticContent(
      {required String title,
      bool setAllBoxBg = false,
      required ThemeData theme}) {
    return TableRow(
      decoration: BoxDecoration(
        color: theme.primaryColor.withOpacity(0.5),
        // border: Border(
        //   bottom: BorderSide(color: theme.scaffoldBackgroundColor),
        // ),
      ),
      children: [
        ReportContentHeader(
          text: title,
          bold: true,
          fullBorder: true,
        ),
        for (int item = 0; item < 6; item++)
          SizedBox(
            height: 40.0,
            child: Row(
              children: [
                Expanded(
                  child: ReportContentHeader(
                    text: '',
                    bold: true,
                    alignment: Alignment.center,
                    textColor: theme.primaryColor,
                    bgColor: setAllBoxBg ? theme.colorScheme.primary : null,
                    padding: const EdgeInsets.all(0.0),
                    fullBorder: true,
                  ),
                ),
                Expanded(
                    child: ReportContentHeader(
                  text: '',
                  bold: true,
                  alignment: Alignment.center,
                  textColor: theme.primaryColor,
                  bgColor: setAllBoxBg ? theme.colorScheme.primary : null,
                  padding: const EdgeInsets.all(0.0),
                  fullBorder: true,
                )),
                Expanded(
                  child: ReportContentHeader(
                    text: '',
                    alignment: Alignment.center,
                    textColor: theme.primaryColor,
                    bgColor: theme.colorScheme.primary,
                    padding: const EdgeInsets.all(0.0),
                    bold: true,
                    fullBorder: true,
                  ),
                )
              ],
            ),
          )
      ],
    );
  }
}
