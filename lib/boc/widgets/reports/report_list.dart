import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../core/providers/connection_provider.dart';
import '../../../core/utils/alert.dart';
import '../../models/option_model.dart';
import '../../utils/constants.dart';

class ReportList extends StatelessWidget {
  final OptionModel report;
  const ReportList({Key? key, required this.report}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Card(
      color: theme.primaryColor,
      elevation: 0.0,
      child: ListTile(
        onTap: () {
          // check is app connect to server or not
          bool isConnected = context.read<ConnectionProvider>().isConnected;
          if (!isConnected) {
            return Alert().show(
                message: CommonMsg.noConnection,
                alertIcon: CommonBocIcons.alertError,
                alertBackGroundColor: CommonBocColors.errorLight,
                context: context);
          }
          Navigator.pushNamed(context, report.value);
        },
        leading: Icon(CommonBocIcons.report, color: theme.iconTheme.color),
        title: Text(report.label, style: theme.textTheme.bodyMedium),
        trailing: Icon(CommonBocIcons.next, color: theme.iconTheme.color),
      ),
    );
  }
}
