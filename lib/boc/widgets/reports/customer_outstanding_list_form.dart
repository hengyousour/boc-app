import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:number_paginator/number_paginator.dart';
import 'package:provider/provider.dart';
import '../../models/reports/customer-list/customer_list_report_model.dart';
import '../../models/reports/customer-outstanding-list/customer_outstanding_list_form_report_model.dart';
import '../../providers/reports/customer_outstanding_list_report_provider.dart';
import '/boc/widgets/components/report_content_header.dart';
import '/core/widgets/share_widget.dart';
import '../../../core/utils/core_convert_date_time.dart';
import '../../utils/custom_form_builder_style.dart';
import '../components/report_header.dart';

class CustomerOutstandingListForm extends StatelessWidget {
  const CustomerOutstandingListForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    CustomerOutstandingListReportProvider _readProvider =
        context.read<CustomerOutstandingListReportProvider>();
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: FormBuilderDateTimePicker(
                  name: 'date',
                  initialValue: DateTime.now(),
                  inputType: InputType.date,
                  decoration: fbTextFieldStyle(
                    label: "Date",
                    theme: theme,
                  ),
                  firstDate: DateTime(1970),
                  lastDate: DateTime(2100),
                  format: DateFormat('dd-MM-yyyy'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                  ]),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20.0),
          Expanded(
            child: ListView(shrinkWrap: true, children: [
              ShareWidget(
                shareReady: (value) {
                  _readProvider.setEnablePagination(value: value);
                },
                child: Column(
                  children: [
                    Selector<CustomerOutstandingListReportProvider, DateTime?>(
                      selector: (_, state) =>
                          state.customerOutstandingListFilter.date,
                      builder: (_, date, child) => ReportHeader(
                        khTitle: 'របាយការណ៍អតិថិជនជំពាក់',
                        enTitle: 'Customer Outstanding List Report',
                        date: date != null
                            ? CoreConvertDateTime.formatTimeStampToString(
                                date, false)
                            : null,
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    StreamBuilder(
                      stream: context
                          .read<CustomerOutstandingListReportProvider>()
                          .streamCustomerList,
                      builder: ((context,
                          AsyncSnapshot<List<CustomerListReportModel>>
                              snapshot) {
                        if (!snapshot.hasData) {
                          return Table(
                            defaultVerticalAlignment:
                                TableCellVerticalAlignment.middle,
                            columnWidths: const {
                              0: FixedColumnWidth(38.0),
                            },
                            children: [
                              tableHeader(theme),
                            ],
                          );
                        }
                        switch (snapshot.connectionState) {
                          case ConnectionState.waiting:
                            return const CircularProgressIndicator();
                          default:
                            if (snapshot.hasError) {
                              return Text(snapshot.error.toString());
                            } else {
                              return Column(
                                children: [
                                  Table(
                                    defaultVerticalAlignment:
                                        TableCellVerticalAlignment.middle,
                                    columnWidths: const {
                                      0: FixedColumnWidth(38.0),
                                    },
                                    children: [
                                      tableHeader(theme),
                                      for (int item = 0;
                                          item < snapshot.data!.length;
                                          item++)
                                        TableRow(
                                            decoration: BoxDecoration(
                                              color: theme.primaryColor
                                                  .withOpacity(0.5),
                                              border: Border(
                                                bottom: BorderSide(
                                                    color: theme
                                                        .scaffoldBackgroundColor),
                                              ),
                                            ),
                                            children: [
                                              ReportContentHeader(
                                                text: '${item + 1}',
                                              ),
                                              ReportContentHeader(
                                                text: snapshot.data![item].name,
                                              ),
                                              ReportContentHeader(
                                                text:
                                                    snapshot.data![item].gender,
                                              ),
                                              ReportContentHeader(
                                                text: CoreConvertDateTime
                                                    .formatTimeStampToString(
                                                        snapshot
                                                            .data![item].date,
                                                        false),
                                              ),
                                              ReportContentHeader(
                                                text:
                                                    '${snapshot.data![item].age}',
                                              ),
                                              ReportContentHeader(
                                                text:
                                                    snapshot.data![item].status,
                                              ),
                                            ]),
                                    ],
                                  ),
                                  Selector<
                                      CustomerOutstandingListReportProvider,
                                      bool>(
                                    selector: (_, state) =>
                                        state.enablePagination,
                                    builder: (_, enablePagination, child) =>
                                        enablePagination &&
                                                snapshot.data!.isNotEmpty
                                            ? NumberPaginator(
                                                numberPages:
                                                    _readProvider.totalPage,
                                                onPageChange: (int index) {
                                                  int _currentPage = index += 1;
                                                  DateTime _date =
                                                      FormBuilder.of(context)!
                                                          .fields['date']!
                                                          .value;
                                                  CustomerOutstandingListFormReportModel
                                                      data =
                                                      CustomerOutstandingListFormReportModel(
                                                          date: _date,
                                                          page: _currentPage);
                                                  _readProvider.submit(
                                                      formDoc: data);
                                                },
                                                buttonSelectedBackgroundColor:
                                                    theme.colorScheme.primary,
                                                buttonUnselectedForegroundColor:
                                                    theme.colorScheme.primary,
                                              )
                                            : const SizedBox(),
                                  ),
                                ],
                              );
                            }
                        }
                      }),
                    ),
                  ],
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }

  TableRow tableHeader(ThemeData theme) {
    return TableRow(
      decoration: BoxDecoration(
        color: theme.primaryColor,
        border: Border(
          bottom: BorderSide(color: theme.scaffoldBackgroundColor),
        ),
      ),
      children: const [
        ReportContentHeader(
          text: 'N\u00BA',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Name',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Gender',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Date',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Age',
          bold: true,
        ),
        ReportContentHeader(
          text: 'Status',
          bold: true,
        ),
      ],
    );
  }
}
