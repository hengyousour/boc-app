import 'dart:math';
import 'package:flutter/material.dart';
import '../../models/dashboard/dashboard_card_model.dart';
import '../components/skeleton.dart';

class OverviewSkeleton extends StatelessWidget {
  final DashboardCardModel customCard;
  const OverviewSkeleton({Key? key, required this.customCard})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final deviceData = MediaQuery.of(context);
    final screenSize = MediaQuery.of(context).size;
    final IconData icon = customCard.iconName ?? Icons.abc_sharp;
    final Color iconColor = customCard.iconColor?.withOpacity(0.1) ??
        theme.iconTheme.color!.withOpacity(0.1);
    double iconSize = customCard.iconSize ?? screenSize.width * 0.4;
    if (deviceData.orientation.name == 'landscape') {
      iconSize = customCard.iconSize ?? screenSize.width * 0.25;
    }
    return SizedBox(
      height: customCard.cardHeight,
      child: Card(
          color: customCard.color ?? theme.primaryColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                left: -36,
                child: ClipRRect(
                  child: SizedBox(
                    height: 300.0,
                    child: Transform.rotate(
                      angle: 32 * pi / 160.0,
                      child: Icon(
                        icon,
                        size: iconSize,
                        color: iconColor,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: const [
                    Skeleton(height: 18.0, width: 100.0),
                    SizedBox(height: 10),
                    Skeleton(height: 16.0, width: 100.0),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
