import 'dart:math';
import 'package:countup/countup.dart';
import 'package:flutter/material.dart';
import 'package:mobile_core_app/boc/models/dashboard/dashboard_card_model.dart';
import 'package:mobile_core_app/core/widgets/responsive.dart';

class CustomCard extends StatelessWidget {
  final DashboardCardModel customCard;
  const CustomCard({Key? key, required this.customCard}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final deviceData = MediaQuery.of(context);
    final screenSize = MediaQuery.of(context).size;
    final IconData icon = customCard.iconName ?? Icons.abc_sharp;
    final Color iconColor = customCard.iconColor?.withOpacity(0.1) ??
        theme.iconTheme.color!.withOpacity(0.1);
    double iconSize = customCard.iconSize ?? screenSize.width * 0.4;
    if (deviceData.orientation.name == 'landscape') {
      iconSize = customCard.iconSize ?? screenSize.width * 0.25;
    }
    return SizedBox(
      height: customCard.cardHeight,
      child: Card(
        color: theme.scaffoldBackgroundColor,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
        ),
        // child: InkWell(
        //   customBorder: RoundedRectangleBorder(
        //     borderRadius: const BorderRadius.all(
        //       Radius.circular(20.0),
        //     ),
        //   ),
        //   onTap: () {},
        child: Stack(
          children: [
            Positioned(
              left: -36,
              child: ClipRRect(
                child: SizedBox(
                  height: 300.0,
                  child: Transform.rotate(
                    angle: 32 * pi / 160.0,
                    child: Icon(
                      icon,
                      size: iconSize,
                      color: iconColor,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(customCard.title!.toUpperCase(),
                      style: Responsive.isMobile(context)
                          ? theme.textTheme.headlineMedium!.copyWith(
                              color: customCard.color,
                              overflow: TextOverflow.ellipsis)
                          : Responsive.isTablet(context)
                              ? theme.textTheme.displayMedium!
                                  .copyWith(color: customCard.color)
                              : theme.textTheme.displayLarge!
                                  .copyWith(color: customCard.color)),
                  (customCard.subtitle.runtimeType == String)
                      ? Text(
                          customCard.subtitle,
                          style: theme.textTheme.headlineSmall!.copyWith(
                              color: customCard.color?.withOpacity(0.8)),
                        )
                      : Countup(
                          begin: 0,
                          end: customCard.subtitle?.toDouble(),
                          duration: const Duration(seconds: 1),
                          separator: ',',
                          style: theme.textTheme.headlineSmall!.copyWith(
                            color: customCard.color?.withOpacity(0.8),
                          ),
                        )
                ],
              ),
            )
          ],
        ),
      ),
      // ),
    );
  }
}
