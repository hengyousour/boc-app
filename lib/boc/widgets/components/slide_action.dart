import 'package:flutter/material.dart';

class SlideActionChild extends StatelessWidget {
  final String text;
  final Color? textColor;
  final Color bgColor;
  final IconData iconName;
  final Color? iconColor;
  const SlideActionChild({
    Key? key,
    required this.text,
    this.textColor,
    required this.iconName,
    this.iconColor,
    required this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Card(
      elevation: 0.0,
      color: bgColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            iconName,
            color: iconColor ?? theme.primaryColor,
          ),
          Text(
            text,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: theme.primaryColor, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
