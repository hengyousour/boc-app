import 'package:flutter/material.dart';

Widget customAppBar({
  required final String title,
  required final Color? titleColor,
  final Widget? customLeading,
  final List<Widget>? customActions,
  final PreferredSizeWidget? customBottom,
  final Color? backgroundColor,
}) {
  return AppBar(
    elevation: 0.0,
    leading: customLeading,
    actions: customActions,
    bottom: customBottom,
    backgroundColor: backgroundColor,
    title: Text(
      title.toUpperCase(),
      // style: TextStyle(
      //   color: titleColor,
      //   fontFamily: CommonTongTinFont.header,
      // ),
    ),
    centerTitle: true,
  );
}
