import 'package:flutter/material.dart';
import '../../../core/utils/constants.dart';

class AlertDialogTitle extends StatelessWidget {
  final String title;
  final TextStyle style;
  const AlertDialogTitle({
    Key? key,
    required this.title,
    this.style = const TextStyle(
      fontWeight: FontWeight.bold,
      fontFamily: CommonFonts.header,
    ),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          title,
          style: style,
        ),
      ],
    );
  }
}
