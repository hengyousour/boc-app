import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class Skeleton extends StatelessWidget {
  final double height;
  final double width;
  final double? radius;
  final Color? color;
  final double? colorOpacity;

  const Skeleton(
      {Key? key,
      required this.height,
      required this.width,
      this.radius = 5.0,
      this.color,
      this.colorOpacity = 0.2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return SkeletonAnimation(
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
            color: theme.iconTheme.color!.withOpacity(colorOpacity!),
            borderRadius: BorderRadius.circular(radius!)),
      ),
    );
  }
}
