import 'package:flutter/material.dart';

class ReportContentHeader extends StatefulWidget {
  final String text;
  final Color? textColor;
  final Color? bgColor;
  final EdgeInsetsGeometry padding;
  final AlignmentGeometry? alignment;
  final bool fullBorder;
  final bool bold;

  const ReportContentHeader({
    Key? key,
    required this.text,
    this.textColor,
    this.bgColor,
    this.padding = const EdgeInsets.all(10.0),
    this.alignment,
    this.fullBorder = false,
    this.bold = false,
  }) : super(key: key);

  @override
  State<ReportContentHeader> createState() => _ReportContentHeaderState();
}

class _ReportContentHeaderState extends State<ReportContentHeader> {
  bool _isCollapse = true;

  void setCollapse() {
    _isCollapse = !_isCollapse;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return GestureDetector(
      onTap: () => setCollapse(),
      child: Container(
        padding: widget.padding,
        alignment: widget.alignment,
        decoration: widget.fullBorder
            ? BoxDecoration(
                color: widget.bgColor,
                border: Border.all(color: theme.primaryColor))
            : null,
        child: Text(widget.text,
            overflow: _isCollapse ? TextOverflow.ellipsis : null,
            style: theme.textTheme.bodyMedium!.copyWith(
              fontWeight: widget.bold ? FontWeight.bold : FontWeight.normal,
              color: widget.textColor,
              // fontSize: CommonTongTinReportTextSize.bodyTextSize,
            )),
      ),
    );
  }
}
