import 'package:flutter/material.dart';

class ReportHeader extends StatelessWidget {
  final String khTitle;
  final String enTitle;
  final String? date;
  final String? fromDate;
  final String? toDate;
  const ReportHeader({
    Key? key,
    required this.khTitle,
    required this.enTitle,
    this.date,
    this.fromDate,
    this.toDate,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Column(
      children: [
        //Kh title header
        Text(
          khTitle,
          style: theme.textTheme.headlineSmall!
              .copyWith(fontWeight: FontWeight.bold),
        ),
        //En title header
        Text(
          enTitle,
          style:
              theme.textTheme.titleLarge!.copyWith(fontWeight: FontWeight.bold),
        ),
        //date or date range
        if (fromDate != null && toDate != null)
          Text(
            '$fromDate - $toDate',
            style: theme.textTheme.titleLarge!
                .copyWith(fontWeight: FontWeight.bold),
          ),
        if (date != null)
          Text(
            '$date',
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              // fontFamily: CommonTongTinFont.header,
              // fontSize: CommonTongTinReportTextSize.subTitleTextSize,
            ),
          )
      ],
    );
  }
}
