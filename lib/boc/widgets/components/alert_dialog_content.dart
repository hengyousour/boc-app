import 'package:flutter/material.dart';

class AlertDialogContent extends StatelessWidget {
  final String textContent;
  const AlertDialogContent({Key? key, required this.textContent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            textContent,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
