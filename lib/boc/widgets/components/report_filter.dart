import 'package:flutter/material.dart';
import '../../models/reports/report_filter_model.dart';

class ReportFilter extends StatelessWidget {
  final List<ReportFilterModel> reportFilterList;
  final WrapAlignment alignment;
  const ReportFilter(
      {Key? key,
      required this.reportFilterList,
      this.alignment = WrapAlignment.spaceBetween})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return SizedBox(
      width: double.maxFinite,
      child: Wrap(
        alignment: alignment,
        children: [
          for (ReportFilterModel reportFilter in reportFilterList)
            RichText(
              text: TextSpan(
                  text: '${reportFilter.label} : ',
                  style: theme.textTheme.bodyMedium!
                      .copyWith(fontWeight: FontWeight.bold),
                  children: [
                    TextSpan(
                      text: '${reportFilter.value}',
                      style: theme.textTheme.bodyMedium,
                    )
                  ]),
            )
        ],
      ),
    );
  }
}
