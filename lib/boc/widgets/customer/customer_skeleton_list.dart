import 'package:flutter/material.dart';
import '../components/skeleton.dart';

class CustomerSkeletonList extends StatelessWidget {
  final Color color;
  final double? colorOpacity;
  const CustomerSkeletonList({
    Key? key,
    this.color = Colors.black,
    this.colorOpacity = 0.5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0.0,
      margin: const EdgeInsets.only(bottom: 10),
      color: color.withOpacity(colorOpacity!),
      child: Column(
        children: [
          ListTile(
            tileColor: color,
            leading: const Skeleton(height: 58.0, width: 58.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Skeleton(height: 10.0, width: 270.0),
                SizedBox(height: 5.0),
                Skeleton(height: 10, width: 150.0),
              ],
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                SizedBox(height: 5.0),
                Skeleton(height: 10, width: 200.0),
                SizedBox(height: 5.0),
                Skeleton(height: 10, width: 200.0)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
