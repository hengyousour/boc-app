import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:mobile_core_app/boc/utils/static_options.dart';
import '../../utils/custom_form_builder_style.dart';

class CreateCustomerForm extends StatelessWidget {
  const CreateCustomerForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return SingleChildScrollView(
      controller: ScrollController(),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: FormBuilderTextField(
                    name: 'name',
                    decoration: fbTextFieldStyle(label: 'Name', theme: theme),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required()]),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: FormBuilderTextField(
                      name: 'age',
                      decoration: fbTextFieldStyle(label: 'Age', theme: theme),
                      valueTransformer: (String? value) {
                        if (value!.isNotEmpty) {
                          return int.tryParse(value);
                        }
                        return value;
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(1),
                        FormBuilderValidators.max(100),
                      ]),
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                      ]),
                ),
                const SizedBox(width: 10.0),
                Expanded(
                  child: FormBuilderDropdown(
                    name: 'gender',
                    decoration:
                        fbSelectOptionFieldStyle(label: 'Gender', theme: theme),
                    initialValue: 'Male',
                    allowClear: true,
                    hint: const Text('Select Gender'),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required()]),
                    items: StaticOptions.genderOption
                        .map((opt) => DropdownMenuItem(
                              value: opt.value,
                              child: Text(opt.value.toString()),
                            ))
                        .toList(),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: FormBuilderTextField(
                    name: 'telephone',
                    decoration:
                        fbTextFieldStyle(label: 'Phone Number', theme: theme),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.numeric(),
                    ]),
                    keyboardType: TextInputType.phone,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp("^[0-9+*#]+"))
                    ],
                  ),
                ),
                const SizedBox(width: 10.0),
                Expanded(
                  child: FormBuilderDateTimePicker(
                    name: 'date',
                    initialValue: DateTime.now(),
                    decoration:
                        fbDateTimePickerFieldStyle(label: 'Date', theme: theme),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(),
                    ]),
                    keyboardType: TextInputType.datetime,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10.0,
            ),
            const ReferDetail(),
            const SizedBox(height: 10.0),
            const VaPhDetail(),
            const SizedBox(height: 10.0),
            const RightEye(),
            const SizedBox(height: 10.0),
            const LeftEye(),
            const SizedBox(height: 10.0),
            Row(
              children: [
                Expanded(
                    child: FormBuilderTextField(
                  name: 'add',
                  decoration: fbTextFieldStyle(label: 'ADD', theme: theme),
                )),
                const SizedBox(width: 10.0),
                Expanded(
                    child: FormBuilderTextField(
                  name: 'distancePd',
                  decoration:
                      fbTextFieldStyle(label: 'Distance PD', theme: theme),
                )),
                const SizedBox(width: 10.0),
                Expanded(
                    child: FormBuilderTextField(
                  name: 'nearPd',
                  decoration: fbTextFieldStyle(label: 'Near PD', theme: theme),
                ))
              ],
            )
          ],
        ),
      ),
    );
  }
}

class RightEye extends StatelessWidget {
  const RightEye({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Text('Right Eye'),
        const SizedBox(height: 10.0),
        Row(
          children: [
            Expanded(
              child: FormBuilderTextField(
                  name: 'right-sph',
                  decoration: fbTextFieldStyle(label: 'SPH', theme: theme)),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: FormBuilderTextField(
                name: 'right-cyl',
                decoration: fbTextFieldStyle(label: 'CYL', theme: theme),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.numeric(),
                ]),
                keyboardType:
                    const TextInputType.numberWithOptions(signed: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp("^[0-9/.,+*() -]+"))
                ],
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: FormBuilderTextField(
                name: 'right-axis',
                decoration: fbTextFieldStyle(label: 'AXIS', theme: theme),
                keyboardType:
                    const TextInputType.numberWithOptions(signed: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp("^[0-9/.,+*() -]+"))
                ],
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: FormBuilderTextField(
                  name: 'right-va',
                  decoration: fbTextFieldStyle(label: 'V/A', theme: theme)),
            ),
          ],
        )
      ],
    );
  }
}

class LeftEye extends StatelessWidget {
  const LeftEye({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Text('Left Eye'),
        const SizedBox(height: 10.0),
        Row(
          children: [
            Expanded(
              child: FormBuilderTextField(
                  name: 'left-sph',
                  decoration: fbTextFieldStyle(label: 'SPH', theme: theme)),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: FormBuilderTextField(
                name: 'left-cyl',
                decoration: fbTextFieldStyle(label: 'CYL', theme: theme),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.numeric(),
                ]),
                keyboardType:
                    const TextInputType.numberWithOptions(signed: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp("^[0-9/.,+*() -]+"))
                ],
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: FormBuilderTextField(
                name: 'left-axis',
                decoration: fbTextFieldStyle(label: 'AXIS', theme: theme),
                keyboardType:
                    const TextInputType.numberWithOptions(signed: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp("^[0-9/.,+*() -]+"))
                ],
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: FormBuilderTextField(
                  name: 'left-va',
                  decoration: fbTextFieldStyle(label: 'V/A', theme: theme)),
            ),
          ],
        )
      ],
    );
  }
}

class VaPhDetail extends StatelessWidget {
  const VaPhDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(children: [
          SizedBox(
            width: size.width * 0.1,
          ),
          const Expanded(child: Center(child: Text('V/A'))),
          const Expanded(child: Center(child: Text('PH')))
        ]),
        const SizedBox(
          height: 10.0,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: size.width * 0.05,
              padding: const EdgeInsets.only(top: 15),
              child: const Text('R', textAlign: TextAlign.center),
            ),
            const SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: FormBuilderTextField(
                name: 'va-r',
                decoration: fbTextFieldStyle(label: 'V/A Right', theme: theme),
              ),
            ),
            const SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: FormBuilderTextField(
                name: 'ph-r',
                decoration: fbTextFieldStyle(label: 'PH Right', theme: theme),
              ),
            ),
          ],
        ),
        const SizedBox(height: 10.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: size.width * 0.05,
              padding: const EdgeInsets.only(top: 15),
              child: const Text('L', textAlign: TextAlign.center),
            ),
            const SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: FormBuilderTextField(
                name: 'va-l',
                decoration: fbTextFieldStyle(label: 'V/A Left', theme: theme),
              ),
            ),
            const SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: FormBuilderTextField(
                name: 'ph-l',
                decoration: fbTextFieldStyle(label: 'PH Left', theme: theme),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class ReferDetail extends StatelessWidget {
  const ReferDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Text('Refer'),
        const SizedBox(height: 10.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: FormBuilderFilterChip(
                name: 'refer',
                decoration:
                    fbMultiSelectChipFieldStyle(label: '', theme: theme),
                spacing: 10.0,
                valueTransformer: (values) {
                  Map<String, String> tempResult = {
                    'cons': 'false',
                    'sur': 'false',
                    'field': 'false'
                  };

                  final List<Map<String, String>> result = values!.map((v) {
                    if (v == 'cons') {
                      tempResult['cons'] = 'true';
                    }

                    if (v == 'sur') {
                      tempResult['sur'] = 'true';
                    }

                    if (v == 'field') {
                      tempResult['field'] = 'true';
                    }
                    return tempResult;
                  }).toList();

                  return result.isNotEmpty ? result.last : tempResult;
                },
                options: StaticOptions.referOption
                    .map((opt) => FormBuilderChipOption(
                        value: opt.value, child: Text(opt.label)))
                    .toList(),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
