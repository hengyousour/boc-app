import 'package:json_annotation/json_annotation.dart';
import '../../../utils/date_model_converter.dart';
part 'customer_list_form_report_model.g.dart';

@JsonSerializable()
class CustomerListFormReportModel {
  @JsonKey(
      fromJson: DateModelConverter.convertDateTimeOptionalForModel,
      toJson: DateModelConverter.convertDateTimeOptionalForModel)
  final DateTime? fromDate;
  @JsonKey(
      fromJson: DateModelConverter.convertDateTimeOptionalForModel,
      toJson: DateModelConverter.convertDateTimeOptionalForModel)
  final DateTime? toDate;
  final String status;
  final int? page;
  final int? pageSize;

  const CustomerListFormReportModel({
    required this.fromDate,
    required this.toDate,
    required this.status,
    this.page,
    this.pageSize,
  });

  factory CustomerListFormReportModel.fromJson(Map<String, dynamic> json) =>
      _$CustomerListFormReportModelFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerListFormReportModelToJson(this);
}
