// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_list_form_report_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerListFormReportModel _$CustomerListFormReportModelFromJson(
        Map<String, dynamic> json) =>
    CustomerListFormReportModel(
      fromDate: DateModelConverter.convertDateTimeOptionalForModel(
          json['fromDate'] as DateTime?),
      toDate: DateModelConverter.convertDateTimeOptionalForModel(
          json['toDate'] as DateTime?),
      status: json['status'] as String,
      page: json['page'] as int?,
      pageSize: json['pageSize'] as int?,
    );

Map<String, dynamic> _$CustomerListFormReportModelToJson(
        CustomerListFormReportModel instance) =>
    <String, dynamic>{
      'fromDate':
          DateModelConverter.convertDateTimeOptionalForModel(instance.fromDate),
      'toDate':
          DateModelConverter.convertDateTimeOptionalForModel(instance.toDate),
      'status': instance.status,
      'page': instance.page,
      'pageSize': instance.pageSize,
    };
