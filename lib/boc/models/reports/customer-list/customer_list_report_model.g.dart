// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_list_report_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerListReportModel _$CustomerListReportModelFromJson(
    Map<String, dynamic> json) {
  $checkKeys(
    json,
    disallowNullValues: const ['_id'],
  );
  return CustomerListReportModel(
    id: json['_id'] as String?,
    name: json['name'] as String,
    gender: json['gender'] as String,
    date: DateModelConverter.convertDateTimeForModel(json['date'] as DateTime),
    age: json['age'] as int,
    status: json['status'] as String,
  );
}

Map<String, dynamic> _$CustomerListReportModelToJson(
    CustomerListReportModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  val['name'] = instance.name;
  val['gender'] = instance.gender;
  val['date'] = DateModelConverter.convertDateTimeForModel(instance.date);
  val['age'] = instance.age;
  val['status'] = instance.status;
  return val;
}
