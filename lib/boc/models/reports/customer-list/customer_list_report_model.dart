import 'package:json_annotation/json_annotation.dart';
import '../../../utils/date_model_converter.dart';
part 'customer_list_report_model.g.dart';

@JsonSerializable()
class CustomerListReportModel {
  @JsonKey(name: '_id', disallowNullValue: true)
  final String? id;
  final String name;
  final String gender;
  @JsonKey(
      fromJson: DateModelConverter.convertDateTimeForModel,
      toJson: DateModelConverter.convertDateTimeForModel)
  final DateTime date;
  final int age;
  final String status;

  const CustomerListReportModel({
    this.id,
    required this.name,
    required this.gender,
    required this.date,
    required this.age,
    required this.status,
  });

  factory CustomerListReportModel.fromJson(Map<String, dynamic> json) =>
      _$CustomerListReportModelFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerListReportModelToJson(this);
}
