import 'package:json_annotation/json_annotation.dart';
import '../../../utils/date_model_converter.dart';
part 'customer_outstanding_list_form_report_model.g.dart';

@JsonSerializable()
class CustomerOutstandingListFormReportModel {
  @JsonKey(
      fromJson: DateModelConverter.convertDateTimeOptionalForModel,
      toJson: DateModelConverter.convertDateTimeOptionalForModel)
  final DateTime? date;
  final int? page;
  final int? pageSize;

  const CustomerOutstandingListFormReportModel(
      {required this.date, this.page, this.pageSize});

  factory CustomerOutstandingListFormReportModel.fromJson(
          Map<String, dynamic> json) =>
      _$CustomerOutstandingListFormReportModelFromJson(json);
  Map<String, dynamic> toJson() =>
      _$CustomerOutstandingListFormReportModelToJson(this);
}
