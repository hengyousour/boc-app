// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_outstanding_list_form_report_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerOutstandingListFormReportModel
    _$CustomerOutstandingListFormReportModelFromJson(
            Map<String, dynamic> json) =>
        CustomerOutstandingListFormReportModel(
          date: DateModelConverter.convertDateTimeOptionalForModel(
              json['date'] as DateTime?),
          page: json['page'] as int?,
          pageSize: json['pageSize'] as int?,
        );

Map<String, dynamic> _$CustomerOutstandingListFormReportModelToJson(
        CustomerOutstandingListFormReportModel instance) =>
    <String, dynamic>{
      'date': DateModelConverter.convertDateTimeOptionalForModel(instance.date),
      'page': instance.page,
      'pageSize': instance.pageSize,
    };
