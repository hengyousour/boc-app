import 'package:json_annotation/json_annotation.dart';
import 'package:mobile_core_app/boc/models/reports/biorc/biorc_data_report_model.dart';

part 'biorc_report_model.g.dart';

@JsonSerializable(explicitToJson: true)
class BiorcReportModel {
  final List<BiorcDataReportModel> data;
  final int gtRFCTotal;
  final int gtRFCMale;
  final int gtRFCFemale;
  final int gtAOSTotal;
  final int gtAOSMale;
  final int gtAOSFemale;
  final int gtAcceptedTotal;
  final int gtAcceptedMale;
  final int gtAcceptedFemale;
  final int gtNotAcceptedTotal;
  final int gtNotAcceptedMale;
  final int gtNotAcceptedFemale;
  final int gtMissingTotal;
  final int gtMissingMale;
  final int gtMissingFemale;

  const BiorcReportModel({
    required this.data,
    required this.gtRFCTotal,
    required this.gtRFCMale,
    required this.gtRFCFemale,
    required this.gtAOSTotal,
    required this.gtAOSMale,
    required this.gtAOSFemale,
    required this.gtAcceptedTotal,
    required this.gtAcceptedMale,
    required this.gtAcceptedFemale,
    required this.gtNotAcceptedTotal,
    required this.gtNotAcceptedMale,
    required this.gtNotAcceptedFemale,
    required this.gtMissingTotal,
    required this.gtMissingMale,
    required this.gtMissingFemale,
  });

  factory BiorcReportModel.fromJson(Map<String, dynamic> json) =>
      _$BiorcReportModelFromJson(json);
  Map<String, dynamic> toJson() => _$BiorcReportModelToJson(this);
}
