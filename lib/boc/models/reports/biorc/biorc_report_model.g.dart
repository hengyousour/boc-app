// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'biorc_report_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BiorcReportModel _$BiorcReportModelFromJson(Map<String, dynamic> json) =>
    BiorcReportModel(
      data: (json['data'] as List<dynamic>)
          .map((e) => BiorcDataReportModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      gtRFCTotal: json['gtRFCTotal'] as int,
      gtRFCMale: json['gtRFCMale'] as int,
      gtRFCFemale: json['gtRFCFemale'] as int,
      gtAOSTotal: json['gtAOSTotal'] as int,
      gtAOSMale: json['gtAOSMale'] as int,
      gtAOSFemale: json['gtAOSFemale'] as int,
      gtAcceptedTotal: json['gtAcceptedTotal'] as int,
      gtAcceptedMale: json['gtAcceptedMale'] as int,
      gtAcceptedFemale: json['gtAcceptedFemale'] as int,
      gtNotAcceptedTotal: json['gtNotAcceptedTotal'] as int,
      gtNotAcceptedMale: json['gtNotAcceptedMale'] as int,
      gtNotAcceptedFemale: json['gtNotAcceptedFemale'] as int,
      gtMissingTotal: json['gtMissingTotal'] as int,
      gtMissingMale: json['gtMissingMale'] as int,
      gtMissingFemale: json['gtMissingFemale'] as int,
    );

Map<String, dynamic> _$BiorcReportModelToJson(BiorcReportModel instance) =>
    <String, dynamic>{
      'data': instance.data.map((e) => e.toJson()).toList(),
      'gtRFCTotal': instance.gtRFCTotal,
      'gtRFCMale': instance.gtRFCMale,
      'gtRFCFemale': instance.gtRFCFemale,
      'gtAOSTotal': instance.gtAOSTotal,
      'gtAOSMale': instance.gtAOSMale,
      'gtAOSFemale': instance.gtAOSFemale,
      'gtAcceptedTotal': instance.gtAcceptedTotal,
      'gtAcceptedMale': instance.gtAcceptedMale,
      'gtAcceptedFemale': instance.gtAcceptedFemale,
      'gtNotAcceptedTotal': instance.gtNotAcceptedTotal,
      'gtNotAcceptedMale': instance.gtNotAcceptedMale,
      'gtNotAcceptedFemale': instance.gtNotAcceptedFemale,
      'gtMissingTotal': instance.gtMissingTotal,
      'gtMissingMale': instance.gtMissingMale,
      'gtMissingFemale': instance.gtMissingFemale,
    };
