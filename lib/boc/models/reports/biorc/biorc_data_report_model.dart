import 'package:json_annotation/json_annotation.dart';
part 'biorc_data_report_model.g.dart';

@JsonSerializable()
class BiorcDataReportModel {
  //cons
  final int consRFCTotal;
  final int consRFCMale;
  final int consRFCFemale;
  final int consAOSTotal;
  final int consAOSMale;
  final int consAOSFemale;
  final int consAcceptedTotal;
  final int consAcceptedMale;
  final int consAcceptedFemale;
  final int consNotAcceptedTotal;
  final int consNotAcceptedMale;
  final int consNotAcceptedFemale;
  final int consMissingTotal;
  final int consMissingMale;
  final int consMissingFemale;
  //sur
  final int surRFCTotal;
  final int surRFCMale;
  final int surRFCFemale;
  final int surAOSTotal;
  final int surAOSMale;
  final int surAOSFemale;
  final int surAcceptedTotal;
  final int surAcceptedMale;
  final int surAcceptedFemale;
  final int surNotAcceptedTotal;
  final int surNotAcceptedMale;
  final int surNotAcceptedFemale;
  final int surMissingTotal;
  final int surMissingMale;
  final int surMissingFemale;
  //field
  final int fieldRFCTotal;
  final int fieldRFCMale;
  final int fieldRFCFemale;
  final int fieldAOSTotal;
  final int fieldAOSMale;
  final int fieldAOSFemale;
  final int fieldAcceptedTotal;
  final int fieldAcceptedMale;
  final int fieldAcceptedFemale;
  final int fieldNotAcceptedTotal;
  final int fieldNotAcceptedMale;
  final int fieldNotAcceptedFemale;
  final int fieldMissingTotal;
  final int fieldMissingMale;
  final int fieldMissingFemale;

  const BiorcDataReportModel({
    required this.consRFCTotal,
    required this.consRFCMale,
    required this.consRFCFemale,
    required this.consAOSTotal,
    required this.consAOSMale,
    required this.consAOSFemale,
    required this.consAcceptedTotal,
    required this.consAcceptedMale,
    required this.consAcceptedFemale,
    required this.consNotAcceptedTotal,
    required this.consNotAcceptedMale,
    required this.consNotAcceptedFemale,
    required this.consMissingTotal,
    required this.consMissingMale,
    required this.consMissingFemale,
    required this.surRFCTotal,
    required this.surRFCMale,
    required this.surRFCFemale,
    required this.surAOSTotal,
    required this.surAOSMale,
    required this.surAOSFemale,
    required this.surAcceptedTotal,
    required this.surAcceptedMale,
    required this.surAcceptedFemale,
    required this.surNotAcceptedTotal,
    required this.surNotAcceptedMale,
    required this.surNotAcceptedFemale,
    required this.surMissingTotal,
    required this.surMissingMale,
    required this.surMissingFemale,
    required this.fieldRFCTotal,
    required this.fieldRFCMale,
    required this.fieldRFCFemale,
    required this.fieldAOSTotal,
    required this.fieldAOSMale,
    required this.fieldAOSFemale,
    required this.fieldAcceptedTotal,
    required this.fieldAcceptedMale,
    required this.fieldAcceptedFemale,
    required this.fieldNotAcceptedTotal,
    required this.fieldNotAcceptedMale,
    required this.fieldNotAcceptedFemale,
    required this.fieldMissingTotal,
    required this.fieldMissingMale,
    required this.fieldMissingFemale,
  });

  factory BiorcDataReportModel.fromJson(Map<String, dynamic> json) =>
      _$BiorcDataReportModelFromJson(json);
  Map<String, dynamic> toJson() => _$BiorcDataReportModelToJson(this);
}
