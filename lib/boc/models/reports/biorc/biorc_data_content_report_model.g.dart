// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'biorc_data_content_report_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BiorcDataContentReportModel _$BiorcDataContentReportModelFromJson(
        Map<String, dynamic> json) =>
    BiorcDataContentReportModel(
      title: json['title'] as String,
      rfcTotal: json['rfcTotal'] as int,
      rfcMale: json['rfcMale'] as int,
      rfcFemale: json['rfcFemale'] as int,
      aosTotal: json['aosTotal'] as int,
      aosMale: json['aosMale'] as int,
      aosFemale: json['aosFemale'] as int,
      acceptedTotal: json['acceptedTotal'] as int,
      acceptedMale: json['acceptedMale'] as int,
      acceptedFemale: json['acceptedFemale'] as int,
      notAcceptedTotal: json['notAcceptedTotal'] as int,
      notAcceptedMale: json['notAcceptedMale'] as int,
      notAcceptedFemale: json['notAcceptedFemale'] as int,
      missingTotal: json['missingTotal'] as int,
      missingMale: json['missingMale'] as int,
      missingFemale: json['missingFemale'] as int,
    );

Map<String, dynamic> _$BiorcDataContentReportModelToJson(
        BiorcDataContentReportModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'rfcTotal': instance.rfcTotal,
      'rfcMale': instance.rfcMale,
      'rfcFemale': instance.rfcFemale,
      'aosTotal': instance.aosTotal,
      'aosMale': instance.aosMale,
      'aosFemale': instance.aosFemale,
      'acceptedTotal': instance.acceptedTotal,
      'acceptedMale': instance.acceptedMale,
      'acceptedFemale': instance.acceptedFemale,
      'notAcceptedTotal': instance.notAcceptedTotal,
      'notAcceptedMale': instance.notAcceptedMale,
      'notAcceptedFemale': instance.notAcceptedFemale,
      'missingTotal': instance.missingTotal,
      'missingMale': instance.missingMale,
      'missingFemale': instance.missingFemale,
    };
