// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'biorc_form_report_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BiorcFormReportModel _$BiorcFormReportModelFromJson(
        Map<String, dynamic> json) =>
    BiorcFormReportModel(
      fromDate: DateModelConverter.convertDateTimeOptionalForModel(
          json['fromDate'] as DateTime?),
      toDate: DateModelConverter.convertDateTimeOptionalForModel(
          json['toDate'] as DateTime?),
    );

Map<String, dynamic> _$BiorcFormReportModelToJson(
        BiorcFormReportModel instance) =>
    <String, dynamic>{
      'fromDate':
          DateModelConverter.convertDateTimeOptionalForModel(instance.fromDate),
      'toDate':
          DateModelConverter.convertDateTimeOptionalForModel(instance.toDate),
    };
