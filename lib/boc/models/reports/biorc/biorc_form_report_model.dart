import 'package:json_annotation/json_annotation.dart';
import '../../../utils/date_model_converter.dart';
part 'biorc_form_report_model.g.dart';

@JsonSerializable()
class BiorcFormReportModel {
  @JsonKey(
      fromJson: DateModelConverter.convertDateTimeOptionalForModel,
      toJson: DateModelConverter.convertDateTimeOptionalForModel)
  final DateTime? fromDate;
  @JsonKey(
      fromJson: DateModelConverter.convertDateTimeOptionalForModel,
      toJson: DateModelConverter.convertDateTimeOptionalForModel)
  final DateTime? toDate;

  const BiorcFormReportModel({
    required this.fromDate,
    required this.toDate,
  });

  factory BiorcFormReportModel.fromJson(Map<String, dynamic> json) =>
      _$BiorcFormReportModelFromJson(json);
  Map<String, dynamic> toJson() => _$BiorcFormReportModelToJson(this);
}
