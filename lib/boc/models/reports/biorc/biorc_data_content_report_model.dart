import 'package:json_annotation/json_annotation.dart';
part 'biorc_data_content_report_model.g.dart';

@JsonSerializable()
class BiorcDataContentReportModel {
  final String title;
  final int rfcTotal;
  final int rfcMale;
  final int rfcFemale;
  final int aosTotal;
  final int aosMale;
  final int aosFemale;
  final int acceptedTotal;
  final int acceptedMale;
  final int acceptedFemale;
  final int notAcceptedTotal;
  final int notAcceptedMale;
  final int notAcceptedFemale;
  final int missingTotal;
  final int missingMale;
  final int missingFemale;

  const BiorcDataContentReportModel({
    required this.title,
    required this.rfcTotal,
    required this.rfcMale,
    required this.rfcFemale,
    required this.aosTotal,
    required this.aosMale,
    required this.aosFemale,
    required this.acceptedTotal,
    required this.acceptedMale,
    required this.acceptedFemale,
    required this.notAcceptedTotal,
    required this.notAcceptedMale,
    required this.notAcceptedFemale,
    required this.missingTotal,
    required this.missingMale,
    required this.missingFemale,
  });

  factory BiorcDataContentReportModel.fromJson(Map<String, dynamic> json) =>
      _$BiorcDataContentReportModelFromJson(json);
  Map<String, dynamic> toJson() => _$BiorcDataContentReportModelToJson(this);
}
