// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_filter_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportFilterModel _$ReportFilterModelFromJson(Map<String, dynamic> json) =>
    ReportFilterModel(
      label: json['label'] as String,
      value: json['value'],
    );

Map<String, dynamic> _$ReportFilterModelToJson(ReportFilterModel instance) =>
    <String, dynamic>{
      'label': instance.label,
      'value': instance.value,
    };
