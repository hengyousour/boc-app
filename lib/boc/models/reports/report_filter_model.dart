import 'package:json_annotation/json_annotation.dart';
part 'report_filter_model.g.dart';

@JsonSerializable()
class ReportFilterModel {
  final String label;
  final dynamic value;
  const ReportFilterModel({required this.label, this.value});

  factory ReportFilterModel.fromJson(Map<String, dynamic> json) =>
      _$ReportFilterModelFromJson(json);
  Map<String, dynamic> toJson() => _$ReportFilterModelToJson(this);
}
