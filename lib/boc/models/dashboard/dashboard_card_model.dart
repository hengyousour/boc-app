import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DashboardCardModel {
  final String? title;
  final dynamic subtitle;
  final double cardHeight;
  final Color? color;
  final VoidCallback? onPressed;
  final IconData? iconName;
  final Color? iconColor;
  final double? iconSize;
  final Color? bgColors;

  const DashboardCardModel({
    this.title = 'title',
    this.subtitle = 'sub-title',
    this.cardHeight = 200.0,
    this.color,
    this.onPressed,
    this.iconName,
    this.iconColor,
    this.iconSize,
    this.bgColors,
  });
}
