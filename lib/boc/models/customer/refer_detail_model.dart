import 'package:json_annotation/json_annotation.dart';

part 'refer_detail_model.g.dart';

@JsonSerializable()
class ReferDetailModel {
  final String? cons;
  final String? sur;
  final String? field;
  const ReferDetailModel({this.cons, this.sur, this.field});
  factory ReferDetailModel.fromJson(Map<String, dynamic> data) =>
      _$ReferDetailModelFromJson(data);
  Map<String, dynamic> toJson() => _$ReferDetailModelToJson(this);
}
