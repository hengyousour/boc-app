// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'eyes_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EyesDetailModel _$EyesDetailModelFromJson(Map<String, dynamic> json) =>
    EyesDetailModel(
      sph: json['sph'] as String?,
      cyl: json['cyl'] as String?,
      axis: json['axis'] as String?,
      va: json['va'] as String?,
    );

Map<String, dynamic> _$EyesDetailModelToJson(EyesDetailModel instance) =>
    <String, dynamic>{
      'sph': instance.sph,
      'cyl': instance.cyl,
      'axis': instance.axis,
      'va': instance.va,
    };
