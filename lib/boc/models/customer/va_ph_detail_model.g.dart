// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'va_ph_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VaPhDetailModel _$VaPhDetailModelFromJson(Map<String, dynamic> json) =>
    VaPhDetailModel(
      left: json['left'] as String?,
      right: json['right'] as String?,
    );

Map<String, dynamic> _$VaPhDetailModelToJson(VaPhDetailModel instance) =>
    <String, dynamic>{
      'left': instance.left,
      'right': instance.right,
    };
