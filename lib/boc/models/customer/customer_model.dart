import 'package:json_annotation/json_annotation.dart';
import '../../utils/date_model_converter.dart';
import 'eyes_detail_model.dart';
import 'refer_detail_model.dart';
import 'va_ph_detail_model.dart';
part 'customer_model.g.dart';

@JsonSerializable(explicitToJson: true)
class CustomerModel {
  @JsonKey(name: '_id', disallowNullValue: true)
  final String? id;
  final String name;
  final int age;
  final String gender;
  final String? telephone;
  @JsonKey(
      fromJson: DateModelConverter.convertDateTimeForModel,
      toJson: DateModelConverter.convertDateTimeForModel)
  final DateTime date;
  @JsonKey(
      fromJson: DateModelConverter.convertDateTimeOptionalForModel,
      toJson: DateModelConverter.convertDateTimeOptionalForModel)
  final DateTime? closedDate;
  final ReferDetailModel? refer;
  final VaPhDetailModel? va;
  final VaPhDetailModel? ph;
  final EyesDetailModel? leftEye;
  final EyesDetailModel? rightEye;
  final String? add;
  final String? distancePd;
  final String? nearPd;
  final String status;
  @JsonKey(disallowNullValue: true)
  final String? branchId;
  const CustomerModel(
      {this.id,
      required this.name,
      required this.age,
      required this.gender,
      this.telephone,
      required this.date,
      this.closedDate,
      this.refer,
      this.va,
      this.ph,
      this.leftEye,
      this.rightEye,
      this.add,
      this.distancePd,
      this.nearPd,
      required this.status,
      this.branchId});
  factory CustomerModel.fromJson(Map<String, dynamic> data) =>
      _$CustomerModelFromJson(data);
  Map<String, dynamic> toJson() => _$CustomerModelToJson(this);
}
