// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refer_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReferDetailModel _$ReferDetailModelFromJson(Map<String, dynamic> json) =>
    ReferDetailModel(
      cons: json['cons'] as String?,
      sur: json['sur'] as String?,
      field: json['field'] as String?,
    );

Map<String, dynamic> _$ReferDetailModelToJson(ReferDetailModel instance) =>
    <String, dynamic>{
      'cons': instance.cons,
      'sur': instance.sur,
      'field': instance.field,
    };
