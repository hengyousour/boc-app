import 'package:json_annotation/json_annotation.dart';
part 'eyes_detail_model.g.dart';

@JsonSerializable()
class EyesDetailModel {
  final String? sph;
  final String? cyl;
  final String? axis;
  final String? va;
  EyesDetailModel({this.sph, this.cyl, this.axis, this.va});
  factory EyesDetailModel.fromJson(Map<String, dynamic> data) =>
      _$EyesDetailModelFromJson(data);
  Map<String, dynamic> toJson() => _$EyesDetailModelToJson(this);
}
