// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerModel _$CustomerModelFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    disallowNullValues: const ['_id', 'branchId'],
  );
  return CustomerModel(
    id: json['_id'] as String?,
    name: json['name'] as String,
    age: json['age'] as int,
    gender: json['gender'] as String,
    telephone: json['telephone'] as String?,
    date: DateModelConverter.convertDateTimeForModel(json['date'] as DateTime),
    closedDate: DateModelConverter.convertDateTimeOptionalForModel(
        json['closedDate'] as DateTime?),
    refer: json['refer'] == null
        ? null
        : ReferDetailModel.fromJson(json['refer'] as Map<String, dynamic>),
    va: json['va'] == null
        ? null
        : VaPhDetailModel.fromJson(json['va'] as Map<String, dynamic>),
    ph: json['ph'] == null
        ? null
        : VaPhDetailModel.fromJson(json['ph'] as Map<String, dynamic>),
    leftEye: json['leftEye'] == null
        ? null
        : EyesDetailModel.fromJson(json['leftEye'] as Map<String, dynamic>),
    rightEye: json['rightEye'] == null
        ? null
        : EyesDetailModel.fromJson(json['rightEye'] as Map<String, dynamic>),
    add: json['add'] as String?,
    distancePd: json['distancePd'] as String?,
    nearPd: json['nearPd'] as String?,
    status: json['status'] as String,
    branchId: json['branchId'] as String?,
  );
}

Map<String, dynamic> _$CustomerModelToJson(CustomerModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  val['name'] = instance.name;
  val['age'] = instance.age;
  val['gender'] = instance.gender;
  val['telephone'] = instance.telephone;
  val['date'] = DateModelConverter.convertDateTimeForModel(instance.date);
  val['closedDate'] =
      DateModelConverter.convertDateTimeOptionalForModel(instance.closedDate);
  val['refer'] = instance.refer?.toJson();
  val['va'] = instance.va?.toJson();
  val['ph'] = instance.ph?.toJson();
  val['leftEye'] = instance.leftEye?.toJson();
  val['rightEye'] = instance.rightEye?.toJson();
  val['add'] = instance.add;
  val['distancePd'] = instance.distancePd;
  val['nearPd'] = instance.nearPd;
  val['status'] = instance.status;
  writeNotNull('branchId', instance.branchId);
  return val;
}
