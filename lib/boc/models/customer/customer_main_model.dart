import 'package:json_annotation/json_annotation.dart';
import 'customer_model.dart';
part 'customer_main_model.g.dart';

@JsonSerializable(explicitToJson: true)
class CustomerMainModel {
  final List<CustomerModel> customerData;
  final int page;
  CustomerMainModel({required this.customerData, required this.page});
  factory CustomerMainModel.fromJson(Map<String, dynamic> data) =>
      _$CustomerMainModelFromJson(data);
  Map<String, dynamic> toJson() => _$CustomerMainModelToJson(this);
}
