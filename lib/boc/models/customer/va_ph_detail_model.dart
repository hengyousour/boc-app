import 'package:json_annotation/json_annotation.dart';
part 'va_ph_detail_model.g.dart';

@JsonSerializable()
class VaPhDetailModel {
  final String? left;
  final String? right;

  const VaPhDetailModel({this.left, this.right});
  factory VaPhDetailModel.fromJson(Map<String, dynamic> data) =>
      _$VaPhDetailModelFromJson(data);
  Map<String, dynamic> toJson() => _$VaPhDetailModelToJson(this);
}
