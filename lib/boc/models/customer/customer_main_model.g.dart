// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_main_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerMainModel _$CustomerMainModelFromJson(Map<String, dynamic> json) =>
    CustomerMainModel(
      customerData: (json['customerData'] as List<dynamic>)
          .map((e) => CustomerModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      page: json['page'] as int,
    );

Map<String, dynamic> _$CustomerMainModelToJson(CustomerMainModel instance) =>
    <String, dynamic>{
      'customerData': instance.customerData.map((e) => e.toJson()).toList(),
      'page': instance.page,
    };
