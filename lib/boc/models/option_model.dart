import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import '../utils/icon_model_converter.dart';
part 'option_model.g.dart';

@JsonSerializable()
class OptionModel {
  @JsonKey(
      disallowNullValue: true,
      fromJson: IconModelConverter.fromJson,
      toJson: IconModelConverter.toJson)
  final IconData? icon;
  final String label;
  final dynamic value;
  const OptionModel({this.icon, required this.label, required this.value});

  factory OptionModel.fromJson(Map<String, dynamic> json) =>
      _$OptionModelFromJson(json);
  Map<String, dynamic> toJson() => _$OptionModelToJson(this);
}
