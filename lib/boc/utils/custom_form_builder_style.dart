import 'package:flutter/material.dart';
import 'package:mobile_core_app/boc/utils/constants.dart';

InputDecoration fbTextFieldStyle({
  required String label,
  required ThemeData theme,
  String? error,
}) {
  return InputDecoration(
    // fillColor: theme.primaryColor,
    // filled: true,
    isDense: true,
    labelText: label,
    errorText: error,
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
  );
}

InputDecoration fbDateTimePickerFieldStyle({
  required String label,
  required ThemeData theme,
  bool? allowClear = false,
  VoidCallback? onClear,
  String? error,
}) {
  return InputDecoration(
    contentPadding: const EdgeInsets.only(left: 12.0, top: 8.0),
    // fillColor: theme.primaryColor,
    // filled: true,
    labelText: label,
    errorText: error,
    suffixIcon: allowClear!
        ? GestureDetector(
            onTap: onClear, child: const Icon(CommonBocIcons.clearDateTime))
        : null,
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
  );
}

InputDecoration fbMultiSelectChipFieldStyle({
  required String label,
  required ThemeData theme,
  String? error,
}) {
  return InputDecoration(
    contentPadding: const EdgeInsets.symmetric(horizontal: 12.0),
    // fillColor: theme.primaryColor,
    // filled: true,
    // isDense: true,
    labelText: label,
    // labelStyle: theme.textTheme.bodyText1,
    isCollapsed: true,
    errorText: error,
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
  );
}

InputDecoration fbSelectOptionFieldStyle({
  required String label,
  required ThemeData theme,
  String? error,
}) {
  return InputDecoration(
    contentPadding: const EdgeInsets.only(left: 12.0, top: 8.0),
    // fillColor: theme.primaryColor,
    // filled: true,
    labelText: label,
    errorText: error,
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
  );
}

InputDecoration fbSearchTextFieldStyle({
  required String hintText,
  required ThemeData theme,
  String searchText = '',
  VoidCallback? onClear,
  Color? fillColor,
}) {
  return InputDecoration(
    fillColor: fillColor,
    filled: true,
    hintText: hintText,
    // labelStyle: theme.textTheme.bodyText1,
    suffixIcon: searchText.isNotEmpty
        ? IconButton(onPressed: onClear, icon: Icon(CommonBocIcons.clearSearch))
        : const Icon(CommonBocIcons.search),
    // border: OutlineInputBorder(
    //   borderRadius: BorderRadius.circular(5.0),
    //   borderSide: BorderSide.none,
    // ),
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
  );
}
