class BooleanModelConverter {
  static String FromBooleanToString(bool value) => value.toString();
  static bool FromStringToBoolean(String value) =>
      value == 'true' ? true : false;
}
