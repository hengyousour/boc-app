import 'package:flutter/material.dart';
import 'package:unicons/unicons.dart';

class CommonBocColors {
  //light
  static const primaryLight = Color(0xFFFFFFFF);
  static const secondaryLight = Color(0xFFFD1A19);
  static const scaffoldBgLight = Color(0xFFF4F3FA);
  static const fillInputLight = Color(0xFFFFFFFF);
  static const textLight = Color(0xFF002C46);
  static const iconLight = Color(0xFF002C46);
  static const successLight = Color(0xFF09B297);
  static const infoLight = Color(0xFF1e96fc);
  static const warningLight = Color(0xFFEEA012);
  static const errorLight = Color(0xFFFD1A19);
  //dark
  static const primaryDark = Color(0xFF0b090a);
  static const secondaryDark = Color(0xFFFD1A19);
  static const scaffoldBgDark = Color(0xFF161a1d);
  static const fillInputDark = Color(0xFFFFFFFF);
  static const textDark = Color(0xFFFFFFFF);
  static const iconDark = Color(0xFFFFFFFF);
  static const successDark = Color(0xFF09B297);
  static const infoDark = Color(0xFF1e96fc);
  static const warningDark = Color(0xFFEEA012);
  static const errorDark = Color(0xFFFD1A19);
}

class CommonMsg {
  static const noConnection =
      'Oops, Can\'t Connect To Server, Check Your Internet Connection And Try Again.';
  static const noPermission = 'Oops, Permission Denied.';
}

class CommonBocIcons {
  static const back = UniconsLine.play;
  static const add = UniconsLine.plus;
  static const edit = UniconsLine.pen;
  static const delete = UniconsLine.trash_alt;
  static const search = UniconsLine.search;
  static const clearSearch = UniconsLine.times;
  static const clearDateTime = UniconsLine.times;
  static const totalCustomers = UniconsLine.smile_wink;
  static const statusActive = UniconsLine.star;
  static const statusReactive = UniconsLine.sync_icon;
  static const statusCancel = UniconsLine.ban;
  static const statusClose = UniconsLine.invoice;
  static const report = UniconsLine.file_alt;
  static const next = UniconsLine.angle_right_b;
  static const submit = UniconsLine.check_circle;
  static const alertSuccess = UniconsLine.smile_wink;
  static const alertError = UniconsLine.sad_squint;
  static const alertInfo = UniconsLine.info_circle;
  static const alertWarning = UniconsLine.exclamation_triangle;
}
