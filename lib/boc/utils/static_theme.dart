import 'package:flutter/material.dart';
import '../../core/utils/constants.dart';
import 'constants.dart';

class StaticTheme {
  static final theme = StaticTheme();
  final ThemeData _lightTheme = ThemeData(
    primaryColor: CommonBocColors.primaryLight,
    brightness: Brightness.light,
    appBarTheme: const AppBarTheme(
      elevation: 0.0,
      color: CommonBocColors.secondaryLight,
      iconTheme: IconThemeData(color: CommonBocColors.primaryLight),
      titleTextStyle: TextStyle(
          fontFamily: CommonFonts.header,
          fontSize: CommonTextSize.header5TextSize,
          fontFamilyFallback: [CommonFonts.kantumruy],
          fontWeight: FontWeight.bold,
          color: CommonBocColors.primaryLight),
    ),
    //Note: For Date Time Picker theme
    colorScheme: const ColorScheme.light(
        primary: CommonBocColors.secondaryLight,
        secondary: CommonBocColors.primaryLight),
    scaffoldBackgroundColor: CommonBocColors.scaffoldBgLight,
    iconTheme: const IconThemeData(color: CommonBocColors.iconLight),
    textTheme: const TextTheme(
        bodyMedium: TextStyle(
            fontFamily: CommonFonts.body,
            fontFamilyFallback: [CommonFonts.kantumruy],
            color: CommonBocColors.textLight),
        labelLarge: TextStyle(
          fontWeight: FontWeight.bold,
          fontFamily: CommonFonts.body,
          fontFamilyFallback: [CommonFonts.kantumruy],
        ),
        //text input
        titleMedium: TextStyle(
            fontFamily: CommonFonts.body,
            fontFamilyFallback: [CommonFonts.kantumruy],
            color: CommonBocColors.textLight),
        titleLarge: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header6TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textLight),
        headlineSmall: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header5TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textLight),
        headlineMedium: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header4TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textLight),
        displaySmall: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header3TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textLight),
        displayMedium: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header5TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textLight),
        displayLarge: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header2TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textLight)),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      fillColor: CommonBocColors.fillInputLight,
      floatingLabelStyle:
          MaterialStateTextStyle.resolveWith((Set<MaterialState> states) {
        final Color color = states.contains(MaterialState.error)
            ? CommonBocColors.errorLight
            : CommonBocColors.textLight;
        return TextStyle(color: color);
      }),
      prefixIconColor: MaterialStateColor.resolveWith(
          (Set<MaterialState> states) => (states.contains(MaterialState.focused)
              ? CommonBocColors.iconLight
              : Colors.grey)),
      suffixIconColor: MaterialStateColor.resolveWith(
          (Set<MaterialState> states) => (states.contains(MaterialState.focused)
              ? CommonBocColors.iconLight
              : Colors.grey)),
      errorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonBocColors.errorLight)),
      focusedErrorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonBocColors.errorLight)),
      errorStyle: const TextStyle(color: CommonBocColors.errorLight),
      enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonBocColors.secondaryLight)),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: CommonBocColors.secondaryLight),
      ),
    ),
    textSelectionTheme:
        const TextSelectionThemeData(cursorColor: CommonBocColors.textLight),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: CommonBocColors.textLight,
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
          foregroundColor: CommonBocColors.primaryLight,
          backgroundColor: CommonBocColors.textLight,
          padding: const EdgeInsets.all(10.0),
          elevation: 0.0),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
          foregroundColor: CommonBocColors.textLight,
          side: const BorderSide(color: CommonBocColors.textLight),
          elevation: 0.0,
          padding: const EdgeInsets.all(20.0)),
    ),
  );

  final ThemeData _darkTheme = ThemeData(
    primaryColor: CommonBocColors.primaryDark,
    brightness: Brightness.light,
    appBarTheme: const AppBarTheme(
      elevation: 0.0,
      color: CommonBocColors.secondaryDark,
      iconTheme: IconThemeData(color: CommonBocColors.primaryDark),
      titleTextStyle: TextStyle(
          fontFamily: CommonFonts.header,
          fontSize: CommonTextSize.header5TextSize,
          fontFamilyFallback: [CommonFonts.kantumruy],
          fontWeight: FontWeight.bold,
          color: CommonBocColors.primaryDark),
    ),
    //Note: For Date Time Picker theme
    colorScheme:
        const ColorScheme.light(primary: CommonBocColors.secondaryDark),
    scaffoldBackgroundColor: CommonBocColors.scaffoldBgDark,
    iconTheme: const IconThemeData(color: CommonBocColors.iconDark),
    textTheme: const TextTheme(
        bodyMedium: TextStyle(
            fontFamily: CommonFonts.body,
            fontFamilyFallback: [CommonFonts.kantumruy],
            color: CommonBocColors.textDark),
        labelLarge: TextStyle(
          fontWeight: FontWeight.bold,
          fontFamily: CommonFonts.body,
          fontFamilyFallback: [CommonFonts.kantumruy],
        ),
        //text input
        titleMedium: TextStyle(
            fontFamily: CommonFonts.body,
            fontFamilyFallback: [CommonFonts.kantumruy],
            color: CommonBocColors.primaryDark),
        titleLarge: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header6TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textDark),
        headlineSmall: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header5TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textDark),
        headlineMedium: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header4TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textDark),
        displaySmall: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header3TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textDark),
        displayMedium: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header5TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textDark),
        displayLarge: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header2TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonBocColors.textDark)),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      fillColor: CommonBocColors.fillInputDark,
      floatingLabelStyle:
          MaterialStateTextStyle.resolveWith((Set<MaterialState> states) {
        final Color color = states.contains(MaterialState.error)
            ? CommonBocColors.errorDark
            : CommonBocColors.primaryDark;
        return TextStyle(color: color);
      }),
      prefixIconColor: MaterialStateColor.resolveWith(
          (Set<MaterialState> states) => (states.contains(MaterialState.focused)
              ? CommonBocColors.primaryDark
              : Colors.grey)),
      suffixIconColor: MaterialStateColor.resolveWith(
          (Set<MaterialState> states) => (states.contains(MaterialState.focused)
              ? CommonBocColors.primaryDark
              : Colors.grey)),
      errorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonBocColors.errorDark)),
      focusedErrorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonBocColors.errorDark)),
      errorStyle: const TextStyle(color: CommonBocColors.errorDark),
      enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonBocColors.secondaryDark)),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: CommonBocColors.secondaryDark),
      ),
    ),
    textSelectionTheme:
        const TextSelectionThemeData(cursorColor: CommonBocColors.primaryDark),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: CommonBocColors.primaryDark,
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
          foregroundColor: CommonBocColors.primaryDark,
          backgroundColor: CommonBocColors.textDark,
          padding: const EdgeInsets.all(10.0),
          elevation: 0.0),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
          foregroundColor: CommonBocColors.textDark,
          side: const BorderSide(color: CommonBocColors.textDark),
          elevation: 0.0,
          padding: const EdgeInsets.all(20.0)),
    ),
  );

  ThemeData phoneFieldTheme({required ThemeData theme}) => theme.copyWith(
        inputDecorationTheme: const InputDecorationTheme(
          border: OutlineInputBorder(),
          enabledBorder: OutlineInputBorder(),
        ),
      );

  ThemeData get lightTheme => _lightTheme;
  ThemeData get darkTheme => _darkTheme;
}
