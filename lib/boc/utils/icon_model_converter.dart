import 'package:flutter/material.dart';

class IconModelConverter {
  static IconData? fromJson(IconData? icon) => icon;
  static IconData? toJson(IconData? icon) => icon;
}
