import '../models/option_model.dart';

class StaticOptions {
  static List<OptionModel> genderOption = const [
    OptionModel(label: 'Male', value: 'Male'),
    OptionModel(label: 'Female', value: 'Female'),
    OptionModel(label: 'Other', value: 'Other'),
  ];

  static List<OptionModel> referOption = const [
    OptionModel(label: 'Cons', value: 'cons'),
    OptionModel(label: 'Sur', value: 'sur'),
    OptionModel(label: 'Field', value: 'field'),
  ];

  static List<OptionModel> customerStatusOption = const [
    OptionModel(label: 'Active', value: 'Active'),
    OptionModel(label: 'Cancel', value: 'Cancel'),
    OptionModel(label: 'Closed', value: 'Closed'),
    OptionModel(label: 'All', value: ''),
  ];
}
