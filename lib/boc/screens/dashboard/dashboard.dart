import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:mobile_core_app/boc/utils/constants.dart';
import 'package:provider/provider.dart';
import '../../../core/providers/connection_provider.dart';
import '../../../core/utils/alert.dart';
import '../../widgets/dashboard/overview_skeleton.dart';
import '/boc/providers/customer_overview_provider.dart';
import '../../models/dashboard/dashboard_card_model.dart';
import '../../widgets/dashboard/custom_card.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  late CustomerOverviewProvider readProvider =
      context.read<CustomerOverviewProvider>();

  @override
  void initState() {
    super.initState();
    readProvider = context.read<CustomerOverviewProvider>();
    readProvider.initUserDocWithRoles();
    readProvider.subscribeOverview();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (context.watch<CustomerOverviewProvider>().isOverviewDataUpdate) {
      readProvider.initData();
    }
  }

  @override
  void dispose() {
    super.dispose();
    readProvider.clearOverviewState();
    readProvider.unSubscribe();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final deviceData = MediaQuery.of(context);
    final size = MediaQuery.of(context).size;
    return FadeIn(
      duration: const Duration(milliseconds: 1200),
      child: Column(children: [
        SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Wrap(
                        children: [
                          Text(
                            'Hello, ',
                            style: theme.textTheme.headlineSmall!
                                .copyWith(fontWeight: FontWeight.normal),
                          ),
                          Text(
                            context
                                .read<CustomerOverviewProvider>()
                                .fullName
                                .toUpperCase(),
                            style: theme.textTheme.headlineSmall,
                          ),
                        ],
                      ),
                      Text(
                        'Start with actions below, Have a great day.',
                        style: theme.textTheme.titleLarge!
                            .copyWith(fontWeight: FontWeight.normal),
                      ),
                      const SizedBox(height: 10.0),
                      Wrap(
                        spacing: 10.0,
                        runSpacing: 10.0,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 4.5,
                            height: 48.0,
                            child: ElevatedButton(
                              onPressed: () {
                                // check is app connect to server or not
                                bool isConnected = context
                                    .read<ConnectionProvider>()
                                    .isConnected;
                                if (!isConnected) {
                                  return Alert().show(
                                      message: CommonMsg.noConnection,
                                      alertIcon: CommonBocIcons.alertError,
                                      alertBackGroundColor:
                                          CommonBocColors.errorLight,
                                      context: context);
                                }

                                Navigator.of(context)
                                    .pushNamed('/boc/customer');
                              },
                              child: const Text(
                                'CUSTOMER',
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 4.5,
                            height: 48.0,
                            child: ElevatedButton(
                              onPressed: () {
                                // check is app connect to server or not
                                bool isConnected = context
                                    .read<ConnectionProvider>()
                                    .isConnected;
                                if (!isConnected) {
                                  return Alert().show(
                                      message: CommonMsg.noConnection,
                                      alertIcon: CommonBocIcons.alertError,
                                      alertBackGroundColor:
                                          CommonBocColors.errorLight,
                                      context: context);
                                }
                                Navigator.of(context).pushNamed('/boc/reports');
                              },
                              child: const Text(
                                'REPORTS',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Lottie.asset(
                      'lib/boc/assets/json/dashboard/friendly-faces.json',
                      fit: BoxFit.cover,
                      height: deviceData.orientation.name == 'landscape'
                          ? size.height * 0.4
                          : size.height * 0.3),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            padding: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
                color: theme.colorScheme.primary,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                )),
            child: SingleChildScrollView(
              controller: ScrollController(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Overview',
                    style: theme.textTheme.titleLarge!
                        .copyWith(color: theme.primaryColor),
                  ),
                  const SizedBox(height: 10),
                  FutureBuilder(
                    future: readProvider.totalCustomer,
                    builder: (context, AsyncSnapshot<int> snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting ||
                          !snapshot.hasData) {
                        return const OverviewSkeleton(
                          customCard: DashboardCardModel(
                              iconName: CommonBocIcons.totalCustomers),
                        );
                      }
                      return CustomCard(
                        customCard: DashboardCardModel(
                          title: 'Total',
                          color: theme.iconTheme.color,
                          iconName: CommonBocIcons.totalCustomers,
                          subtitle: snapshot.data,
                        ),
                      );
                    },
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        child: FutureBuilder(
                            future: context
                                .read<CustomerOverviewProvider>()
                                .totalActiveCustomer,
                            builder: (context, AsyncSnapshot<int> snapshot) {
                              if (snapshot.connectionState ==
                                      ConnectionState.waiting ||
                                  !snapshot.hasData) {
                                return const OverviewSkeleton(
                                  customCard: DashboardCardModel(
                                      iconName: CommonBocIcons.statusActive),
                                );
                              }
                              return CustomCard(
                                customCard: DashboardCardModel(
                                  title: 'Active',
                                  color: theme.iconTheme.color,
                                  iconName: CommonBocIcons.statusActive,
                                  subtitle: snapshot.data,
                                ),
                              );
                            }),
                      ),
                      Expanded(
                        child: FutureBuilder(
                            future: readProvider.totalCancelCustomer,
                            builder: (context, AsyncSnapshot<int> snapshot) {
                              if (snapshot.connectionState ==
                                      ConnectionState.waiting ||
                                  !snapshot.hasData) {
                                return const OverviewSkeleton(
                                  customCard: DashboardCardModel(
                                      iconName: CommonBocIcons.statusCancel),
                                );
                              }
                              return CustomCard(
                                customCard: DashboardCardModel(
                                  title: 'Cancel',
                                  color: theme.iconTheme.color,
                                  iconName: CommonBocIcons.statusCancel,
                                  subtitle: snapshot.data,
                                ),
                              );
                            }),
                      ),
                      Expanded(
                        child: FutureBuilder(
                            future: readProvider.totalClosedCustomer,
                            builder: (context, AsyncSnapshot<int> snapshot) {
                              if (snapshot.connectionState ==
                                      ConnectionState.waiting ||
                                  !snapshot.hasData) {
                                return const OverviewSkeleton(
                                  customCard: DashboardCardModel(
                                      iconName: CommonBocIcons.statusClose),
                                );
                              }
                              return CustomCard(
                                customCard: DashboardCardModel(
                                  title: 'Closed',
                                  color: theme.iconTheme.color,
                                  iconName: CommonBocIcons.statusClose,
                                  subtitle: snapshot.data,
                                ),
                              );
                            }),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        )
      ]),
    );
  }
}
