import 'package:flutter/material.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import '../../../core/providers/connection_provider.dart';
import '../../../core/utils/alert.dart';
import '../../../core/widgets/real_time_connection.dart';
import '../../models/reports/customer-list/customer_list_form_report_model.dart';
import '../../providers/reports/customer_list_report_provider.dart';
import '../../utils/constants.dart';
import '../../widgets/components/custom_app_bar.dart';
import '../../widgets/reports/customer_list_form.dart';

class CustomerListReport extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbCustomerListReportKey =
      GlobalKey<FormBuilderState>();

  const CustomerListReport({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isConnected =
        context.select<ConnectionProvider, bool>((state) => state.isConnected);
    final ThemeData theme = Theme.of(context);
    context.read<CustomerListReportProvider>().initData();
    return Scaffold(
      appBar: customAppBar(
          title: "Customer List Report",
          titleColor: theme.primaryColor,
          customLeading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
              context
                  .read<CustomerListReportProvider>()
                  .clearCustomerListReportState();
            },
            icon: RotatedBox(
              quarterTurns: 2,
              child: Icon(
                CommonBocIcons.back,
                color: theme.primaryColor,
              ),
            ),
          ),
          customActions: [
            IconButton(
              onPressed: () async {
                // check is app connect to server or not
                bool isConnected =
                    context.read<ConnectionProvider>().isConnected;
                if (!isConnected) {
                  return Alert().show(
                      message: CommonMsg.noConnection,
                      alertIcon: CommonBocIcons.alertError,
                      alertBackGroundColor: CommonBocColors.errorLight,
                      context: context);
                }
                if (_fbCustomerListReportKey.currentState!.saveAndValidate()) {
                  //temp data from user input
                  Map<String, dynamic> tempData =
                      Map.of(_fbCustomerListReportKey.currentState!.value);

                  //prepare data
                  DateTimeRange date = tempData['dateRange'];
                  DateTime startOfDay = date.start.startOfDay;
                  DateTime endOfDay = date.end.endOfDay;

                  CustomerListFormReportModel data =
                      CustomerListFormReportModel(
                    fromDate: startOfDay,
                    toDate: endOfDay,
                    status: tempData['customerStatus'],
                  );
                  //call submit method
                  await context
                      .read<CustomerListReportProvider>()
                      .submit(formDoc: data);
                }
              },
              icon: Icon(
                CommonBocIcons.submit,
                color: theme.primaryColor,
              ),
            ),
          ]) as PreferredSizeWidget?,
      body: FormBuilder(
        key: _fbCustomerListReportKey,
        child: const CustomerListForm(),
      ),
      bottomSheet:
          !isConnected ? const RealTimeConnection() : const SizedBox.shrink(),
    );
  }
}
