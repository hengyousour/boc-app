import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../core/providers/connection_provider.dart';
import '../../../core/widgets/real_time_connection.dart';
import '../../providers/customer_overview_provider.dart';
import '../../providers/reports/reports_provider.dart';
import '../../utils/constants.dart';
import '../../widgets/components/custom_app_bar.dart';
import '../../widgets/reports/report_list.dart';

class Reports extends StatelessWidget {
  const Reports({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isConnected =
        context.select<ConnectionProvider, bool>((state) => state.isConnected);
    final theme = Theme.of(context);
    context
        .read<ReportsProvider>()
        .initData(roleUser: context.read<CustomerOverviewProvider>().userRole);
    return Scaffold(
      appBar: customAppBar(
        title: "Reports",
        titleColor: theme.primaryColor,
        customLeading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: RotatedBox(
            quarterTurns: 2,
            child: Icon(
              CommonBocIcons.back,
              color: theme.primaryColor,
            ),
          ),
        ),
      ) as PreferredSizeWidget?,
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView.builder(
          itemCount: context.read<ReportsProvider>().reportList.length,
          itemBuilder: (context, index) => ReportList(
            report: context.read<ReportsProvider>().reportList[index],
          ),
        ),
      ),
      bottomSheet:
          !isConnected ? const RealTimeConnection() : const SizedBox.shrink(),
    );
  }
}
