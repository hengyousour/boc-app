import 'package:flutter/material.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import '../../../core/providers/connection_provider.dart';
import '../../../core/utils/alert.dart';
import '../../../core/widgets/real_time_connection.dart';
import '../../models/reports/biorc/biorc_form_report_model.dart';
import '../../providers/reports/biorc_report_provider.dart';
import '../../utils/constants.dart';
import '../../widgets/components/custom_app_bar.dart';
import '../../widgets/reports/biorc_form.dart';

class BiorcReport extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbBiorcReportKey =
      GlobalKey<FormBuilderState>();

  const BiorcReport({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isConnected =
        context.select<ConnectionProvider, bool>((state) => state.isConnected);
    final ThemeData theme = Theme.of(context);
    context.read<BiorcReportProvider>().initData();
    return Scaffold(
      appBar: customAppBar(
          title: "Brief Information Of Referral Case Report",
          titleColor: theme.primaryColor,
          customLeading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
              context.read<BiorcReportProvider>().clearBiorcReportState();
            },
            icon: RotatedBox(
              quarterTurns: 2,
              child: Icon(
                CommonBocIcons.back,
                color: theme.primaryColor,
              ),
            ),
          ),
          customActions: [
            IconButton(
              onPressed: () async {
                // check is app connect to server or not
                bool isConnected =
                    context.read<ConnectionProvider>().isConnected;
                if (!isConnected) {
                  return Alert().show(
                      message: CommonMsg.noConnection,
                      alertIcon: CommonBocIcons.alertError,
                      alertBackGroundColor: CommonBocColors.errorLight,
                      context: context);
                }
                if (_fbBiorcReportKey.currentState!.saveAndValidate()) {
                  //temp data from user input
                  Map<String, dynamic> tempData =
                      Map.of(_fbBiorcReportKey.currentState!.value);

                  //prepare data
                  DateTimeRange date = tempData['dateRange'];
                  DateTime startOfDay = date.start.startOfDay;
                  DateTime endOfDay = date.end.endOfDay;

                  BiorcFormReportModel data = BiorcFormReportModel(
                    fromDate: startOfDay,
                    toDate: endOfDay,
                  );
                  //call submit method
                  await context
                      .read<BiorcReportProvider>()
                      .submit(formDoc: data);
                }
              },
              icon: Icon(
                CommonBocIcons.submit,
                color: theme.primaryColor,
              ),
            ),
          ]) as PreferredSizeWidget?,
      body: FormBuilder(
        key: _fbBiorcReportKey,
        child: const BiorcForm(),
      ),
      bottomSheet:
          !isConnected ? const RealTimeConnection() : const SizedBox.shrink(),
    );
  }
}
