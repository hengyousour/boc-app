import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import '../../../core/providers/connection_provider.dart';
import '../../../core/utils/alert.dart';
import '../../../core/widgets/real_time_connection.dart';
import '../../models/reports/customer-outstanding-list/customer_outstanding_list_form_report_model.dart';
import '../../providers/reports/customer_outstanding_list_report_provider.dart';
import '../../utils/constants.dart';
import '../../widgets/components/custom_app_bar.dart';
import '../../widgets/reports/customer_outstanding_list_form.dart';

class CustomerOutstandingListReport extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbCustomerOutstandingListReportKey =
      GlobalKey<FormBuilderState>();

  const CustomerOutstandingListReport({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isConnected =
        context.select<ConnectionProvider, bool>((state) => state.isConnected);
    final ThemeData theme = Theme.of(context);
    context.read<CustomerOutstandingListReportProvider>().initData();
    return Scaffold(
      appBar: customAppBar(
          title: "Customer Outstanding List Report",
          titleColor: theme.primaryColor,
          customLeading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
              context
                  .read<CustomerOutstandingListReportProvider>()
                  .clearCustomerOutstandingListReportState();
            },
            icon: RotatedBox(
              quarterTurns: 2,
              child: Icon(
                CommonBocIcons.back,
                color: theme.primaryColor,
              ),
            ),
          ),
          customActions: [
            IconButton(
              onPressed: () async {
                // check is app connect to server or not
                bool isConnected =
                    context.read<ConnectionProvider>().isConnected;
                if (!isConnected) {
                  return Alert().show(
                      message: CommonMsg.noConnection,
                      alertIcon: CommonBocIcons.alertError,
                      alertBackGroundColor: CommonBocColors.errorLight,
                      context: context);
                }
                if (_fbCustomerOutstandingListReportKey.currentState!
                    .saveAndValidate()) {
                  //temp data from user input
                  Map<String, dynamic> tempData = Map.of(
                      _fbCustomerOutstandingListReportKey.currentState!.value);

                  //prepare data
                  DateTime date = tempData['date'];
                  CustomerOutstandingListFormReportModel data =
                      CustomerOutstandingListFormReportModel(
                    date: date,
                  );
                  //call submit method
                  await context
                      .read<CustomerOutstandingListReportProvider>()
                      .submit(formDoc: data);
                }
              },
              icon: Icon(
                CommonBocIcons.submit,
                color: theme.primaryColor,
              ),
            ),
          ]) as PreferredSizeWidget?,
      body: FormBuilder(
        key: _fbCustomerOutstandingListReportKey,
        child: const CustomerOutstandingListForm(),
      ),
      bottomSheet:
          !isConnected ? const RealTimeConnection() : const SizedBox.shrink(),
    );
  }
}
