import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '../../../core/providers/connection_provider.dart';
import '../../../core/utils/alert.dart';
import '../../../core/widgets/real_time_connection.dart';
import '/boc/models/customer/eyes_detail_model.dart';
import '/boc/providers/customer_provider.dart';
import 'package:provider/provider.dart';
import '../../models/customer/va_ph_detail_model.dart';
import '../../models/customer/customer_model.dart';
import '../../models/customer/refer_detail_model.dart';
import '../../utils/constants.dart';
import '../../widgets/components/custom_app_bar.dart';
import '../../widgets/customer/create_customer_form.dart';

class CreateCustomer extends StatelessWidget {
  const CreateCustomer({Key? key}) : super(key: key);
  static final GlobalKey<FormBuilderState> _fbCreateCustomerKey =
      GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    bool isConnected =
        context.select<ConnectionProvider, bool>((state) => state.isConnected);
    final theme = Theme.of(context);
    // final isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    return GestureDetector(
      onTap: () => WidgetsBinding.instance.focusManager.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: customAppBar(
            title: "Create Customer",
            titleColor: theme.primaryColor,
            customLeading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: RotatedBox(
                quarterTurns: 2,
                child: Icon(CommonBocIcons.back, color: theme.primaryColor),
              ),
            ),
            customActions: <Widget>[]) as PreferredSizeWidget?,
        body: FormBuilder(
          key: _fbCreateCustomerKey,
          child: Column(
            children: [
              const Expanded(
                child: CreateCustomerForm(),
              ),
              SafeArea(
                child: Container(
                  // margin: isKeyboard ? EdgeInsets.all(10.0) : EdgeInsets.all(0.0),
                  margin: const EdgeInsets.only(
                      left: 10.0, right: 10.0, bottom: 10.0),
                  decoration: BoxDecoration(
                    color: theme.iconTheme.color,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextButton(
                          onPressed: () {
                            _fbCreateCustomerKey.currentState!.reset();
                          },
                          style: TextButton.styleFrom(
                              foregroundColor: theme.primaryColor,
                              padding: const EdgeInsets.all(30.0)),
                          child: const Text('Reset'),
                        ),
                      ),
                      Expanded(
                        child: TextButton(
                          onPressed: () {
                            // check is app connect to server or not
                            bool isConnected =
                                context.read<ConnectionProvider>().isConnected;
                            if (!isConnected) {
                              return Alert().show(
                                  message: CommonMsg.noConnection,
                                  alertIcon: CommonBocIcons.alertError,
                                  alertBackGroundColor:
                                      CommonBocColors.errorLight,
                                  context: context);
                            }
                            if (_fbCreateCustomerKey.currentState!
                                .saveAndValidate()) {
                              final Map<String, dynamic> tempDoc =
                                  _fbCreateCustomerKey.currentState!.value;
                              // prepare from doc
                              final CustomerModel formDoc = CustomerModel(
                                name: tempDoc['name'],
                                age: tempDoc['age'],
                                gender: tempDoc['gender'],
                                telephone: tempDoc['telephone'],
                                date: tempDoc['date'],
                                refer:
                                    ReferDetailModel.fromJson(tempDoc['refer']),
                                va: VaPhDetailModel(
                                    left: tempDoc['va-l'],
                                    right: tempDoc['va-r']),
                                ph: VaPhDetailModel(
                                    left: tempDoc['ph-l'],
                                    right: tempDoc['ph-r']),
                                rightEye: EyesDetailModel(
                                  sph: tempDoc['right-sph'],
                                  cyl: tempDoc['right-cyl'],
                                  axis: tempDoc['right-axis'],
                                  va: tempDoc['right-va'],
                                ),
                                leftEye: EyesDetailModel(
                                  sph: tempDoc['left-sph'],
                                  cyl: tempDoc['left-cyl'],
                                  axis: tempDoc['left-axis'],
                                  va: tempDoc['left-va'],
                                ),
                                add: tempDoc['add'],
                                distancePd: tempDoc['distancePd'],
                                nearPd: tempDoc['nearPd'],
                                status: 'Active',
                              );
                              context
                                  .read<CustomerProvider>()
                                  .insertCustomer(doc: formDoc.toJson())
                                  .then((value) {
                                Alert().show(
                                  message:
                                      'Good job, your customer has been successfully created.',
                                  alertIcon: CommonBocIcons.alertSuccess,
                                  alertBackGroundColor:
                                      CommonBocColors.successLight,
                                  context: context,
                                  duration: const Duration(seconds: 1),
                                );
                                //reset from
                                _fbCreateCustomerKey.currentState!.reset();
                              }).catchError((error) {
                                Alert().show(
                                    message: error.message,
                                    alertIcon: CommonBocIcons.alertError,
                                    alertBackGroundColor:
                                        CommonBocColors.errorLight,
                                    context: context);
                              });
                            }
                          },
                          style: TextButton.styleFrom(
                              foregroundColor: theme.primaryColor,
                              padding: const EdgeInsets.all(30.0)),
                          child: const Text('Create'),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        bottomSheet:
            !isConnected ? const RealTimeConnection() : const SizedBox.shrink(),
      ),
    );
  }
}
