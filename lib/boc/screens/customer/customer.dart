import 'package:dice_bear/dice_bear.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../../core/providers/connection_provider.dart';
import '../../../core/utils/alert.dart';
import '../../../core/utils/core_convert_date_time.dart';
import '../../../core/widgets/real_time_connection.dart';
import '../../providers/customer_overview_provider.dart';
import '../../widgets/components/alert_dialog_content.dart';
import '../../widgets/components/alert_dialog_title.dart';
import '../../widgets/components/slide_action.dart';
import '../../widgets/customer/customer_skeleton_list.dart';
import '/boc/providers/customer_provider.dart';
import '../../models/customer/customer_model.dart';
import '../../utils/debouncer.dart';
import '/boc/utils/custom_form_builder_style.dart';
import '../../utils/constants.dart';
import '../../widgets/components/custom_app_bar.dart';

class Customer extends StatelessWidget {
  const Customer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormBuilderState> fbCustomerKey =
        GlobalKey<FormBuilderState>();
    final CustomerProvider readProvider = context.read<CustomerProvider>();

    bool isConnected =
        context.select<ConnectionProvider, bool>((state) => state.isConnected);
    final theme = Theme.of(context);
    return GestureDetector(
      onTap: () => WidgetsBinding.instance.focusManager.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: customAppBar(
            title: "Customer",
            titleColor: theme.primaryColor,
            customLeading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
                readProvider.clearCustomerState();
              },
              icon: RotatedBox(
                quarterTurns: 2,
                child: Icon(
                  CommonBocIcons.back,
                  color: theme.primaryColor,
                ),
              ),
            ),
            customActions: <Widget>[
              IconButton(
                  color: theme.primaryColor,
                  onPressed: () {
                    // check is app connect to server or not
                    bool isConnected =
                        context.read<ConnectionProvider>().isConnected;
                    if (!isConnected) {
                      return Alert().show(
                          message: CommonMsg.noConnection,
                          alertIcon: CommonBocIcons.alertError,
                          alertBackGroundColor: CommonBocColors.errorLight,
                          context: context);
                    }
                    Navigator.pushNamed(context, '/boc/create-customer');
                  },
                  icon: const Icon(CommonBocIcons.add))
            ]) as PreferredSizeWidget?,
        body: FormBuilder(
            key: fbCustomerKey,
            child: Column(children: const [CustomerFilter(), CustomerBody()])),
        bottomSheet:
            !isConnected ? const RealTimeConnection() : const SizedBox.shrink(),
      ),
    );
  }
}

class CustomerBody extends StatefulWidget {
  const CustomerBody({
    Key? key,
  }) : super(key: key);

  @override
  State<CustomerBody> createState() => _CustomerBodyState();
}

class _CustomerBodyState extends State<CustomerBody> {
  final ScrollController _scrollControllerActive = ScrollController();
  final ScrollController _scrollControllerCancel = ScrollController();
  final ScrollController _scrollControllerClosed = ScrollController();
  final ScrollController _scrollControllerAll = ScrollController();
  late final CustomerProvider readProvider;

  @override
  void initState() {
    super.initState();
    readProvider = context.read<CustomerProvider>();
    readProvider.subscribe();
    //listen load more data
    _scrollControllerActive.addListener(() {
      if (_scrollControllerActive.position.maxScrollExtent ==
          _scrollControllerActive.offset) {
        readProvider.loadMore(
            status: readProvider.status,
            page: readProvider.pages[0],
            pageSize: 15);
      }
    });
    _scrollControllerCancel.addListener(() {
      if (_scrollControllerCancel.position.maxScrollExtent ==
          _scrollControllerCancel.offset) {
        readProvider.loadMore(
            status: readProvider.status,
            page: readProvider.pages[1],
            pageSize: 15);
      }
    });
    _scrollControllerClosed.addListener(() {
      if (_scrollControllerClosed.position.maxScrollExtent ==
          _scrollControllerClosed.offset) {
        readProvider.loadMore(
            status: readProvider.status,
            page: readProvider.pages[2],
            pageSize: 15);
      }
    });
    _scrollControllerAll.addListener(() {
      if (_scrollControllerAll.position.maxScrollExtent ==
          _scrollControllerAll.offset) {
        readProvider.loadMore(
            status: readProvider.status,
            page: readProvider.pages[3],
            pageSize: 15);
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (context.watch<CustomerProvider>().isCustomerDataUpdate) {
      context.read<CustomerProvider>().initData(
          pageSize: readProvider.pageSize,
          status: readProvider.status,
          date: readProvider.date,
          searchText: readProvider.searchText);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _scrollControllerActive.dispose();
    _scrollControllerCancel.dispose();
    _scrollControllerClosed.dispose();
    _scrollControllerAll.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Expanded(
      child: DefaultTabController(
        length: readProvider.customerStatusList.length,
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                TabBar(
                    indicatorColor: theme.colorScheme.primary,
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    onTap: ((index) {
                      String? searchText =
                          FormBuilder.of(context)!.fields['search']!.value;
                      DateTime? date =
                          FormBuilder.of(context)!.fields['date']!.value;
                      // start filter
                      readProvider
                          .filter(
                        searchText: searchText ?? '',
                        date: date,
                        status: readProvider.customerStatusList[index].value,
                      )
                          .catchError((error) {
                        Alert().show(
                            message: error.message,
                            alertIcon: CommonBocIcons.alertError,
                            alertBackGroundColor: CommonBocColors.errorLight,
                            context: context);
                      });
                    }),
                    labelColor: theme.iconTheme.color,
                    tabs: readProvider.customerStatusList
                        .map((status) => Tab(
                              text: status.label,
                            ))
                        .toList()),
                Expanded(
                    child: StreamBuilder<List<CustomerModel>>(
                        stream: readProvider.streamActive,
                        builder: (context,
                            AsyncSnapshot<List<CustomerModel>> snapshotActive) {
                          return StreamBuilder<List<CustomerModel>>(
                              stream: readProvider.streamCancel,
                              builder: (context, snapshotCancel) {
                                return StreamBuilder<List<CustomerModel>>(
                                    stream: readProvider.streamClosed,
                                    builder: (context,
                                        AsyncSnapshot<List<CustomerModel>>
                                            snapshotClosed) {
                                      return StreamBuilder<List<CustomerModel>>(
                                          stream: readProvider.streamAll,
                                          builder: (context,
                                              AsyncSnapshot<List<CustomerModel>>
                                                  snapshotAll) {
                                            return TabBarView(
                                              children: [
                                                CustomerBodyList(
                                                  storageKey: 'Active',
                                                  data:
                                                      snapshotActive.data ?? [],
                                                  scrollController:
                                                      _scrollControllerActive,
                                                  connectionState:
                                                      snapshotActive
                                                          .connectionState,
                                                  hasMoreData: readProvider
                                                      .hasMore[0]['Active']!,
                                                ),
                                                CustomerBodyList(
                                                  storageKey: 'Cancel',
                                                  data:
                                                      snapshotCancel.data ?? [],
                                                  scrollController:
                                                      _scrollControllerCancel,
                                                  connectionState:
                                                      snapshotCancel
                                                          .connectionState,
                                                  hasMoreData: readProvider
                                                      .hasMore[1]['Cancel']!,
                                                ),
                                                CustomerBodyList(
                                                  storageKey: 'Closed',
                                                  data:
                                                      snapshotClosed.data ?? [],
                                                  scrollController:
                                                      _scrollControllerClosed,
                                                  connectionState:
                                                      snapshotClosed
                                                          .connectionState,
                                                  hasMoreData: readProvider
                                                      .hasMore[2]['Closed']!,
                                                ),
                                                CustomerBodyList(
                                                  storageKey: 'All',
                                                  data: snapshotAll.data ?? [],
                                                  scrollController:
                                                      _scrollControllerAll,
                                                  connectionState: snapshotAll
                                                      .connectionState,
                                                  hasMoreData: readProvider
                                                      .hasMore[3]['All']!,
                                                ),
                                              ],
                                            );
                                          });
                                    });
                              });
                        })),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CustomerBodyList extends StatefulWidget {
  final String storageKey;
  final ScrollController scrollController;
  final ConnectionState connectionState;
  final List<CustomerModel> data;
  final bool hasMoreData;

  const CustomerBodyList(
      {Key? key,
      required this.storageKey,
      required this.scrollController,
      required this.connectionState,
      required this.data,
      required this.hasMoreData})
      : super(key: key);

  @override
  State<CustomerBodyList> createState() => _CustomerBodyListState();
}

class _CustomerBodyListState extends State<CustomerBodyList>
    with AutomaticKeepAliveClientMixin<CustomerBodyList> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final size = MediaQuery.of(context).size;

    if (widget.connectionState == ConnectionState.waiting) {
      return ListView.builder(
          itemCount: 15,
          itemBuilder: ((context, index) => CustomerSkeletonList(
                color: theme.primaryColor,
              )));
    }

    if (widget.data.isEmpty) {
      return Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Data Not Found', style: theme.textTheme.titleLarge),
              Lottie.asset(
                'lib/boc/assets/json/customers/data-404.json',
                fit: BoxFit.cover,
                height: size.height / 4,
                repeat: false,
              )
            ],
          ),
        ),
      );
    }

    return ListView.builder(
        key: PageStorageKey<String>(widget.storageKey),
        controller: widget.scrollController,
        itemCount: widget.data.length + 1,
        itemBuilder: (_, i) {
          if (i < widget.data.length) {
            final CustomerModel _customer = widget.data[i];
            final Avatar _avatar = DiceBearBuilder(
              sprite: DiceBearSprite.bigEarsNeutral, radius: 10,
              seed: i
                  .toString(), // Default seed is an empty string if you don't set it
            ).build();
            Color statusColor;
            switch (_customer.status) {
              case 'Cancel':
                statusColor = CommonBocColors.warningLight;
                break;
              case 'Closed':
                statusColor = CommonBocColors.errorLight;
                break;
              default:
                statusColor = CommonBocColors.successLight;
            }
            return Slidable(
              actionPane: const SlidableDrawerActionPane(),
              actionExtentRatio: 0.25,
              actions: context.read<CustomerOverviewProvider>().userRole ==
                      'admin'
                  ? [
                      SlideAction(
                        child: const SlideActionChild(
                          text: 'Edit',
                          iconName: CommonBocIcons.edit,
                          bgColor: CommonBocColors.warningLight,
                        ),
                        onTap: () async {
                          // check is app connect to server or not
                          bool isConnected =
                              context.read<ConnectionProvider>().isConnected;
                          if (!isConnected) {
                            return Alert().show(
                                message: CommonMsg.noConnection,
                                alertIcon: CommonBocIcons.alertError,
                                alertBackGroundColor:
                                    CommonBocColors.errorLight,
                                context: context);
                          }
                          await Future.delayed(
                              const Duration(milliseconds: 300));
                          Navigator.pushNamed(context, '/boc/edit-customer',
                              arguments: _customer);
                        },
                      ),
                      SlideAction(
                          child: const SlideActionChild(
                            text: 'Delete',
                            iconName: CommonBocIcons.delete,
                            bgColor: CommonBocColors.errorLight,
                          ),
                          onTap: () {
                            // check is app connect to server or not
                            bool isConnected =
                                context.read<ConnectionProvider>().isConnected;
                            if (!isConnected) {
                              return Alert().show(
                                  message: CommonMsg.noConnection,
                                  alertIcon: CommonBocIcons.alertError,
                                  alertBackGroundColor:
                                      CommonBocColors.errorLight,
                                  context: context);
                            }
                            Alert().showAlertDialog(
                                title: const AlertDialogTitle(
                                    title: 'Confirmation'),
                                content: const AlertDialogContent(
                                  textContent: 'Are you sure to delete ?',
                                ),
                                context: context,
                                onCancel: () =>
                                    Navigator.of(context, rootNavigator: true)
                                        .pop(),
                                onAgree: () {
                                  context
                                      .read<CustomerProvider>()
                                      .removeCustomer(
                                        id: _customer.id!,
                                      )
                                      .then((value) {
                                    Alert().show(
                                      message:
                                          'Good job, your customer has been successfully deleted.',
                                      alertIcon: CommonBocIcons.alertSuccess,
                                      alertBackGroundColor:
                                          CommonBocColors.successLight,
                                      context: context,
                                      duration: const Duration(seconds: 1),
                                    );

                                    //close dailog
                                    Navigator.of(context, rootNavigator: true)
                                        .pop();
                                  }).catchError((error) {
                                    Alert().show(
                                        message: error.message,
                                        alertIcon: CommonBocIcons.alertError,
                                        alertBackGroundColor:
                                            CommonBocColors.errorLight,
                                        context: context);
                                  });
                                },
                                agreeBtnLabel: 'Yes',
                                agreeBtnColor: theme.iconTheme.color!);
                          })
                    ]
                  : null,
              child: Card(
                color: theme.primaryColor,
                elevation: 0.0,
                child: InkWell(
                  borderRadius: BorderRadius.circular(4.0),
                  onTap: () => Navigator.pushNamed(
                      context, '/boc/show-customer',
                      arguments: _customer),
                  child: Column(
                    children: [
                      ListTile(
                        isThreeLine: _customer.closedDate != null,
                        leading: widget.storageKey == 'All'
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    _avatar?.toImage(width: 30.0) ??
                                        Image.asset(
                                            'assets/images/no-image.png'),
                                    Container(
                                        margin: EdgeInsets.only(top: 3),
                                        padding: const EdgeInsets.all(3.5),
                                        decoration: BoxDecoration(
                                            color: statusColor,
                                            borderRadius:
                                                BorderRadius.circular(10.0)),
                                        child: Text(_customer.status,
                                            style: theme.textTheme.bodyMedium!
                                                .copyWith(
                                                    color: theme.primaryColor,
                                                    fontSize: 12.0))),
                                  ])
                            : _avatar?.toImage(width: 58.0) ??
                                Image.asset('assets/images/no-image.png'),
                        title: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                'Date: ${CoreConvertDateTime.formatTimeStampToString(_customer.date, true)}'),
                            Text(
                              _customer.name,
                              style: theme.textTheme.bodyMedium!
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text('${_customer.gender} | Age: ${_customer.age}'),
                            if (_customer.closedDate != null)
                              Text(
                                  'Closed Date: ${CoreConvertDateTime.formatTimeStampToString(_customer.closedDate!, true)}'),
                          ],
                        ),
                        trailing: Wrap(
                          spacing: 10.0,
                          runSpacing: 10.0,
                          children: [
                            if (_customer.status == 'Active')
                              ElevatedButton(
                                  onPressed: () {
                                    // check is app connect to server or not
                                    bool isConnected = context
                                        .read<ConnectionProvider>()
                                        .isConnected;
                                    if (!isConnected) {
                                      return Alert().show(
                                          message: CommonMsg.noConnection,
                                          alertIcon: CommonBocIcons.alertError,
                                          alertBackGroundColor:
                                              CommonBocColors.errorLight,
                                          context: context);
                                    }
                                    Alert().showAlertDialog(
                                        title: const AlertDialogTitle(
                                            title: 'Confirmation'),
                                        content: const AlertDialogContent(
                                          textContent:
                                              'Are you sure to close ?',
                                        ),
                                        context: context,
                                        onCancel: () => Navigator.of(context,
                                                rootNavigator: true)
                                            .pop(),
                                        onAgree: () {
                                          context
                                              .read<CustomerProvider>()
                                              .updateCustomerStatus(
                                                id: _customer.id!,
                                                status: 'Closed',
                                                closedDate: DateTime.now(),
                                              )
                                              .then((value) {
                                            Alert().show(
                                              message:
                                                  'Good job, your customer has been successfully closed.',
                                              alertIcon:
                                                  CommonBocIcons.alertSuccess,
                                              alertBackGroundColor:
                                                  CommonBocColors.successLight,
                                              context: context,
                                              duration:
                                                  const Duration(seconds: 1),
                                            );

                                            //close dailog
                                            Navigator.of(context,
                                                    rootNavigator: true)
                                                .pop();
                                          }).catchError((error) {
                                            Alert().show(
                                                message: error.message,
                                                alertIcon:
                                                    CommonBocIcons.alertError,
                                                alertBackGroundColor:
                                                    CommonBocColors.errorLight,
                                                context: context);
                                          });
                                        },
                                        agreeBtnLabel: 'Yes',
                                        agreeBtnColor: theme.iconTheme.color!);
                                  },
                                  style: ElevatedButton.styleFrom(
                                      padding: const EdgeInsets.all(8.0),
                                      backgroundColor:
                                          CommonBocColors.errorLight),
                                  child: const Text('Close')),
                            if (_customer.status == 'Active')
                              ElevatedButton(
                                  onPressed: () {
                                    // check is app connect to server or not
                                    bool isConnected = context
                                        .read<ConnectionProvider>()
                                        .isConnected;
                                    if (!isConnected) {
                                      return Alert().show(
                                          message: CommonMsg.noConnection,
                                          alertIcon: CommonBocIcons.alertError,
                                          alertBackGroundColor:
                                              CommonBocColors.errorLight,
                                          context: context);
                                    }
                                    Alert().showAlertDialog(
                                        title: const AlertDialogTitle(
                                            title: 'Confirmation'),
                                        content: const AlertDialogContent(
                                          textContent:
                                              'Are you sure to cancel ?',
                                        ),
                                        context: context,
                                        onCancel: () => Navigator.of(context,
                                                rootNavigator: true)
                                            .pop(),
                                        onAgree: () {
                                          context
                                              .read<CustomerProvider>()
                                              .updateCustomerStatus(
                                                id: _customer.id!,
                                                status: 'Cancel',
                                              )
                                              .then((value) {
                                            Alert().show(
                                              message:
                                                  'Good job, your customer has been successfully cancel.',
                                              alertIcon:
                                                  CommonBocIcons.alertSuccess,
                                              alertBackGroundColor:
                                                  CommonBocColors.successLight,
                                              context: context,
                                              duration:
                                                  const Duration(seconds: 1),
                                            );

                                            //close dailog
                                            Navigator.of(context,
                                                    rootNavigator: true)
                                                .pop();
                                          }).catchError((error) {
                                            Alert().show(
                                                message: error.message,
                                                alertIcon:
                                                    CommonBocIcons.alertError,
                                                alertBackGroundColor:
                                                    CommonBocColors.errorLight,
                                                context: context);
                                          });
                                        },
                                        agreeBtnLabel: 'Yes',
                                        agreeBtnColor: theme.iconTheme.color!);
                                  },
                                  style: ElevatedButton.styleFrom(
                                      padding: const EdgeInsets.all(8.0),
                                      backgroundColor:
                                          CommonBocColors.warningLight),
                                  child: const Text('Cancel')),
                            if (_customer.status == 'Cancel' &&
                                    context
                                            .read<CustomerOverviewProvider>()
                                            .userRole ==
                                        'admin' ||
                                _customer.status == 'Closed')
                              ElevatedButton(
                                  onPressed: () {
                                    Alert().showAlertDialog(
                                        title: const AlertDialogTitle(
                                            title: 'Confirmation'),
                                        content: const AlertDialogContent(
                                          textContent:
                                              'Are you sure to reactive ?',
                                        ),
                                        context: context,
                                        onCancel: () => Navigator.of(context,
                                                rootNavigator: true)
                                            .pop(),
                                        onAgree: () {
                                          context
                                              .read<CustomerProvider>()
                                              .updateCustomerStatus(
                                                  id: _customer.id!,
                                                  status: 'Active')
                                              .then((value) {
                                            Alert().show(
                                              message:
                                                  'Good job, your customer has been successfully reactive.',
                                              alertIcon:
                                                  CommonBocIcons.alertSuccess,
                                              alertBackGroundColor:
                                                  CommonBocColors.successLight,
                                              context: context,
                                              duration:
                                                  const Duration(seconds: 1),
                                            );

                                            //close dailog
                                            Navigator.of(context,
                                                    rootNavigator: true)
                                                .pop();
                                          }).catchError((error) {
                                            Alert().show(
                                                message: error.message,
                                                alertIcon:
                                                    CommonBocIcons.alertError,
                                                alertBackGroundColor:
                                                    CommonBocColors.errorLight,
                                                context: context);
                                          });
                                        },
                                        agreeBtnLabel: 'Yes',
                                        agreeBtnColor: theme.iconTheme.color!);
                                  },
                                  style: ElevatedButton.styleFrom(
                                      padding: const EdgeInsets.all(8.0),
                                      backgroundColor:
                                          CommonBocColors.successLight),
                                  child: const Text('Reactive')),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Center(
                child: widget.hasMoreData
                    ? ListView.builder(
                        itemCount: 1,
                        shrinkWrap: true,
                        itemBuilder: ((context, index) => CustomerSkeletonList(
                              color: theme.primaryColor,
                            )),
                      )
                    : const SizedBox());
          }
        });
  }

  @override
  bool get wantKeepAlive => true;
}

class CustomerFilter extends StatelessWidget {
  const CustomerFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final size = MediaQuery.of(context).size;
    final debouncer = Debouncer(milliseconds: 300);
    final readProvider = context.read<CustomerProvider>();
    return Container(
      color: theme.colorScheme.primary,
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Expanded(
            child: Selector<CustomerProvider, String>(
              selector: (context, state) => state.searchText,
              builder: (context, searchText, child) => FormBuilderTextField(
                name: 'search',
                textAlignVertical: TextAlignVertical.center,
                decoration: fbSearchTextFieldStyle(
                  hintText: 'Search',
                  theme: theme,
                  searchText: searchText,
                  onClear: () {
                    FormBuilder.of(context)!.fields['search']!.didChange('');
                  },
                ),
                onTap: () {
                  final FocusScopeNode currentScope = FocusScope.of(context);
                  if (!currentScope.hasPrimaryFocus && currentScope.hasFocus) {
                    FocusManager.instance.primaryFocus!.unfocus();
                  }
                },
                onChanged: (String? text) {
                  String status = readProvider.status;
                  DateTime? date =
                      FormBuilder.of(context)!.fields['date']!.value;
                  text!.isNotEmpty
                      ?
                      //search all records on server side
                      debouncer.run(() => readProvider.filter(
                          date: date,
                          searchText: text,
                          status: status,
                          pageSize: 50))
                      :
                      // return back to normal
                      debouncer.run(() =>
                          readProvider.filter(date: date, status: status));
                },
              ),
            ),
          ),
          const SizedBox(width: 10.0),
          SizedBox(
            width: size.width * 0.4,
            child: Row(
              children: [
                Expanded(
                  child: FormBuilderDateTimePicker(
                    name: 'date',
                    onChanged: (date) {
                      String? searchText =
                          FormBuilder.of(context)!.fields['search']!.value;
                      String status = readProvider.status;
                      date != null
                          ? readProvider.filter(
                              searchText: searchText ?? '',
                              date: date,
                              status: status,
                              pageSize: 50)
                          : readProvider.filter(
                              searchText: searchText ?? '',
                              date: date,
                              status: status);
                    },
                    decoration: fbDateTimePickerFieldStyle(
                        label: 'Date',
                        theme: theme,
                        allowClear: true,
                        onClear: () {
                          FormBuilder.of(context)!
                              .fields['date']!
                              .didChange(null);
                        }),
                    inputType: InputType.date,
                    format: DateFormat('dd/MM/yyyy'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
