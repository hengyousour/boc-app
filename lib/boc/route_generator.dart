import 'package:flutter/material.dart';
import '../core/screens/print_preview.dart';
import '/app.dart';
import '/core/screens/home.dart';
import '/core/screens/profile.dart';
import '/core/screens/login.dart';
import '/core/screens/setting.dart';
import '/core/screens/dashboard.dart';
import '/core/screens/sign_up.dart';
import '/core/screens/verify_account.dart';
import '/core/screens/verified_account.dart';
import '/core/screens/reset_password.dart';
import '/core/screens/reset_password_by_phone.dart';
import '/core/screens/error_404.dart';
import 'models/customer/customer_model.dart';
import 'screens/customer/customer.dart';
import 'screens/customer/create_customer.dart';
import 'screens/customer/edit_customer.dart';
import 'screens/reports/reports.dart';
import 'screens/reports/customer_list_report.dart';
import 'screens/reports/customer_outstanding_list_report.dart';
import 'screens/reports/biorc_report.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/app':
        return MaterialPageRoute(builder: (_) => const App());
      case '/home':
        return MaterialPageRoute(builder: (_) => const Home());
      case '/login':
        return MaterialPageRoute(builder: (_) => const Login());
      case '/sign-up':
        return MaterialPageRoute(builder: (_) => const SignUp());
      case '/verify-account':
        return MaterialPageRoute(builder: (_) => const VerifyAccount());
      case '/verified-account':
        return MaterialPageRoute(builder: (_) => const VerifiedAccount());
      case '/reset-password':
        return MaterialPageRoute(builder: (_) => const ResetPassword());
      case '/reset-password-by-phone':
        return MaterialPageRoute(builder: (_) => const ResetPasswordByPhone());
      case '/dashboard':
        return MaterialPageRoute(builder: (_) => const Dashboard());
      case '/profile':
        return MaterialPageRoute(builder: (_) => const Profile());
      case '/setting':
        return MaterialPageRoute(builder: (_) => const Setting());
      case '/print-preview':
        return MaterialPageRoute(builder: (_) => const PrintPreview());
      case '/boc/customer':
        return MaterialPageRoute(builder: (_) => const Customer());
      case '/boc/create-customer':
        return MaterialPageRoute(builder: (_) => const CreateCustomer());
      case '/boc/edit-customer':
        return MaterialPageRoute(builder: (_) {
          final CustomerModel customerArgs =
              settings.arguments as CustomerModel;
          return EditCustomer(customer: customerArgs);
        });
      case '/boc/show-customer':
        return MaterialPageRoute(builder: (_) {
          final CustomerModel customerArgs =
              settings.arguments as CustomerModel;
          return EditCustomer(customer: customerArgs, editMode: false);
        });
      case '/boc/reports':
        return MaterialPageRoute(builder: (_) => const Reports());
      case '/boc/customer-list-report':
        return MaterialPageRoute(builder: (_) => const CustomerListReport());
      case '/boc/customer-outstanding-list-report':
        return MaterialPageRoute(
            builder: (_) => const CustomerOutstandingListReport());
      case '/boc/biorc-report':
        return MaterialPageRoute(builder: (_) => const BiorcReport());
      default:
        return MaterialPageRoute(builder: (_) => const Error404());
    }
  }
}
