import 'dart:async';
import 'package:flutter/material.dart';
import '../../../app.dart';
import '../../models/reports/biorc/biorc_form_report_model.dart';
import '../../models/reports/biorc/biorc_report_model.dart';

class BiorcReportProvider with ChangeNotifier {
  BiorcFormReportModel _biorcFilter =
      const BiorcFormReportModel(fromDate: null, toDate: null);
  BiorcFormReportModel get biorcFilter => _biorcFilter;

  Stream<BiorcReportModel>? _streamBiorc;
  Stream<BiorcReportModel>? get streamBiorc => _streamBiorc;
  StreamController<Map<String, dynamic>>? _streamBiorcController;
  StreamController<Map<String, dynamic>>? get streamBiorcController =>
      _streamBiorcController;

  void initData() {
    _streamBiorcController = StreamController<Map<String, dynamic>>.broadcast();
    _streamBiorc = _streamBiorcController!.stream
        .map((event) => BiorcReportModel.fromJson(event));
  }

  Future<void> submit({required BiorcFormReportModel formDoc}) async {
    _biorcFilter = BiorcFormReportModel(
      fromDate: formDoc.fromDate,
      toDate: formDoc.toDate,
    );

    Map<String, dynamic> _tempData =
        await meteor.call('customerBriefInfoReport', args: [
      {'params': formDoc.toJson()}
    ]);
    if (_tempData['rptContent'] != null) {
      _streamBiorcController!.add(_tempData['rptContent']);
    }

    notifyListeners();
  }

  void clearBiorcReportState() {
    _biorcFilter = const BiorcFormReportModel(fromDate: null, toDate: null);
    _streamBiorcController!.close();
    notifyListeners();
  }
}
