import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../../models/option_model.dart';

class ReportsProvider with ChangeNotifier {
  List<OptionModel> _reportList = [];
  List<OptionModel> get reportList => _reportList;

  void initData({required String roleUser}) {
    switch (roleUser) {
      case 'admin':
        _reportList = const [
          OptionModel(
            label: 'Customer Report',
            value: '/boc/customer-list-report',
          ),
          OptionModel(
            label: 'Customer Outstanding Report',
            value: '/boc/customer-outstanding-list-report',
          ),
        ];
        break;
      default:
        _reportList = const [
          OptionModel(
            label: 'Customer Report',
            value: '/boc/customer-list-report',
          ),
          OptionModel(
            label: 'Customer Outstanding Report',
            value: '/boc/customer-outstanding-list-report',
          ),
          OptionModel(
            label: 'Brief Information Of Referral Case Report',
            value: '/boc/biorc-report',
          ),
        ];
        break;
    }
  }
}
