import 'dart:async';
import 'package:flutter/material.dart';
import '../../../app.dart';
import '../../models/reports/customer-list/customer_list_form_report_model.dart';
import '../../models/reports/customer-list/customer_list_report_model.dart';

class CustomerListReportProvider with ChangeNotifier {
  CustomerListFormReportModel _customerListFilter =
      const CustomerListFormReportModel(
          fromDate: null, toDate: null, status: '');
  CustomerListFormReportModel get customerListFilter => _customerListFilter;

  Stream<List<CustomerListReportModel>>? _streamCustomerList;
  Stream<List<CustomerListReportModel>>? get streamCustomerList =>
      _streamCustomerList;
  StreamController<List<Map<String, dynamic>>>? _streamCustomerListController;
  StreamController<List<Map<String, dynamic>>>?
      get streamCustomerListController => _streamCustomerListController;
  int _totalPage = 0;
  int get totalPage => _totalPage;
  bool _enablePagination = true;
  bool get enablePagination => _enablePagination;

  void initData() {
    _streamCustomerListController =
        StreamController<List<Map<String, dynamic>>>.broadcast();
    _streamCustomerList = _streamCustomerListController!.stream
        .map((List<Map<String, dynamic>> customersData) {
      return customersData.map((Map<String, dynamic> customerData) {
        return CustomerListReportModel.fromJson(customerData);
      }).toList();
    });
  }

  Future<void> submit({required CustomerListFormReportModel formDoc}) async {
    _customerListFilter = CustomerListFormReportModel(
      fromDate: formDoc.fromDate,
      toDate: formDoc.toDate,
      status: formDoc.status == '' ? 'All' : formDoc.status,
    );

    Map<String, dynamic> _tempData = await meteor.call('customerReport', args: [
      {'params': formDoc.toJson()}
    ]);

    List<dynamic> _contentData = _tempData['rptContent'];
    _streamCustomerListController!.add(_contentData.cast());
    _totalPage = _tempData['totalContentPage'];
    notifyListeners();
  }

  void setEnablePagination({required bool value}) {
    _enablePagination = value;
    notifyListeners();
  }

  void clearCustomerListReportState() {
    _customerListFilter = const CustomerListFormReportModel(
        fromDate: null, toDate: null, status: '');
    _streamCustomerListController!.close();
    notifyListeners();
  }
}
