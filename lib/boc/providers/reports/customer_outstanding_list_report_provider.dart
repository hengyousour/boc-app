import 'dart:async';
import 'package:flutter/material.dart';
import '../../../app.dart';
import '../../models/reports/customer-list/customer_list_report_model.dart';
import '../../models/reports/customer-outstanding-list/customer_outstanding_list_form_report_model.dart';

class CustomerOutstandingListReportProvider with ChangeNotifier {
  CustomerOutstandingListFormReportModel _customerOutstandingListFilter =
      const CustomerOutstandingListFormReportModel(date: null);
  CustomerOutstandingListFormReportModel get customerOutstandingListFilter =>
      _customerOutstandingListFilter;

  Stream<List<CustomerListReportModel>>? _streamCustomerOutstandingList;
  Stream<List<CustomerListReportModel>>? get streamCustomerList =>
      _streamCustomerOutstandingList;
  StreamController<List<Map<String, dynamic>>>?
      _streamCustomerOutstandingListController;
  StreamController<List<Map<String, dynamic>>>?
      get streamCustomerListController =>
          _streamCustomerOutstandingListController;
  int _totalPage = 0;
  int get totalPage => _totalPage;
  bool _enablePagination = true;
  bool get enablePagination => _enablePagination;

  void initData() {
    _streamCustomerOutstandingListController =
        StreamController<List<Map<String, dynamic>>>.broadcast();
    _streamCustomerOutstandingList = _streamCustomerOutstandingListController!
        .stream
        .map((List<Map<String, dynamic>> customersData) {
      return customersData.map((Map<String, dynamic> customerData) {
        return CustomerListReportModel.fromJson(customerData);
      }).toList();
    });
  }

  Future<void> submit(
      {required CustomerOutstandingListFormReportModel formDoc}) async {
    _customerOutstandingListFilter = CustomerOutstandingListFormReportModel(
      date: formDoc.date,
    );

    Map<String, dynamic> _tempData =
        await meteor.call('customerOutstandingReport', args: [
      {'params': formDoc.toJson()}
    ]);

    List<dynamic> _contentData = _tempData['rptContent'];
    _streamCustomerOutstandingListController!.add(_contentData.cast());
    _totalPage = _tempData['totalContentPage'];
    notifyListeners();
  }

  void setEnablePagination({required bool value}) {
    _enablePagination = value;
    notifyListeners();
  }

  void clearCustomerOutstandingListReportState() {
    _customerOutstandingListFilter =
        const CustomerOutstandingListFormReportModel(date: null);
    _streamCustomerOutstandingListController!.close();
    notifyListeners();
  }
}
