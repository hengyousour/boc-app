import 'dart:async';
import 'package:dart_meteor/dart_meteor.dart';
import 'package:flutter/foundation.dart';
import '/boc/models/option_model.dart';
import '../../app.dart';
import '../models/customer/customer_model.dart';
import 'package:dart_date/dart_date.dart';

class CustomerProvider with ChangeNotifier {
  SubscriptionHandler? _customerSubscription;
  SubscriptionHandler? get customerSubscription => _customerSubscription;
  StreamSubscription<Map<String, dynamic>>? _customerListener;
  final List<OptionModel> _customerStatusList = const [
    OptionModel(label: 'Active', value: 'Active'),
    OptionModel(label: 'Cancel', value: 'Cancel'),
    OptionModel(label: 'Closed', value: 'Closed'),
    OptionModel(label: 'All', value: ''),
  ];
  List<OptionModel> get customerStatusList => _customerStatusList;
  bool _isCustomerDataUpdate = false;
  bool get isCustomerDataUpdate => _isCustomerDataUpdate;
  List<Map<String, bool>> _hasMore = [
    {'Active': false},
    {'Cancel': false},
    {'Closed': false},
    {'All': false}
  ];
  List<Map<String, bool>> get hasMore => _hasMore;

  Stream<List<CustomerModel>>? _streamActive;
  Stream<List<CustomerModel>>? get streamActive => _streamActive;
  List<Map<String, dynamic>> _dataActive = [];
  List<Map<String, dynamic>> get dataActive => _dataActive;
  StreamController<List<Map<String, dynamic>>>? _controller;
  StreamController<List<Map<String, dynamic>>>? get controller => _controller;

  Stream<List<CustomerModel>>? _streamClosed;
  Stream<List<CustomerModel>>? get streamClosed => _streamClosed;
  List<Map<String, dynamic>> _dataClosed = [];
  List<Map<String, dynamic>> get dataClosed => _dataClosed;
  StreamController<List<Map<String, dynamic>>>? _controllerClosed;
  StreamController<List<Map<String, dynamic>>>? get controllerClosed =>
      _controllerClosed;

  Stream<List<CustomerModel>>? _streamCancel;
  Stream<List<CustomerModel>>? get streamCancel => _streamCancel;
  List<Map<String, dynamic>> _dataCancel = [];
  List<Map<String, dynamic>> get dataCancel => _dataCancel;
  StreamController<List<Map<String, dynamic>>>? _controllerCancel;
  StreamController<List<Map<String, dynamic>>>? get controllerCancel =>
      _controllerCancel;

  Stream<List<CustomerModel>>? _streamAll;
  Stream<List<CustomerModel>>? get streamAll => _streamAll;
  List<Map<String, dynamic>> _dataAll = [];
  List<Map<String, dynamic>> get dataAll => _dataAll;
  StreamController<List<Map<String, dynamic>>>? _controllerAll;
  StreamController<List<Map<String, dynamic>>>? get controllerAll =>
      _controllerAll;

  //note: index 0 = active page,index 1 = cancel page,index 2 = close page,index 3 = all page
  List<int> _pages = [1, 1, 1, 1];
  List<int> get pages => _pages;

  int _pageSize = 15;
  int get pageSize => _pageSize;
  String _searchText = '';
  String get searchText => _searchText;
  String _status = 'Active';
  String get status => _status;
  DateTime? _date;
  DateTime? get date => _date;

  void subscribe() {
    _controller = StreamController<List<Map<String, dynamic>>>.broadcast();
    _controllerCancel =
        StreamController<List<Map<String, dynamic>>>.broadcast();
    _controllerClosed =
        StreamController<List<Map<String, dynamic>>>.broadcast();
    _controllerAll = StreamController<List<Map<String, dynamic>>>.broadcast();
    _streamActive =
        _controller!.stream.map((List<Map<String, dynamic>> customersData) {
      return customersData.map((Map<String, dynamic> customerData) {
        return CustomerModel.fromJson(customerData);
      }).toList();
    });
    _streamCancel = _controllerCancel!.stream
        .map((List<Map<String, dynamic>> customersData) {
      return customersData.map((Map<String, dynamic> customerData) {
        return CustomerModel.fromJson(customerData);
      }).toList();
    });
    _streamClosed = _controllerClosed!.stream
        .map((List<Map<String, dynamic>> customersData) {
      return customersData.map((Map<String, dynamic> customerData) {
        return CustomerModel.fromJson(customerData);
      }).toList();
    });

    _streamAll =
        _controllerAll!.stream.map((List<Map<String, dynamic>> customersData) {
      return customersData.map((Map<String, dynamic> customerData) {
        return CustomerModel.fromJson(customerData);
      }).toList();
    });

    _customerListener = meteor.collection('customers').listen((event) {
      if (event.isNotEmpty) {
        _isCustomerDataUpdate = true;
        notifyListeners();
      } else {
        initData();
      }
    });
  }

  void initData(
      {String? searchText,
      String status = 'Active',
      DateTime? date,
      int page = 1,
      int pageSize = 15}) async {
    _controller!.add([]);
    _controllerCancel!.add([]);
    _controllerClosed!.add([]);
    _controllerAll!.add([]);
    final List<dynamic> _tempData =
        await meteor.call('findCustomerByStatus', args: [
      {
        'filter': searchText,
        'fromDate': date,
        'page': page,
        'pageSize': pageSize
      }
    ]);
    List<dynamic> _active = _tempData[0]['data'];
    if (_active.isNotEmpty) {
      _dataActive = _active.cast();
      _controller!.add(_dataActive);
      if (_active.length == pageSize) {
        _hasMore[0]['Active'] = true;
      }
    }
    List<dynamic> _cancel = _tempData[1]['data'];
    if (_cancel.isNotEmpty) {
      _dataCancel = _cancel.cast();
      _controllerCancel!.add(_dataCancel);
      if (_cancel.length == pageSize) {
        _hasMore[1]['Cancel'] = true;
      }
    }
    List<dynamic> _closed = _tempData[2]['data'];
    if (_closed.isNotEmpty) {
      _dataClosed = _closed.cast();
      _controllerClosed!.add(_dataClosed);
      if (_closed.length == pageSize) {
        _hasMore[2]['Closed'] = true;
      }
    }
    List<dynamic> _all = _tempData[3]['data'];
    if (_all.isNotEmpty) {
      _dataAll = _all.cast();
      _controllerAll!.add(_dataAll);
      if (_all.length == pageSize) {
        _hasMore[3]['All'] = true;
      }
    }
    _isCustomerDataUpdate = false;
    notifyListeners();
  }

  Future<void> filter(
      {String searchText = '',
      DateTime? date,
      required String status,
      int page = 1,
      int pageSize = 15}) async {
    _status = status;
    _searchText = searchText;
    _date = date?.endOfDay ?? null;
    try {
      final List<dynamic> _tempDataFilter =
          await meteor.call('findCustomer', args: [
        {
          'filter': _searchText,
          'status': _status,
          'fromDate': _date,
          'page': page,
          'pageSize': pageSize
        }
      ]);

      switch (status) {
        case 'Active':
          _pages[0] = page;
          _dataActive = [];
          _dataActive = _tempDataFilter.cast();
          _controller!.add(_dataActive);
          if (_tempDataFilter.length == pageSize) {
            _hasMore[0]['Active'] = true;
          } else {
            _hasMore[0]['Active'] = false;
          }
          break;
        case 'Cancel':
          _pages[1] = page;
          _dataCancel = [];
          _dataCancel = _tempDataFilter.cast();
          _controllerCancel!.add(_dataCancel);
          if (_tempDataFilter.length == pageSize) {
            _hasMore[1]['Cancel'] = true;
          } else {
            _hasMore[1]['Cancel'] = false;
          }
          break;
        case 'Closed':
          _pages[2] = page;
          _dataClosed = [];
          _dataClosed = _tempDataFilter.cast();
          _controllerClosed!.add(_dataClosed);
          if (_tempDataFilter.length == pageSize) {
            _hasMore[2]['Closed'] = true;
          } else {
            _hasMore[2]['Closed'] = false;
          }
          break;
        default:
          _pages[3] = page;
          _dataAll = [];
          _dataAll = _tempDataFilter.cast();
          _controllerAll!.add(_dataAll);
          if (_tempDataFilter.length == pageSize) {
            _hasMore[3]['All'] = true;
          } else {
            _hasMore[3]['All'] = false;
          }
          break;
      }
      notifyListeners();
    } catch (e) {
      return Future.error(e);
    }
  }

  Future<void> loadMore(
      {bool clearCachedData = false,
      DateTime? date,
      required String status,
      required int page,
      required int pageSize}) async {
    page += 1;

    switch (status) {
      case 'Active':
        if (!_hasMore[0]['Active']!) {
          return Future.value();
        }
        break;
      case 'Cancel':
        if (!_hasMore[1]['Cancel']!) {
          return Future.value();
        }
        break;
      case 'Closed':
        if (!_hasMore[2]['Closed']!) {
          return Future.value();
        }
        break;
      default:
        if (!_hasMore[3]['All']!) {
          return Future.value();
        }
        break;
    }
    _date = date?.endOfDay ?? null;
    final List<dynamic> _tempData = await meteor.call('findCustomer', args: [
      {
        'status': status,
        'fromDate': _date,
        'page': page,
        'pageSize': pageSize,
      }
    ]);

    if (_tempData.isNotEmpty && _tempData.length <= pageSize) {
      switch (status) {
        case 'Active':
          _dataActive.addAll(_tempData.cast());
          _controller!.add(_dataActive);
          _pages[0] += 1;
          _pageSize += _tempData.length;
          break;
        case 'Cancel':
          _dataCancel.addAll(_tempData.cast());
          _controllerCancel!.add(_dataCancel);
          _pages[1] += 1;
          _pageSize += _tempData.length;
          break;
        case 'Closed':
          _dataClosed.addAll(_tempData.cast());
          _controllerClosed!.add(_dataClosed);
          _pages[2] += 1;
          _pageSize += _tempData.length;
          break;
        default:
          _dataAll.addAll(_tempData.cast());
          _controllerAll!.add(_dataAll);
          _pages[3] += 1;
          _pageSize += _tempData.length;
          break;
      }
    } else {
      switch (status) {
        case 'Active':
          _hasMore[0]['Active'] = false;
          break;
        case 'Cancel':
          _hasMore[1]['Cancel'] = false;
          break;
        case 'Closed':
          _hasMore[2]['Closed'] = false;
          break;
        default:
          _hasMore[3]['All'] = false;
          break;
      }
    }
    notifyListeners();
  }

  void clearCustomerState() {
    _dataActive.clear();
    _dataCancel.clear();
    _dataClosed.clear();
    _dataAll.clear();
    _hasMore = [
      {'Active': false},
      {'Cancel': false},
      {'Closed': false},
      {'All': false}
    ];
    _pages = [1, 1, 1, 1];
    _pageSize = 15;
    _status = 'Active';
    _searchText = '';
    _date = null;
    _isCustomerDataUpdate = false;
    _controller!.close();
    _controllerCancel!.close();
    _controllerClosed!.close();
    _controllerAll!.close();
    //stop customer listener
    _customerListener!.cancel();
  }

  void unSubscribe() {
    if (_customerSubscription!.subId.isNotEmpty) {
      _customerSubscription!.stop();
    }
  }

  Future insertCustomer({required Map<String, dynamic> doc}) async {
    return meteor.call('insertCustomer', args: [
      {'doc': doc}
    ]);
  }

  Future updateCustomer({required Map<String, dynamic> doc}) async {
    return meteor.call('updateCustomer', args: [
      {'doc': doc}
    ]);
  }

  Future updateCustomerStatus(
      {required String id,
      required String status,
      DateTime? closedDate}) async {
    return meteor.call('updateCustomerStatus', args: [
      {'_id': id, 'status': status, 'closedDate': closedDate}
    ]);
  }

  Future removeCustomer({required String id}) async {
    return meteor.call('removeCustomer', args: [
      {'_id': id}
    ]);
  }
}
