import 'dart:async';
import 'package:dart_meteor/dart_meteor.dart';
import 'package:flutter/foundation.dart';
import '../../app.dart';
import '../../core/storages/user_storage.dart';

class CustomerOverviewProvider with ChangeNotifier {
  SubscriptionHandler? _customerOverviewSubscription;
  SubscriptionHandler? get customerOverviewSubscription =>
      _customerOverviewSubscription;
  StreamSubscription<Map<String, dynamic>>? _overviewListener;
  String _fullName = '';
  String get fullName => _fullName;
  Future<int>? _totalCustomer;
  Future<int>? get totalCustomer => _totalCustomer;
  Future<int>? _totalActiveCustomer;
  Future<int>? get totalActiveCustomer => _totalActiveCustomer;
  Future<int>? _totalCancelCustomer;
  Future<int>? get totalCancelCustomer => _totalCancelCustomer;
  Future<int>? _totalClosedCustomer;
  Future<int>? get totalClosedCustomer => _totalClosedCustomer;
  bool _isOverviewDataUpdate = false;
  bool get isOverviewDataUpdate => _isOverviewDataUpdate;
  String _userRole = '';
  String get userRole => _userRole;

  void initUserDocWithRoles() async {
    //get currenct user doc
    Map<String, dynamic> _tempUserDoc = await UserStorage().getUser();
    _fullName = _tempUserDoc['fullName'];

    //check user role
    dynamic _tempUsers = await meteor.call('findUsersById', args: [
      {'id': _tempUserDoc['id']}
    ]);
    List<dynamic> _roles = _tempUsers['roles'];
    if (_roles.contains('admin-setting')) _userRole = 'admin';
    if (_roles.contains('staff')) _userRole = 'staff';
  }

  void subscribeOverview() async {
    _customerOverviewSubscription = meteor.subscribe(
      'customers',
      args: [{}],
      onReady: () {
        _overviewListener = meteor.collection('customers').listen((event) {
          if (event.isNotEmpty) {
            _isOverviewDataUpdate = true;
            notifyListeners();
          } else {
            initData();
          }
        });
      },
    );
  }

  void initData() async {
    List<dynamic> _tempData =
        await meteor.call('countCustomerByStatus', args: [{}]);
    _totalCustomer = Future.value(0);
    _totalActiveCustomer = Future.value(0);
    _totalCancelCustomer = Future.value(0);
    _totalClosedCustomer = Future.value(0);

    if (_tempData.isNotEmpty) {
      int total = _tempData[0]['total'];
      List<Map<String, dynamic>> data =
          List<Map<String, dynamic>>.from(_tempData[0]['data']);
      _totalCustomer = Future.value(total);
      for (int i = 0; i < data.length; i++) {
        switch (data[i]['status']) {
          case 'Active':
            _totalActiveCustomer = Future.value(data[i]['count']);
            break;
          case 'Cancel':
            _totalCancelCustomer = Future.value(data[i]['count']);
            break;
          case 'Closed':
            _totalClosedCustomer = Future.value(data[i]['count']);
            break;
          default:
            _totalActiveCustomer = Future.value(0);
            _totalCancelCustomer = Future.value(0);
            _totalClosedCustomer = Future.value(0);
        }
      }
    }
    _isOverviewDataUpdate = false;
    notifyListeners();
  }

  void clearOverviewState() {
    _totalCustomer = null;
    _totalActiveCustomer = null;
    _totalCancelCustomer = null;
    _totalClosedCustomer = null;
    //stop listen
    _overviewListener!.cancel();
  }

  void unSubscribe() {
    if (_customerOverviewSubscription?.subId != null) {
      _customerOverviewSubscription!.stop();
      _isOverviewDataUpdate = false;
    }
  }
}
