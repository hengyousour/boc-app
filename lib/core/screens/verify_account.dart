import 'package:flutter/material.dart';
import '../widgets/responsive.dart';
import '../widgets/verify-account/verify_account_form.dart';

class VerifyAccount extends StatelessWidget {
  const VerifyAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Responsive(
                    desktop: Row(
                      children: const [
                        Spacer(),
                        Expanded(
                          child: VerifyAccountForm(),
                        ),
                        Spacer(),
                      ],
                    ),
                    tablet: Row(
                      children: const [
                        Spacer(),
                        Expanded(flex: 6, child: VerifyAccountForm()),
                        Spacer(),
                      ],
                    ),
                    mobile: const VerifyAccountForm(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
