import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

import '../providers/profile_provider.dart';
import '../utils/constants.dart';
import '../../core/widgets/dashboard/dashboard_grid.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return FadeIn(
      duration: const Duration(milliseconds: 1200),
      child: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Consumer<ProfileProvider>(
                      builder: (_, state, child) => RichText(
                        text: TextSpan(
                          text: 'dashboard.hello'.tr(),
                          style: TextStyle(
                            color: theme.iconTheme.color,
                            fontFamily: CommonFonts.header,
                          ),
                          children: [
                            TextSpan(
                                text: state.data.fullName?.toUpperCase(),
                                style: theme.textTheme.bodyLarge!.copyWith(
                                    color: theme.iconTheme.color,
                                    fontFamily: CommonFonts.header,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 20.0,
                ),
                const Expanded(
                  child: DashBoardGrid(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
