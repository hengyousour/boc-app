import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import '/core/models/user/user_model.dart';
import '/core/widgets/profile/edit_profile.dart';
import '../providers/profile_provider.dart';
import '../utils/constants.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  static final GlobalKey<FormBuilderState> _fbProfileKey =
      GlobalKey<FormBuilderState>();
  @override
  void initState() {
    super.initState();
    context.read<ProfileProvider>().getUserDocFromLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return FadeIn(
      duration: const Duration(milliseconds: 1200),
      child: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Scaffold(
          body: SafeArea(
            child: Consumer<ProfileProvider>(
              builder: (_, state, child) => Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(top: 30.0),
                    child: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? _profileHeaderPortrait(
                            theme: theme,
                            context: context,
                            data: state.data,
                          )
                        : _profileHeaderLandscape(
                            theme: theme,
                            context: context,
                            data: state.data,
                          ),
                  ),
                  const SizedBox(
                    height: 30.0,
                  ),
                  FormBuilder(
                    key: _fbProfileKey,
                    initialValue: {
                      '_id': state.data.id,
                      'code': state.data.code,
                      'fullName': state.data.fullName,
                    },
                    child: const Expanded(child: EditProfile()),
                  )
                ],
              ),
            ),
          ),
          bottomNavigationBar: Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    textStyle: const TextStyle(color: Colors.white),
                    padding: const EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                  ),
                  onPressed: () {
                    _fbProfileKey.currentState!.reset();
                  },
                  child: Text('form.reset'.tr(),
                      style: const TextStyle(
                          fontSize: 20, fontFamily: CommonFonts.body)),
                ),
              ),
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    textStyle: const TextStyle(color: Colors.white),
                    padding: const EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                  ),
                  onPressed: () async {
                    if (_fbProfileKey.currentState!.saveAndValidate()) {
                      final UserModel formDoc =
                          UserModel.fromJson(_fbProfileKey.currentState!.value);
                      context
                          .read<ProfileProvider>()
                          .updateProfile(data: formDoc, context: context);
                    }
                  },
                  child: Text('form.save'.tr(),
                      style: const TextStyle(
                          fontSize: 20, fontFamily: CommonFonts.body)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Column _profileHeaderPortrait(
      {required ThemeData theme,
      required BuildContext context,
      required UserModel data}) {
    return Column(
      children: <Widget>[
        Stack(children: [
          Container(
            width: 120.0,
            height: 120.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: theme.iconTheme.color!,
                width: 2.5,
              ),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: ((data.imagePath != null)
                          ? FileImage(File(data.imagePath!))
                          : const AssetImage('assets/images/logo.png'))
                      as ImageProvider<Object>),
            ),
          ),
          Positioned(
              bottom: 1,
              right: 1,
              child: ElevatedButton(
                onPressed: () => _settingModalBottomSheet(context),
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size(40.0, 40.0),
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
                child: const Icon(
                  CommonIcons.edit,
                ),
              ))
        ]),
        // Text(
        //   data.username!.toUpperCase(),
        //   style: theme.textTheme.bodyText1!.copyWith(
        //       color: theme.iconTheme.color,
        //       fontWeight: FontWeight.bold,
        //       fontSize: 40.0,
        //       fontFamily: CommonFonts.header),
        // ),
        // Text(data.emails as String? ?? 'unknown@gmail.com',
        //     style: TextStyle(
        //       color: theme.iconTheme.color,
        //       fontSize: 16.0,
        //       fontFamily: CommonFonts.body,
        //     )),
      ],
    );
  }

  Row _profileHeaderLandscape({
    required ThemeData theme,
    required BuildContext context,
    required UserModel data,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Stack(children: [
          Container(
            width: 120.0,
            height: 120.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: theme.iconTheme.color!,
                width: 3.0,
              ),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: ((data.imagePath != null)
                          ? FileImage(File(data.imagePath!))
                          : const AssetImage('assets/images/logo.png'))
                      as ImageProvider<Object>),
            ),
          ),
          Positioned(
              bottom: 1,
              right: 1,
              child: ElevatedButton(
                onPressed: () => _settingModalBottomSheet(context),
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size(40.0, 40.0),
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
                child: const Icon(
                  CommonIcons.edit,
                ),
              ))
        ]),
        // SizedBox(
        //   width: 10,
        // ),
        // Column(
        //   children: [
        //     Text(
        //       data.username!.toUpperCase(),
        //       style: theme.textTheme.bodyText1!.copyWith(
        //           color: theme.iconTheme.color,
        //           fontWeight: FontWeight.bold,
        //           fontSize: 40.0,
        //           fontFamily: CommonFonts.header),
        //     ),
        //     SizedBox(
        //       height: 10,
        //     ),
        //     Text(data.emails as String? ?? 'unknown@gmail.com',
        //         style: TextStyle(
        //             color: theme.iconTheme.color,
        //             fontSize: 16.0,
        //             fontFamily: CommonFonts.body)),
        //   ],
        // ),
      ],
    );
  }

  void _settingModalBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(CommonIcons.camera),
                title: const Text(
                  'Camera',
                  style: TextStyle(fontFamily: CommonFonts.body),
                ),
                onTap: () {
                  context
                      .read<ProfileProvider>()
                      .setUserImagePath(source: ImageSource.camera);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: const Icon(CommonIcons.gallery),
                title: const Text(
                  'Gallery',
                  style: TextStyle(fontFamily: CommonFonts.body),
                ),
                onTap: () {
                  context
                      .read<ProfileProvider>()
                      .setUserImagePath(source: ImageSource.gallery);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
}
