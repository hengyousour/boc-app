import 'package:flutter/material.dart';
import '../widgets/reset-password/reset_password_form.dart';
import '../widgets/responsive.dart';

class ResetPassword extends StatelessWidget {
  const ResetPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
        },
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  'Reset Password',
                  style: theme.textTheme.displayLarge,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Responsive(
                  desktop: Row(
                    children: const [
                      Spacer(),
                      Expanded(
                        child: ResetPasswordForm(),
                      ),
                      Spacer(),
                    ],
                  ),
                  tablet: Row(
                    children: const [
                      Spacer(),
                      Expanded(flex: 6, child: ResetPasswordForm()),
                      Spacer(),
                    ],
                  ),
                  mobile: const ResetPasswordForm(),
                ),
                const SizedBox(
                  height: 20.0,
                ),
                RichText(
                  text: TextSpan(
                      text: 'Remember password or want to go back ?',
                      style: TextStyle(color: theme.iconTheme.color),
                      children: [
                        WidgetSpan(
                          alignment: PlaceholderAlignment.middle,
                          child: TextButton(
                            onPressed: () => Navigator.pop(context),
                            style: TextButton.styleFrom(
                              minimumSize: Size.zero,
                              padding: EdgeInsets.zero,
                            ),
                            child: const Text('Log In'),
                          ),
                        )
                      ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
