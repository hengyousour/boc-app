import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bluetooth_printer/flutter_bluetooth_printer.dart';
import '../utils/alert.dart';
import '../utils/block_ui.dart';
import '../widgets/test_printing.dart';
import '../widgets/waiting_data.dart';
import '../models/setting/setting_model.dart';
import '../providers/setting_provider.dart';
import '../services/select_options/static_options.dart';
import '../utils/constants.dart';
import '../utils/custom_form_build_style.dart';

class Setting extends StatefulWidget {
  const Setting({Key? key}) : super(key: key);

  @override
  State<Setting> createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();
  late SettingProvider _readProvider;

  @override
  void initState() {
    super.initState();
    _readProvider = context.read<SettingProvider>();
    _readProvider.getSettingDocFromLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return FadeIn(
      duration: const Duration(milliseconds: 1200),
      child: Consumer<SettingProvider>(
        builder: (_, state, child) => Scaffold(
          body: SafeArea(
            child: state.isLoading
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      WaitingData(
                        textColor: theme.textTheme.bodyMedium!.color,
                        loadingColor: theme.iconTheme.color,
                      ),
                    ],
                  )
                : SingleChildScrollView(
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      child: FormBuilder(
                        key: _fbKey,
                        child: Column(
                          children: [
                            FormBuilderTextField(
                              enabled: false,
                              name: 'ipAddress',
                              initialValue: state.data!.ipAddress,
                              style:
                                  const TextStyle(fontFamily: CommonFonts.body),
                              decoration: fbTextFieldStyle(
                                labelText: "setting.ipAddress".tr(),
                                theme: theme,
                              ),
                              validator: FormBuilderValidators.compose(
                                  [FormBuilderValidators.required()]),
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            FormBuilderDropdown(
                              items: StaticOptions()
                                  .languageOpts
                                  .map((options) => DropdownMenuItem(
                                      value: options.value,
                                      child: Text(options.label!)))
                                  .toList(),
                              decoration: fbTextFieldStyle(
                                labelText: "Language",
                                theme: theme,
                              ),
                              name: "language",
                              initialValue: state.data!.language,
                              validator: FormBuilderValidators.compose(
                                  [FormBuilderValidators.required()]),
                            ),
                            const SizedBox(height: 10.0),
                            Row(
                              children: [
                                Expanded(
                                    child: FormBuilderTextField(
                                  enabled: false,
                                  name: 'printer',
                                  initialValue: state.data!.printer,
                                  style: const TextStyle(
                                      fontFamily: CommonFonts.body),
                                  decoration: fbTextFieldStyle(
                                    labelText: "setting.printer".tr(),
                                    theme: theme,
                                  ),
                                )),
                                const SizedBox(width: 10.0),
                                ElevatedButton(
                                    onPressed: () async {
                                      final BluetoothDevice?
                                          selectedBluetoothDevice =
                                          await FlutterBluetoothPrinter
                                              .selectDevice(context);
                                      if (selectedBluetoothDevice != null) {
                                        _readProvider.setBluetoothDeviceInfo(
                                            btDeviceInfo:
                                                selectedBluetoothDevice);
                                        _fbKey.currentState!.fields['printer']!
                                            .didChange(
                                                selectedBluetoothDevice.name);
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                        padding: const EdgeInsets.all(11.5)),
                                    child: const Icon(CommonIcons.bluetooth)),
                                state.btDeviceInfo.address.isNotEmpty
                                    ? Padding(
                                        padding:
                                            const EdgeInsets.only(left: 10.0),
                                        child: ElevatedButton(
                                            onPressed: () async {
                                              Alert().showAlertDialog(
                                                  titlePadding: EdgeInsets.zero,
                                                  content: Receipt(
                                                    builder: ((context) {
                                                      return const TestPrinting();
                                                    }),
                                                    onInitialized:
                                                        (ReceiptController
                                                            controller) {
                                                      _readProvider
                                                              .receiptController =
                                                          controller;
                                                    },
                                                  ),
                                                  context: context,
                                                  onCancel: () {
                                                    Navigator.of(context,
                                                            rootNavigator: true)
                                                        .pop();
                                                  },
                                                  agreeBtnLabel: 'Print',
                                                  agreeBtnColor:
                                                      theme.colorScheme.primary,
                                                  onAgree: () async {
                                                    await BlockUI.show(
                                                        context: context,
                                                        msgProcessingData:
                                                            'Please, wait while printer is printing.');
                                                    _readProvider
                                                        .printEvent()
                                                        .then((res) {
                                                      BlockUI.hide(context);
                                                      return Alert().show(
                                                          message:
                                                              'Good job, Printing is completed',
                                                          alertIcon: CommonIcons
                                                              .alertSuccess,
                                                          alertBackGroundColor:
                                                              CommonColors
                                                                  .successLight,
                                                          context: context);
                                                    }).catchError(((error) {
                                                      BlockUI.hide(context);
                                                      return Alert().show(
                                                          message: '$error',
                                                          alertIcon: CommonIcons
                                                              .alertError,
                                                          alertBackGroundColor:
                                                              CommonColors
                                                                  .errorLight,
                                                          context: context);
                                                    }));
                                                  });
                                            },
                                            style: ElevatedButton.styleFrom(
                                                padding:
                                                    const EdgeInsets.all(11.5)),
                                            child:
                                                const Icon(CommonIcons.print)),
                                      )
                                    : const SizedBox.shrink(),
                              ],
                            ),
                            const SizedBox(height: 10.0),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: FormBuilderTextField(
                                    name: 'printerAddress',
                                    initialValue: state.data!.printerAddress,
                                    style: const TextStyle(
                                        fontFamily: CommonFonts.body),
                                    decoration: fbTextFieldStyle(
                                      labelText:
                                          "setting.ipAddressPrinter".tr(),
                                      theme: theme,
                                    ),
                                    onChanged: (ipAddress) {
                                      if (ipAddress!.isEmpty) return;
                                      _readProvider.setWifiOrNetworkPrinter(
                                          ip: ipAddress);
                                    },
                                    validator: FormBuilderValidators.compose([
                                      FormBuilderValidators.ip(),
                                    ]),
                                  ),
                                ),
                                const SizedBox(width: 10.0),
                                Expanded(
                                  child: FormBuilderTextField(
                                    name: 'printerPort',
                                    initialValue: '${state.data!.printerPort}',
                                    style: const TextStyle(
                                        fontFamily: CommonFonts.body),
                                    decoration: fbTextFieldStyle(
                                      labelText: "setting.port".tr(),
                                      theme: theme,
                                    ),
                                    valueTransformer: (value) =>
                                        int.tryParse(value!),
                                    onChanged: (port) {
                                      if (port!.isEmpty) return;
                                      _readProvider.setWifiOrNetworkPrinter(
                                          port: int.tryParse(port));
                                    },
                                    validator: FormBuilderValidators.compose([
                                      FormBuilderValidators.required(),
                                      FormBuilderValidators.integer(),
                                    ]),
                                  ),
                                ),
                                const SizedBox(width: 10.0),
                                ElevatedButton(
                                    onPressed: () {
                                      if (_fbKey.currentState!
                                          .saveAndValidate()) {
                                        if (_fbKey
                                            .currentState!
                                            .fields['printerAddress']!
                                            .value
                                            .isEmpty) {
                                          return Alert().show(
                                              message:
                                                  'IP Address Printer field cannot be empty.',
                                              alertIcon: CommonIcons.alertError,
                                              alertBackGroundColor:
                                                  CommonColors.errorLight,
                                              context: context);
                                        }
                                        Navigator.of(context)
                                            .pushNamed('/print-preview');
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                        padding: const EdgeInsets.all(11.5)),
                                    child: const Icon(CommonIcons.print))
                              ],
                            ),
                            const SizedBox(height: 10.0),
                            FormBuilderDropdown(
                              initialValue: state.data!.printerPaperSize,
                              items: _readProvider.paperSizeOpts
                                  .map((options) => DropdownMenuItem(
                                      value: options.value,
                                      child: Text(options.label!)))
                                  .toList(),
                              decoration: fbTextFieldStyle(
                                labelText: "Paper Size",
                                theme: theme,
                              ),
                              name: "printerPaperSize",
                              onChanged: (paperSize) {
                                _readProvider.setPaperSize(
                                    paperSize: '$paperSize');
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
          ),
          bottomNavigationBar: Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    textStyle: const TextStyle(color: Colors.white),
                    padding: const EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                  ),
                  onPressed: () {
                    _fbKey.currentState!.reset();
                  },
                  child: Text('form.reset'.tr(),
                      style: const TextStyle(
                          fontSize: 20, fontFamily: CommonFonts.body)),
                ),
              ),
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    textStyle: const TextStyle(color: Colors.white),
                    padding: const EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                  ),
                  onPressed: () async {
                    if (_fbKey.currentState!.saveAndValidate()) {
                      final SettingModel formDoc =
                          SettingModel.fromJson(_fbKey.currentState!.value);
                      _readProvider.insertSetting(
                          formDoc: formDoc, context: context);
                    }
                  },
                  child: Text('form.save'.tr(),
                      style: const TextStyle(
                          fontSize: 20, fontFamily: CommonFonts.body)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
