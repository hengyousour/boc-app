import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:screenshot/screenshot.dart';
import '../providers/setting_provider.dart';
import '../utils/alert.dart';
import '../utils/block_ui.dart';
import '../utils/constants.dart';
import '../widgets/app_bar_widget.dart';
import '../widgets/test_printing.dart';

class PrintPreview extends StatefulWidget {
  const PrintPreview({super.key});

  @override
  State<PrintPreview> createState() => _PrintPreviewState();
}

class _PrintPreviewState extends State<PrintPreview> {
  late SettingProvider _readProvider;
  @override
  void initState() {
    super.initState();
    _readProvider = context.read<SettingProvider>();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: appBarWidget(
          title: 'Preview',
          titleStyle: theme.textTheme.titleLarge,
          actionIcon: CommonIcons.print,
          actionOnPress: () async {
            await BlockUI.show(
                context: context,
                msgProcessingData: 'Please, wait while printer is printing.');
            _readProvider.printByIpAddressEvent().then((res) {
              BlockUI.hide(context);
              return Alert().show(
                  message: 'Good job, Printing is completed',
                  alertIcon: CommonIcons.alertSuccess,
                  alertBackGroundColor: CommonColors.successLight,
                  context: context);
            }).catchError(((error) {
              BlockUI.hide(context);
              return Alert().show(
                  message: '$error',
                  alertIcon: CommonIcons.alertError,
                  alertBackGroundColor: CommonColors.errorLight,
                  context: context);
            }));
          }) as PreferredSizeWidget?,
      body: SingleChildScrollView(
        child: Center(
          child: Screenshot(
              controller: _readProvider.screenshotController,
              child:
                  const TestPrinting(connectionType: ConnectionType.ipAddress)),
        ),
      ),
    );
  }
}
