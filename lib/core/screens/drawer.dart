import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';
import '../providers/menu_provider.dart';
import '../providers/profile_provider.dart';
import '../providers/setting_provider.dart';
import '../providers/theme_provider.dart';
import '../storages/auth_storage.dart';
import '../storages/printer_paper_size_storage.dart';
import '../storages/printer_storage.dart';
import '../storages/user_storage.dart';
import '../utils/constants.dart';
import '../../app.dart';

class Menu extends StatefulWidget {
  final List<MenuClass> mainMenu;
  final Function(int)? callback;
  final int? current;

  const Menu(
    this.mainMenu, {
    Key? key,
    this.callback,
    this.current,
  }) : super(key: key);

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  final widthBox = const SizedBox(
    width: 16.0,
  );

  @override
  void initState() {
    super.initState();
    context.read<ProfileProvider>().getUserDocFromLocalStorage();
    context.read<SettingProvider>().getSettingDocFromLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final TextStyle androidStyle = TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
        color: theme.iconTheme.color);
    final TextStyle iosStyle = TextStyle(
      color: theme.iconTheme.color,
    );
    final style = kIsWeb
        ? androidStyle
        : Platform.isAndroid
            ? androidStyle
            : iosStyle;
    final int year = DateTime.now().year;
    return GestureDetector(
      onTap: () {
        ZoomDrawer.of(context)!.toggle();
      },
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 10.0),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Consumer<ProfileProvider>(
                    builder: (_, state, child) {
                      return CircleAvatar(
                        backgroundColor: theme.iconTheme.color,
                        radius: 30,
                        child: CircleAvatar(
                          backgroundImage: (state.data.imagePath != null
                                  ? FileImage(File(state.data.imagePath!))
                                  : const AssetImage("assets/images/logo.png"))
                              as ImageProvider<Object>?,
                          radius: 28.0,
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 8.0),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Consumer<ProfileProvider>(
                    builder: (_, state, child) => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: 'dashboard.hello'.tr(),
                            style: theme.textTheme.bodyLarge,
                            children: [
                              TextSpan(
                                text: state.data.fullName?.toUpperCase(),
                                style: theme.textTheme.bodyLarge!.copyWith(
                                    fontFamily: CommonFonts.header,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        if (state.data.code != null)
                          RichText(
                            text: TextSpan(
                              text: 'ID : ',
                              style: TextStyle(
                                color: theme.colorScheme.background,
                                fontFamily: CommonFonts.header,
                              ),
                              children: [
                                TextSpan(
                                    text: state.data.code,
                                    style: theme.textTheme.bodyLarge!.copyWith(
                                        color: theme.colorScheme.background,
                                        fontFamily: CommonFonts.header,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
                Selector<MenuProvider, int>(
                  selector: (_, provider) => provider.currentPage,
                  builder: (_, index, __) => Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Consumer<ThemeProvider>(
                          builder: (_, state, child) => Row(children: [
                            state.isDarkmode
                                ? const Icon(
                                    CommonIcons.darkMode,
                                    size: 28.0,
                                  )
                                : const Icon(
                                    CommonIcons.lightMode,
                                    size: 28.0,
                                  ),
                            Switch.adaptive(
                                value: state.isDarkmode,
                                onChanged: (value) {
                                  state.toggleTheme(value);
                                }),
                          ]),
                        ),
                      ),
                      ...widget.mainMenu
                          .map((item) => MenuItemWidget(
                                key: Key(item.index.toString()),
                                item: item,
                                callback: widget.callback,
                                widthBox: widthBox,
                                style: style,
                                selected: index == item.index,
                              ))
                          .toList()
                    ],
                  ),
                ),
                const SizedBox(height: 20.0),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: ElevatedButton(
                    child: Text(
                      tr("drawer.logout").toUpperCase(),
                    ),
                    onPressed: () {
                      AuthStorage().clearUserLoginToken();
                      UserStorage().clearUser();
                      PrinterStorage().clearPrinter();
                      PrinterPaperSizeStorage().clearPrinterPaperSize();
                      context.read<MenuProvider>().clearCurrentPage();
                      context.read<ThemeProvider>().toggleTheme(false);
                      meteor.logout();
                      Navigator.of(context).pushReplacementNamed('/app');
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.transparent,
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                  'V 1.0.0 | Copyright \u00a9 $year Rabbit Technology. All Rights Reserved.'),
            )),
      ),
    );
  }
}

class MenuItemWidget extends StatelessWidget {
  final MenuClass? item;
  final Widget? widthBox;
  final TextStyle? style;
  final Function? callback;
  final bool? selected;

  const MenuItemWidget(
      {Key? key,
      this.item,
      this.widthBox,
      this.style,
      this.callback,
      this.selected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () => callback!(item!.index),
      // color:
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            item!.icon,
            color: style!.color,
            size: 24,
          ),
          widthBox!,
          Expanded(
            child: Text(
              item!.title.tr().toUpperCase(),
              style: style,
            ),
          )
        ],
      ),
    );
  }
}

class MenuClass {
  final String title;
  final IconData icon;
  final int index;

  const MenuClass(this.title, this.icon, this.index);
}
