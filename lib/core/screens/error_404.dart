import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '/core/utils/constants.dart';

class Error404 extends StatelessWidget {
  const Error404({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              SvgPicture.asset(
                'assets/images/404-error/404-error.svg',
                height: 180,
              ),
              const SizedBox(
                height: 40.0,
              ),
              Text(
                '404 Not Found',
                style: theme.textTheme.titleLarge!,
              ),
              const SizedBox(
                height: 10.0,
              ),
              Text(
                'It look like you\'re lost ...',
                textAlign: TextAlign.center,
                style: theme.textTheme.headlineSmall!,
              ),
              const SizedBox(
                height: 40.0,
              ),
              ElevatedButton.icon(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(CommonIcons.back),
                  label: const Text('Back'))
            ]),
          ),
        ),
      ),
    );
  }
}
