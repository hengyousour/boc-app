import 'package:flutter/material.dart';
import '../widgets/reset-new-password/reset_new_password_form.dart';
import '../widgets/responsive.dart';

class ResetPasswordByPhone extends StatelessWidget {
  const ResetPasswordByPhone({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Responsive(
                    desktop: Row(
                      children: const [
                        Spacer(),
                        Expanded(
                          child: ResetNewPasswordForm(),
                        ),
                        Spacer(),
                      ],
                    ),
                    tablet: Row(
                      children: const [
                        Spacer(),
                        Expanded(flex: 6, child: ResetNewPasswordForm()),
                        Spacer(),
                      ],
                    ),
                    mobile: const ResetNewPasswordForm(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
