import 'package:flutter/material.dart';
import '../../core/widgets/connection/connection_form.dart';
import '../../core/widgets/responsive.dart';

//ignore: must_be_immutable
class Connecting extends StatelessWidget {
  const Connecting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      backgroundColor: theme.primaryColor,
      body: Center(
        child: SingleChildScrollView(
          child: Responsive(
            desktop: Row(children: [
              const Spacer(),
              Expanded(
                child: ConnectionForm(theme: theme),
              ),
              const Spacer(),
            ]),
            tablet: Row(
              children: [
                const Spacer(),
                Expanded(flex: 2, child: ConnectionForm(theme: theme)),
                const Spacer(),
              ],
            ),
            mobile: ConnectionForm(theme: theme),
          ),
        ),
      ),
    );
  }
}
