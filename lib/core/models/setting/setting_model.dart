import 'package:json_annotation/json_annotation.dart';

part 'setting_model.g.dart';

@JsonSerializable()
class SettingModel {
  final String? ipAddress;
  final String? language;
  final String? printer;
  final String? printerPaperSize;
  final String? printerAddress;
  final int printerPort;
  const SettingModel({
    required this.ipAddress,
    required this.language,
    this.printer,
    this.printerPaperSize,
    this.printerAddress,
    required this.printerPort,
  });

  factory SettingModel.fromJson(Map<String, dynamic> data) =>
      _$SettingModelFromJson(data);
  Map<String, dynamic> toJson() => _$SettingModelToJson(this);
}
