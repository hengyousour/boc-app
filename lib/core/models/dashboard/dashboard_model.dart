import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DashboardModel {
  final int index;
  final String title;
  final Function(BuildContext) onPressed;
  final IconData iconName;
  final double iconSize;
  final List<Color> bgColors;

  const DashboardModel({
    required this.index,
    required this.title,
    required this.onPressed,
    required this.iconName,
    this.iconSize = 200.0,
    required this.bgColors,
  });
}
