import 'package:json_annotation/json_annotation.dart';
import '/core/models/auth/user_profile_model.dart';
import './email_model.dart';

part 'user_model.g.dart';

@JsonSerializable(explicitToJson: true)
class UserModel {
  @JsonKey(name: '_id')
  final String? id;
  @JsonKey(disallowNullValue: true)
  final String? username;
  @JsonKey(disallowNullValue: true)
  final String? fullName;
  @JsonKey(disallowNullValue: true)
  final String? code;
  @JsonKey(disallowNullValue: true)
  final String? imagePath;
  @JsonKey(disallowNullValue: true)
  final List<EmailModel>? emails;
  @JsonKey(disallowNullValue: true)
  final UserProfileModel? profile;

  const UserModel({
    required this.id,
    required this.username,
    this.fullName,
    this.code,
    this.imagePath,
    this.emails,
    this.profile,
  });

  factory UserModel.fromJson(Map<String, dynamic> data) =>
      _$UserModelFromJson(data);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
