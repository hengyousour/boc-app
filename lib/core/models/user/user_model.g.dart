// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    disallowNullValues: const [
      'username',
      'fullName',
      'code',
      'imagePath',
      'emails',
      'profile'
    ],
  );
  return UserModel(
    id: json['_id'] as String?,
    username: json['username'] as String?,
    fullName: json['fullName'] as String?,
    code: json['code'] as String?,
    imagePath: json['imagePath'] as String?,
    emails: (json['emails'] as List<dynamic>?)
        ?.map((e) => EmailModel.fromJson(e as Map<String, dynamic>))
        .toList(),
    profile: json['profile'] == null
        ? null
        : UserProfileModel.fromJson(json['profile'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UserModelToJson(UserModel instance) {
  final val = <String, dynamic>{
    '_id': instance.id,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('username', instance.username);
  writeNotNull('fullName', instance.fullName);
  writeNotNull('code', instance.code);
  writeNotNull('imagePath', instance.imagePath);
  writeNotNull('emails', instance.emails?.map((e) => e.toJson()).toList());
  writeNotNull('profile', instance.profile?.toJson());
  return val;
}
