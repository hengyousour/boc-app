// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reset_password_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResetPasswordModel _$ResetPasswordModelFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    disallowNullValues: const ['email', 'telephone', 'password'],
  );
  return ResetPasswordModel(
    email: json['email'] as String?,
    telephone: json['telephone'] as String?,
    password: json['password'] as String?,
  );
}

Map<String, dynamic> _$ResetPasswordModelToJson(ResetPasswordModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('email', instance.email);
  writeNotNull('telephone', instance.telephone);
  writeNotNull('password', instance.password);
  return val;
}
