// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_up_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignUpModel _$SignUpModelFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    disallowNullValues: const ['username'],
  );
  return SignUpModel(
    username: json['username'] as String?,
    password: json['password'] as String?,
    profile: json['profile'] == null
        ? null
        : UserProfileModel.fromJson(json['profile'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SignUpModelToJson(SignUpModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('username', instance.username);
  val['password'] = instance.password;
  val['profile'] = instance.profile?.toJson();
  return val;
}
