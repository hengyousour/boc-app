import 'package:json_annotation/json_annotation.dart';
import 'user_profile_model.dart';
part 'sign_up_model.g.dart';

@JsonSerializable(explicitToJson: true)
class SignUpModel {
  @JsonKey(disallowNullValue: true)
  final String? username;
  final String? password;
  final UserProfileModel? profile;

  const SignUpModel({
    this.username,
    required this.password,
    required this.profile,
  });

  factory SignUpModel.fromJson(Map<String, dynamic> data) =>
      _$SignUpModelFromJson(data);
  Map<String, dynamic> toJson() => _$SignUpModelToJson(this);
}
