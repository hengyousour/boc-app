// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_profile_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserProfileModel _$UserProfileModelFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    disallowNullValues: const ['code', 'isVerified'],
  );
  return UserProfileModel(
    fullName: json['fullName'] as String?,
    code: json['code'] as String?,
    telephone: json['telephone'] as String?,
    isVerified: json['isVerified'] as bool?,
  );
}

Map<String, dynamic> _$UserProfileModelToJson(UserProfileModel instance) {
  final val = <String, dynamic>{
    'fullName': instance.fullName,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('code', instance.code);
  val['telephone'] = instance.telephone;
  writeNotNull('isVerified', instance.isVerified);
  return val;
}
