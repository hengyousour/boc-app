import 'package:json_annotation/json_annotation.dart';
part 'user_profile_model.g.dart';

@JsonSerializable()
class UserProfileModel {
  final String? fullName;
  @JsonKey(disallowNullValue: true)
  final String? code;
  final String? telephone;
  @JsonKey(disallowNullValue: true)
  final bool? isVerified;
  const UserProfileModel({
    required this.fullName,
    this.code,
    required this.telephone,
    this.isVerified,
  });

  factory UserProfileModel.fromJson(Map<String, dynamic> data) =>
      _$UserProfileModelFromJson(data);
  Map<String, dynamic> toJson() => _$UserProfileModelToJson(this);
}
