// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LogInModel _$LogInModelFromJson(Map<String, dynamic> json) => LogInModel(
      username: json['username'] as String?,
      password: json['password'] as String?,
    );

Map<String, dynamic> _$LogInModelToJson(LogInModel instance) =>
    <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
    };
