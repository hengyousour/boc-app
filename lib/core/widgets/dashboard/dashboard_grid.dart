import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/dashboard_provider.dart';
import '../responsive.dart';
import 'dashboard_grid_list.dart';

class DashBoardGrid extends StatelessWidget {
  const DashBoardGrid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int crossAxisCount;
    return OrientationBuilder(
      builder: (context, orientation) {
        if (orientation == Orientation.portrait) {
          if (Responsive.isDesktop(context) | Responsive.isTablet(context)) {
            crossAxisCount = 4;
          } else {
            crossAxisCount = 2;
          }
        } else {
          crossAxisCount = 4;
        }
        return Consumer<DashboardProvider>(
          builder: (_, state, child) => GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: crossAxisCount,
              childAspectRatio: 1,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20,
            ),
            itemCount: state.dashboardList.length,
            itemBuilder: (ctx, i) {
              return DashBoardGridList(
                dashboard: state.dashboardList[i],
              );
            },
          ),
        );
      },
    );
  }
}
