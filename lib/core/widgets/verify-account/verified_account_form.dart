import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import '../../providers/sign_up_provider.dart';

class VerifiedAccountForm extends StatelessWidget {
  const VerifiedAccountForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        children: [
          Text(
            'Verified',
            style: theme.textTheme.titleLarge,
          ),
          const SizedBox(
            height: 10.0,
          ),
          Text(
            'Good job, your account is verified',
            style: theme.textTheme.titleLarge,
          ),
          const SizedBox(
            height: 40.0,
          ),
          SvgPicture.asset(
            'assets/images/sign-up/verified.svg',
            width: 150,
          ),
          Container(
            margin: const EdgeInsets.only(top: 40.0),
            width: double.infinity,
            height: 60.0,
            child: ElevatedButton(
                onPressed: () {
                  context.read<SignUpProvider>().clearState();
                  Navigator.pop(context);
                },
                child: const Text(
                  'LOGIN',
                )),
          ),
        ],
      ),
    );
  }
}
