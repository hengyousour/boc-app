import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pinput/pinput.dart';

import 'package:provider/provider.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '../../providers/reset_password_provider.dart';
import '../../providers/sign_up_provider.dart';
import '../../providers/verify_account_provider.dart';

class VerifyAccountForm extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();

  const VerifyAccountForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? userId = context.read<SignUpProvider>().userId != ""
        ? context.read<SignUpProvider>().userId
        : context.read<ResetPasswordProvider>().userId;

    final ThemeData theme = Theme.of(context);

    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: FormBuilder(
        key: _fbKey,
        child: Column(
          children: [
            Text(
              'Verification',
              style: theme.textTheme.titleLarge,
            ),
            const SizedBox(
              height: 10.0,
            ),
            Text(
              'Enter your verification code',
              style: theme.textTheme.headlineSmall,
            ),
            const SizedBox(
              height: 40.0,
            ),
            SvgPicture.asset(
              'assets/images/sign-up/verification.svg',
              width: 150,
            ),
            const SizedBox(
              height: 40.0,
            ),
            CodeTextField(
              userId: userId,
              theme: theme,
            ),
            Container(
              margin: const EdgeInsets.only(top: 20.0),
              width: double.infinity,
              height: 60.0,
              child: Consumer<ResetPasswordProvider>(
                builder: (_, state, child) => ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    textStyle: const TextStyle(color: Colors.white),
                    padding: const EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  onPressed: () => state.type == 'phone'
                      ? Navigator.pushReplacementNamed(
                          context, '/reset-password')
                      : Navigator.pushReplacementNamed(context, '/sign-up'),
                  child: Text(
                    state.type == 'phone' ? 'CHANGE PHONE NUMBER' : 'SIGNUP',
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            RichText(
              text: TextSpan(
                  text: 'Didn\'t you receive any code ?',
                  style: TextStyle(color: theme.iconTheme.color),
                  children: [
                    WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: Consumer<VerifyAccountProvider>(
                        builder: (_, state, child) =>
                            state.seconds == 0 || state.seconds == 30
                                ? TextButton(
                                    onPressed: () => context
                                        .read<VerifyAccountProvider>()
                                        .resendVerificationCode(
                                            userId: userId, context: context),
                                    style: TextButton.styleFrom(
                                        foregroundColor:
                                            theme.colorScheme.secondary,
                                        padding: const EdgeInsets.all(0.0),
                                        backgroundColor: Colors.transparent),
                                    child: const Text('Resend'),
                                  )
                                : Text(
                                    ' Wait (${state.seconds} s)',
                                    style: TextStyle(
                                        color: theme.colorScheme.secondary),
                                  ),
                      ),
                    )
                  ]),
            )
          ],
        ),
      ),
    );
  }
}

class CodeTextField extends StatefulWidget {
  final String? userId;
  final ThemeData theme;

  const CodeTextField({
    Key? key,
    required this.userId,
    required this.theme,
  }) : super(key: key);

  @override
  State<CodeTextField> createState() => _CodeTextFieldState();
}

class _CodeTextFieldState extends State<CodeTextField> {
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();

  // BoxDecoration get _pinPutDecoration {
  //   return BoxDecoration(
  //     color: Colors.white,
  //     border: Border.all(color: widget.theme.primaryColor),
  //     borderRadius: BorderRadius.circular(15.0),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 56,
      height: 56,
      textStyle: const TextStyle(
          fontSize: 20,
          color: Color.fromRGBO(30, 60, 87, 1),
          fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        border: Border.all(color: const Color.fromRGBO(234, 239, 243, 1)),
        borderRadius: BorderRadius.circular(20),
      ),
    );

    final focusedPinTheme = defaultPinTheme.copyDecorationWith(
      border: Border.all(color: const Color.fromRGBO(114, 178, 238, 1)),
      borderRadius: BorderRadius.circular(8),
    );

    final submittedPinTheme = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration!.copyWith(
        color: const Color.fromRGBO(234, 239, 243, 1),
      ),
    );
    return Pinput(
      defaultPinTheme: defaultPinTheme,
      focusedPinTheme: focusedPinTheme,
      submittedPinTheme: submittedPinTheme,
      pinAnimationType: PinAnimationType.scale,
      length: 4,
      keyboardType: TextInputType.number,
      autofocus: true,
      onCompleted: (String code) => context
          .read<VerifyAccountProvider>()
          .verifyAccount(userId: widget.userId, code: code, context: context),
      focusNode: _pinPutFocusNode,
      controller: _pinPutController,
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly,
      ],
    );
  }
}
