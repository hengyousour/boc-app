import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:form_builder_phone_field/form_builder_phone_field.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import '/core/utils/static_theme.dart';
import 'package:provider/provider.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '../../models/auth/sign_up_model.dart';
import '../../models/auth/user_profile_model.dart';
import '../../providers/sign_up_provider.dart';
import '../../services/select_options/static_options.dart';
import '../../utils/alert.dart';
import '../../utils/constants.dart';
import '../../utils/custom_form_build_style.dart';
import '../../utils/custom_login_input.dart';
import '../../utils/get_form_error_text.dart';

class SignUpForm extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();
  final List<FormBuilderFieldOption> _genderOpts = [];

  SignUpForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    for (var genderOpt in StaticOptions().genderOpts) {
      _genderOpts.add(FormBuilderFieldOption(
        value: genderOpt.value,
        child: Text(genderOpt.label!),
      ));
    }
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: FormBuilder(
        key: _fbKey,
        child: Column(
          children: [
            Text(
              'Sign Up',
              style: theme.textTheme.displayLarge,
            ),
            const SizedBox(
              height: 20.0,
            ),
            Text(
              'Let\'s start create your account',
              style: theme.textTheme.titleLarge,
            ),
            const SizedBox(
              height: 20.0,
            ),
            SvgPicture.asset(
              'assets/images/sign-up/sign-up.svg',
              width: 150.0,
            ),
            const SizedBox(
              height: 20.0,
            ),
            Container(
              decoration: kBoxDecorationStyle(
                color: theme.primaryColor,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: FormBuilderTextField(
                name: 'fullName',
                textAlignVertical: TextAlignVertical.center,
                cursorColor: theme.iconTheme.color,
                style: TextStyle(
                  fontFamily: CommonFonts.body,
                  color: theme.iconTheme.color,
                ),
                decoration: fbLoginTextFieldStyle(
                    labelText: "Full Name",
                    icon: CommonIcons.fullName,
                    theme: theme),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
            ),
            const SizedBox(height: 20.0),
            Container(
              decoration: kBoxDecorationStyle(
                color: theme.primaryColor,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: Consumer<SignUpProvider>(
                builder: (_, state, child) => FormBuilderTextField(
                  name: 'password',
                  obscureText: state.obsurceText,
                  cursorColor: theme.iconTheme.color,
                  textAlignVertical: TextAlignVertical.center,
                  style: TextStyle(
                    fontFamily: CommonFonts.body,
                    color: theme.iconTheme.color,
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  decoration: fbLoginTextFieldStyle(
                    labelText: "Password",
                    icon: CommonIcons.password,
                    theme: theme,
                    suffixIsExist: true,
                    suffixIcon: (state.obsurceText)
                        ? CommonIcons.hide
                        : CommonIcons.show,
                    onPressedSuffixIcon: () =>
                        context.read<SignUpProvider>().toggleSuffixIcon(),
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                  ]),
                ),
              ),
            ),
            const SizedBox(height: 20.0),
            Container(
              decoration: kBoxDecorationStyle(
                color: theme.primaryColor,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: Theme(
                data: StaticTheme.theme.phoneFieldTheme(theme: theme),
                child: FormBuilderPhoneField(
                  name: 'telephone',
                  keyboardType: TextInputType.phone,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  defaultSelectedCountryIsoCode: 'KH',
                  countryFilterByIsoCode: const ['KH'],
                  cursorColor: theme.iconTheme.color,
                  style: TextStyle(
                    fontFamily: CommonFonts.body,
                    color: theme.iconTheme.color,
                  ),
                  decoration: fbPhoneFieldStyle(
                      labelText: "Phone Number",
                      icon: CommonIcons.telephone,
                      theme: theme),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                    FormBuilderValidators.numeric()
                  ]),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 40.0),
              width: double.infinity,
              height: 60.0,
              child: ElevatedButton(
                onPressed: () {
                  if (_fbKey.currentState!.saveAndValidate()) {
                    Map<String, dynamic> tempFormDoc = SignUpModel(
                      username: _fbKey.currentState!.value['username'],
                      password: _fbKey.currentState!.value['password'],
                      profile: UserProfileModel(
                        fullName: _fbKey.currentState!.value['fullName'],
                        telephone: _fbKey.currentState!.value['telephone'],
                      ),
                    ).toJson();

                    final SignUpModel formDoc =
                        SignUpModel.fromJson(tempFormDoc);

                    context
                        .read<SignUpProvider>()
                        .signUp(formDoc: formDoc, context: context);
                    _fbKey.currentState!.reset();
                  } else {
                    String errorText =
                        getFormErrorText(formState: _fbKey.currentState!)
                            .toLowerCase();

                    Alert().show(
                        message: errorText,
                        alertIcon: CommonIcons.alertError,
                        alertBackGroundColor: CommonColors.errorLight,
                        context: context);
                  }
                },
                child: const Text(
                  'SIGNUP',
                ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            RichText(
              text: TextSpan(
                text: 'Already have an account ?',
                style: TextStyle(color: theme.iconTheme.color),
                children: [
                  WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: TextButton(
                      onPressed: () => Navigator.pop(context),
                      style: TextButton.styleFrom(
                        minimumSize: Size.zero,
                        padding: EdgeInsets.zero,
                      ),
                      child: const Text('Log In'),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
