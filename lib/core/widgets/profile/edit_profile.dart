import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '/core/utils/custom_form_build_style.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Visibility(
              visible: false,
              maintainState: true,
              child: FormBuilderTextField(
                name: '_id',
                decoration: fbTextFieldStyle(
                  labelText: "ID",
                  theme: theme,
                ),
              ),
            ),
            // FormBuilderTextField(
            //   name: 'code',
            //   decoration: fbTextFieldStyle(
            //     labelText: "ID",
            //     theme: theme,
            //   ),
            // ),
            // const SizedBox(
            //   height: 10.0,
            // ),
            FormBuilderTextField(
              name: 'fullName',
              decoration: fbTextFieldStyle(
                labelText: "Full Name",
                theme: theme,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
