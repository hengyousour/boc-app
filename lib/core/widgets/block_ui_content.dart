import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class BlockUIContent extends StatelessWidget {
  final bool emptyData;
  final bool processingData;
  final String msg;
  final String msgEmptyData;
  final String msgProcessingData;
  const BlockUIContent({
    Key? key,
    this.emptyData = false,
    this.processingData = false,
    this.msg = 'Look like you haven\'t made your choice yet ...',
    this.msgEmptyData =
        'We\'re unable to find the data that you\'re looking for ...',
    this.msgProcessingData = 'Please, wait while we set thing up for you!',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: emptyData == true
                ? [
                    LoadingAnimationWidget.beat(
                      color: theme.iconTheme.color!,
                      size: 48.0,
                    ),
                    const SizedBox(height: 10.0),
                    Text('No data found', style: theme.textTheme.titleLarge),
                    Text(msgEmptyData, textAlign: TextAlign.center),
                  ]
                : [
                    LoadingAnimationWidget.inkDrop(
                      color: theme.iconTheme.color!,
                      size: 48.0,
                    ),
                    const SizedBox(height: 10.0),
                    Text('Processing', style: theme.textTheme.titleLarge),
                    Text(msgProcessingData, textAlign: TextAlign.center),
                  ]),
      ),
    );
  }
}
