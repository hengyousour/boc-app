import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import '../utils/constants.dart';

class WaitingData extends StatelessWidget {
  final String? text;
  final Color? textColor;
  final Color? loadingColor;
  const WaitingData(
      {Key? key,
      this.text,
      required this.textColor,
      required this.loadingColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          text ?? 'Loading',
          style: TextStyle(
              fontFamily: CommonFonts.body, fontSize: 20.0, color: textColor),
        ),
        const SizedBox(
          height: 10.0,
        ),
        LoadingAnimationWidget.beat(
          color: loadingColor!,
          size: 48.0,
        ),
      ],
    );
  }
}
