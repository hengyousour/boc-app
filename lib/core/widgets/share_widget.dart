import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import '/core/utils/constants.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';

class ShareWidget extends StatelessWidget {
  final Widget child;
  final ScreenshotController _screenshotController = ScreenshotController();
  final ValueChanged<bool> shareReady;
  ShareWidget({Key? key, required this.child, required this.shareReady})
      : super(key: key);

  _takeScreenShotAndShare({required BuildContext context}) async {
    shareReady(false);
    await _screenshotController
        .capture(delay: const Duration(milliseconds: 10))
        .then((Uint8List? image) async {
      if (image == null) return;
      final directory = await getApplicationDocumentsDirectory();
      String fileName = DateTime.now().toString();
      final imagePath = await File('${directory.path}/$fileName.png').create();
      final Uint8List pngBytes = image.buffer.asUint8List();
      await imagePath.writeAsBytes(pngBytes);

      /// Share Plugin
      // ignore: use_build_context_synchronously
      Size size = MediaQuery.of(context).size;

      // ignore: deprecated_member_use
      await Share.shareFiles(
        [imagePath.path],
        sharePositionOrigin: Rect.fromLTWH(0, 0, size.width, size.height / 2),
      );
      shareReady(true);
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Screenshot(
        controller: _screenshotController,
        child: Container(color: theme.scaffoldBackgroundColor, child: child),
      ),
      Align(
        alignment: Alignment.bottomRight,
        child: TextButton.icon(
          style: TextButton.styleFrom(
              foregroundColor: theme.iconTheme.color,
              padding: const EdgeInsets.all(0.0)),
          onPressed: () => _takeScreenShotAndShare(context: context),
          icon: const Icon(CommonIcons.share),
          label: const Text('Share'),
        ),
      ),
    ]);
  }
}
