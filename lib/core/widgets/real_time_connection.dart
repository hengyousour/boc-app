import 'package:flutter/material.dart';
import '../../app.dart';
import '../utils/block_ui.dart';
import '../utils/constants.dart';

class RealTimeConnection extends StatefulWidget {
  final bool isDailogOpen;
  const RealTimeConnection({Key? key, this.isDailogOpen = false})
      : super(key: key);

  @override
  State<RealTimeConnection> createState() => _RealTimeConnectionState();
}

class _RealTimeConnectionState extends State<RealTimeConnection> {
  @override
  void initState() {
    super.initState();
    // check if has any block ui open or not after disconnect from server
    // if exist hide block ui
    if (widget.isDailogOpen) {
      BlockUI.hide(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Card(
      margin: EdgeInsets.zero,
      elevation: 10.0,
      child: ListTile(
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(CommonIcons.noConnection, color: theme.iconTheme.color),
          ],
        ),
        title: const Text('Oops, Can\'t Connect To Server'),
        subtitle:
            const Text('Please, Check Your Internet Connection And Try Again'),
        trailing: ElevatedButton(
            onPressed: () {
              meteor.reconnect();
            },
            child: const Text('Retry')),
      ),
    );
  }
}
