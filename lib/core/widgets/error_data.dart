import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import '../utils/constants.dart';

class ErrorData extends StatelessWidget {
  final Color iconColor;
  final Color textColor;
  final Color loadingColor;
  const ErrorData(
      {Key? key,
      required this.iconColor,
      required this.textColor,
      required this.loadingColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.sentiment_dissatisfied,
          size: 40.0,
          color: iconColor,
        ),
        const SizedBox(
          height: 10.0,
        ),
        Text(
          'Error 404 Data Not Found',
          style: TextStyle(
              fontFamily: CommonFonts.body, fontSize: 20.0, color: textColor),
        ),
        const SizedBox(
          height: 10.0,
        ),
        LoadingAnimationWidget.beat(
          color: loadingColor,
          size: 48.0,
        ),
      ],
    );
  }
}
