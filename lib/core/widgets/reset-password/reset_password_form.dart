import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_phone_field/form_builder_phone_field.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/svg.dart';
import '../../models/auth/reset_password_model.dart';
import '../../providers/reset_password_provider.dart';
import '../../utils/alert.dart';
import '../../utils/constants.dart';
import '../../utils/custom_form_build_style.dart';
import '../../utils/custom_login_input.dart';
import '../../utils/get_form_error_text.dart';
import '../../utils/static_theme.dart';

class ResetPasswordForm extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();

  const ResetPasswordForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String? type = context.read<ResetPasswordProvider>().type;
    final theme = Theme.of(context);
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: FormBuilder(
        key: _fbKey,
        child: Column(
          children: [
            Text(
              type == 'email'
                  ? 'Enter your registered email below to receive password reset instrustion'
                  : 'Enter your registered phone number below to receive confirmation code',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: theme.iconTheme.color,
                fontFamily: CommonFonts.header,
              ),
            ),
            const SizedBox(
              height: 40.0,
            ),
            SvgPicture.asset(
              type == 'email'
                  ? 'assets/images/reset-password/email.svg'
                  : 'assets/images/reset-password/password.svg',
              width: 150.0,
            ),
            const SizedBox(
              height: 40.0,
            ),
            Container(
              decoration: kBoxDecorationStyle(
                color: theme.primaryColor,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: (type == 'email')
                  ? FormBuilderTextField(
                      name: 'email',
                      textAlignVertical: TextAlignVertical.center,
                      cursorColor: theme.iconTheme.color,
                      style: TextStyle(
                        fontFamily: CommonFonts.body,
                        color: theme.iconTheme.color,
                      ),
                      keyboardType: TextInputType.emailAddress,
                      decoration: fbLoginTextFieldStyle(
                          labelText: "Email",
                          icon: type == 'email'
                              ? CommonIcons.email
                              : CommonIcons.telephone,
                          theme: theme),
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                        FormBuilderValidators.email()
                      ]),
                    )
                  : Theme(
                      data: StaticTheme.theme.phoneFieldTheme(theme: theme),
                      child: FormBuilderPhoneField(
                        name: 'telephone',
                        keyboardType: TextInputType.phone,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                        defaultSelectedCountryIsoCode: 'KH',
                        countryFilterByIsoCode: const ['KH'],
                        cursorColor: theme.iconTheme.color,
                        style: TextStyle(
                          fontFamily: CommonFonts.body,
                          color: theme.iconTheme.color,
                        ),
                        decoration: fbPhoneFieldStyle(
                            labelText: "Phone Number",
                            icon: CommonIcons.telephone,
                            theme: theme),
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(),
                          FormBuilderValidators.numeric()
                        ]),
                      ),
                    ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 40.0),
                width: double.infinity,
                height: 60.0,
                child: Consumer<ResetPasswordProvider>(
                  builder: (_, state, child) => ElevatedButton(
                    onPressed: state.loading
                        ? null
                        : () {
                            if (_fbKey.currentState!.saveAndValidate()) {
                              final formDoc = ResetPasswordModel.fromJson(
                                  _fbKey.currentState!.value);
                              context
                                  .read<ResetPasswordProvider>()
                                  .resetPassword(
                                      formDoc: formDoc, context: context);
                            } else {
                              String errorText = getFormErrorText(
                                      formState: _fbKey.currentState!)
                                  .toLowerCase();

                              Alert().show(
                                  message: errorText,
                                  alertIcon: CommonIcons.alertError,
                                  alertBackGroundColor: CommonColors.errorLight,
                                  context: context);
                            }
                          },
                    child: Text(
                      state.loading ? 'SENDING...' : 'SEND',
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
