import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {
  final Widget mobile;
  final Widget tablet;
  final Widget desktop;

  const Responsive({
    Key? key,
    required this.mobile,
    required this.tablet,
    required this.desktop,
  }) : super(key: key);

  // breakpoint platform
  static int mobileBreakPoint = 650;
  static int tabletBreakPoint = 768;
  static int desktopBreakPoint = 1366;

  // This isMobile, isTablet, isDesktop helep us later
  static bool isMobile(BuildContext context) =>
      MediaQuery.of(context).size.width < tabletBreakPoint;

  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width <= desktopBreakPoint &&
      MediaQuery.of(context).size.width >= tabletBreakPoint;

  static bool isDesktop(BuildContext context) =>
      MediaQuery.of(context).size.width >= desktopBreakPoint;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      // If our width is more than 1366 then we consider it a desktop
      builder: (context, constraints) {
        if (constraints.maxWidth >= desktopBreakPoint) {
          return desktop;
        }
        // If width it less then 1366 and more then 768 we consider it as tablet
        else if (constraints.maxWidth >= tabletBreakPoint) {
          return tablet;
        }
        // Or less then that we called it mobile
        else {
          return mobile;
        }
      },
    );
  }
}
