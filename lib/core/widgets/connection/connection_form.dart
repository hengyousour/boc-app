import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import '/core/utils/custom_form_build_style.dart';
import '../../../core/widgets/responsive.dart';
import '../../../core/storages/connection_storage.dart';
import '../../../core/utils/constants.dart';
import '../../../app.dart';

class ConnectionForm extends StatelessWidget {
  const ConnectionForm({
    Key? key,
    required ThemeData theme,
  })  : _theme = theme,
        super(key: key);

  final ThemeData _theme;

  @override
  Widget build(BuildContext context) {
    return FadeIn(
      duration: const Duration(milliseconds: 1800),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Material(
              color: Colors.transparent,
              child: IconButton(
                splashRadius: 35.0,
                icon: const Icon(CommonIcons.noConnection),
                // splashColor: CommonColors.secondary.withOpacity(0.1),
                onPressed: () => settingModalBottomSheet(context),
                iconSize: 60,
                // color: CommonColors.secondary,
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            LoadingAnimationWidget.staggeredDotsWave(
              color: _theme.iconTheme.color!,
              size: 48.0,
            ),
            const SizedBox(
              height: 10.0,
            ),
            Text('Oops, Can\'t Connect To Server',
                textAlign: TextAlign.center,
                style: _theme.textTheme.displaySmall),
            const SizedBox(
              height: 10.0,
            ),
            Text(
              'Please Check Your Internet Connection Or IP Address And Try Again',
              textAlign: TextAlign.center,
              style: _theme.textTheme.titleLarge!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
            const SizedBox(
              height: 60.0,
            ),
            SizedBox(
              width: 100.0,
              child: ElevatedButton(
                onPressed: () {
                  meteor.reconnect();
                },
                child: const Text(
                  'Retry',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

void settingModalBottomSheet(
  BuildContext context,
) {
  final ThemeData theme = Theme.of(context);
  final ConnectionStorage connectionStorage = ConnectionStorage();

  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Wrap(
          children: <Widget>[
            ListTile(
              leading: const Icon(CommonIcons.ipAddress),
              title: const Text(
                'IP Address',
                style: TextStyle(fontFamily: CommonFonts.body),
              ),
              onTap: () async {
                Navigator.pop(context);
                showDialog(
                    context: context,
                    builder: (_) {
                      return Responsive(
                          mobile: SettingForm(
                              theme: theme,
                              connectionStorage: connectionStorage),
                          tablet: Row(
                            children: [
                              const Spacer(),
                              Expanded(
                                flex: 4,
                                child: SettingForm(
                                    theme: theme,
                                    connectionStorage: connectionStorage),
                              ),
                              const Spacer(),
                            ],
                          ),
                          desktop: Row(
                            children: [
                              const Spacer(),
                              Expanded(
                                child: SettingForm(
                                    theme: theme,
                                    connectionStorage: connectionStorage),
                              ),
                              const Spacer()
                            ],
                          ));
                    });
              },
            ),
            ListTile(
              leading: const Icon(CommonIcons.qrCode),
              title: const Text(
                'QR Code',
                style: TextStyle(fontFamily: CommonFonts.body),
              ),
              onTap: () async {
                Navigator.pop(context);
                FlutterBarcodeScanner.scanBarcode(
                        "#000000", "Cancel", true, ScanMode.DEFAULT)
                    .then((result) async {
                  if (result != '-1') {
                    connectionStorage.setIpAddress(ip: result);
                    Phoenix.rebirth(context);
                  }
                });
              },
            ),
          ],
        );
      });
}

class SettingForm extends StatelessWidget {
  const SettingForm({
    Key? key,
    required ThemeData theme,
    required ConnectionStorage connectionStorage,
  })  : _theme = theme,
        _connectionStorage = connectionStorage,
        super(key: key);

  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();
  final ThemeData _theme;
  final ConnectionStorage _connectionStorage;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(10.0),
      title: const Text(
        'Setting',
        textAlign: TextAlign.center,
      ),
      content: FormBuilder(
        key: _fbKey,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: FormBuilderTextField(
                name: "ipAddress",
                decoration:
                    fbTextFieldStyle(labelText: 'Ip Address', theme: _theme),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                  FormBuilderValidators.ip()
                ]),
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: FormBuilderTextField(
                name: "port",
                initialValue: '3000',
                decoration: fbTextFieldStyle(labelText: 'Port', theme: _theme),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                  FormBuilderValidators.integer()
                ]),
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width * 1,
          child: ElevatedButton(
            onPressed: () async {
              if (_fbKey.currentState!.saveAndValidate()) {
                String ip =
                    '${_fbKey.currentState!.fields['ipAddress']!.value}:${_fbKey.currentState!.fields['port']!.value}';
                _connectionStorage.setIpAddress(ip: ip);
                Navigator.pop(context);
                Phoenix.rebirth(context);
              }
            },
            child: const Text(
              'Set Up',
            ),
          ),
        )
      ],
    );
  }
}
