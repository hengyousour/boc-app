import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/svg.dart';
import '../../models/auth/reset_password_model.dart';
import '../../providers/reset_password_provider.dart';
import '../../providers/sign_up_provider.dart';
import '../../utils/alert.dart';
import '../../utils/constants.dart';
import '../../utils/custom_form_build_style.dart';
import '../../utils/custom_login_input.dart';
import '../../utils/get_form_error_text.dart';

class ResetNewPasswordForm extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();

  const ResetNewPasswordForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: FormBuilder(
        key: _fbKey,
        child: Column(
          children: [
            Text(
              'Reset Password',
              style: theme.textTheme.displayLarge,
            ),
            const SizedBox(
              height: 10.0,
            ),
            Text(
              'Enter your new password below to reset and log in again',
              style: theme.textTheme.titleLarge,
            ),
            const SizedBox(
              height: 20.0,
            ),
            SvgPicture.asset(
              'assets/images/reset-password/reset-password.svg',
              width: 150.0,
            ),
            const SizedBox(
              height: 20.0,
            ),
            Container(
              decoration: kBoxDecorationStyle(
                color: CommonColors.secondaryLight,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: Consumer<SignUpProvider>(
                builder: (_, state, child) => FormBuilderTextField(
                  name: 'password',
                  textAlignVertical: TextAlignVertical.center,
                  obscureText: state.obsurceText,
                  cursorColor: theme.iconTheme.color,
                  style: TextStyle(
                    fontFamily: CommonFonts.body,
                    color: theme.iconTheme.color,
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  decoration: fbLoginTextFieldStyle(
                    labelText: "New Password",
                    icon: CommonIcons.password,
                    theme: theme,
                    suffixIsExist: true,
                    suffixIcon: (state.obsurceText)
                        ? CommonIcons.hide
                        : CommonIcons.show,
                    onPressedSuffixIcon: () =>
                        context.read<SignUpProvider>().toggleSuffixIcon(),
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                  ]),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 40.0),
              width: double.infinity,
              height: 60.0,
              child: Consumer<ResetPasswordProvider>(
                builder: (_, state, child) => ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    textStyle: const TextStyle(color: Colors.white),
                    padding: const EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  onPressed: state.loading
                      ? null
                      : () {
                          if (_fbKey.currentState!.saveAndValidate()) {
                            final formDoc = ResetPasswordModel.fromJson(
                                _fbKey.currentState!.value);
                            context
                                .read<ResetPasswordProvider>()
                                .resetNewPassword(
                                  formDoc: formDoc,
                                  context: context,
                                );
                          } else {
                            String errorText = getFormErrorText(
                                    formState: _fbKey.currentState!)
                                .toLowerCase();

                            Alert().show(
                                message: errorText,
                                alertIcon: CommonIcons.alertError,
                                alertBackGroundColor: CommonColors.errorLight,
                                context: context);
                          }
                        },
                  child: Text(
                    state.loading ? 'RESETTING...' : 'RESET',
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            RichText(
              text: TextSpan(
                  text: 'Remember password or want to go back ?',
                  style: TextStyle(color: theme.iconTheme.color),
                  children: [
                    WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: TextButton(
                        onPressed: () {
                          context.read<ResetPasswordProvider>().clearState();
                          Navigator.pop(context);
                        },
                        style: TextButton.styleFrom(
                            foregroundColor: theme.colorScheme.secondary,
                            padding: const EdgeInsets.all(0.0),
                            backgroundColor: Colors.transparent),
                        child: const Text('Log In'),
                      ),
                    )
                  ]),
            )
          ],
        ),
      ),
    );
  }
}
