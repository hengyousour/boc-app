import 'package:flutter/material.dart';
import '../utils/constants.dart';

Widget appBarWidget({
  required final String title,
  TextStyle? titleStyle,
  final Widget? customLeading,
  final IconData? actionIcon,
  final Color? actionColor,
  final Function? actionOnPress,
  final Color? backgroundColor,
}) {
  return AppBar(
    elevation: 0.0,
    leading: customLeading,

    actions: <Widget>[
      IconButton(
          icon: Icon(
            actionIcon,
          ),
          color: actionColor,
          onPressed: actionOnPress as void Function()?),
    ],
    backgroundColor: backgroundColor,
    // flexibleSpace: Container(
    //   decoration: BoxDecoration(
    //     gradient: LinearGradient(
    //       begin: Alignment.centerLeft,
    //       end: Alignment.centerRight,
    //       colors: backgroundColor,
    //     ),
    //   ),
    // ),
    title: Text(
      title.toUpperCase(),
      style: titleStyle,
    ),
    centerTitle: true,
  );
}

Widget commonAppBarWidget({
  required final String title,
  required final Color titleColor,
  final Color? backButtonColor,
  // @required final List<Color> backgroundColor,
  final Color? backgroundColor,
}) {
  return AppBar(
    elevation: 0.0,
    leading: BackButton(color: backButtonColor),
    backgroundColor: backgroundColor,
    // flexibleSpace: Container(
    //   decoration: BoxDecoration(
    //     gradient: LinearGradient(
    //       begin: Alignment.centerLeft,
    //       end: Alignment.centerRight,
    //       colors: backgroundColor,
    //     ),
    //   ),
    // ),
    title: Text(
      title.toUpperCase(),
      style: TextStyle(
        color: titleColor,
        fontFamily: CommonFonts.header,
      ),
    ),
    centerTitle: true,
  );
}
