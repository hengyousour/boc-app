import 'package:flutter/material.dart';
import '../../app.dart';

class ConnectionProvider with ChangeNotifier {
  bool _isConnected = false;
  bool get isConnected => _isConnected;
  void realTimeConnection() {
    meteor.status().listen((event) async {
      _isConnected = event.connected;
      notifyListeners();
    });
  }
}
