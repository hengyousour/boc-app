import 'package:dart_meteor/dart_meteor.dart';
import 'dart:async';
// import 'package:dart_meteor_web/dart_meteor_web.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '/core/providers/reset_password_provider.dart';
import '/core/utils/alert.dart';
import '/core/utils/constants.dart';
import '../../app.dart';

class VerifyAccountProvider with ChangeNotifier {
  bool _loading = false;
  bool get loading => _loading;
  int _seconds = 30;
  int get seconds => _seconds;
  late Timer _timer;

  void verifyAccount({
    required String? userId,
    required String code,
    required BuildContext context,
  }) {
    _loading = true;
    meteor.call('verifyAccount', args: [
      {
        'userId': userId,
        'code': code,
      }
    ]).then((result) {
      _loading = false;
      String? resetPasswordType = context.read<ResetPasswordProvider>().type;
      if (resetPasswordType != null && resetPasswordType == "phone") {
        Navigator.pushReplacementNamed(context, '/reset-password-by-phone');
      } else {
        Navigator.pushReplacementNamed(context, '/verified-account');
      }
      notifyListeners();
    }).catchError((err) {
      if (err is MeteorError) {
        Alert().show(
            message: err.message,
            alertIcon: CommonIcons.alertError,
            alertBackGroundColor: CommonColors.errorLight,
            context: context);
        _loading = false;
        notifyListeners();
      }
    });
    notifyListeners();
  }

  void _startTimer() {
    _seconds = 30;
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_seconds > 0) {
        _seconds--;
        notifyListeners();
      } else {
        _timer.cancel();
      }
    });
  }

  void resendVerificationCode({
    required String? userId,
    required BuildContext context,
  }) {
    meteor.call('resendCode', args: [
      {'userId': userId}
    ]).then((result) {
      Alert().show(
        message: 'Good luck, we already send you a new verification code',
        alertIcon: CommonIcons.alertSuccess,
        alertBackGroundColor: CommonColors.successLight,
        context: context,
      );
      //start timer
      _startTimer();
      notifyListeners();
    }).catchError((err) {
      if (err is MeteorError) {
        Alert().show(
            message: err.message,
            alertIcon: CommonIcons.alertError,
            alertBackGroundColor: CommonColors.errorLight,
            context: context);
      }
    });
  }
}
