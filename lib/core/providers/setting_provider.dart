import 'dart:typed_data';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_printer/flutter_bluetooth_printer.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';
// ignore: depend_on_referenced_packages
import 'package:esc_pos_utils/esc_pos_utils.dart' as esc;
import 'package:image/image.dart' as i;
import 'package:screenshot/screenshot.dart';
import '../models/select-option/select_option_model.dart';
import '../storages/printer_paper_size_storage.dart';
import '../storages/printer_storage.dart';
import '../utils/alert.dart';
import '../utils/constants.dart';
import '../models/setting/setting_model.dart';
import '../storages/connection_storage.dart';
import '../storages/user_storage.dart';

class SettingProvider with ChangeNotifier {
  SettingModel? _data;
  SettingModel? get data => _data;
  bool _isLoading = false;
  bool get isLoading => _isLoading;
  ScreenshotController screenshotController = ScreenshotController();
  //Bluetooth Printer
  ReceiptController? receiptController;
  BluetoothDevice _btDeviceInfo = BluetoothDevice(name: '', address: '');
  BluetoothDevice get btDeviceInfo => _btDeviceInfo;
  final List<SelectOptionModel> _paperSizeOpts = const [
    SelectOptionModel(label: '58 mm', value: '58mm'),
    // SelectOptionModel(label: '72 mm', value: '72mm'),
    SelectOptionModel(label: '80 mm', value: '80mm')
  ];
  List<SelectOptionModel> get paperSizeOpts => _paperSizeOpts;
  String _paperSize = '';
  String get paperSize => _paperSize;
  //Wifi/NetWork Printer
  String _printerIpAddress = '';
  String get printerIpAddress => _printerIpAddress;
  int _printerPort = 9100;
  int get printerPort => _printerPort;

  void setBluetoothDeviceInfo({required BluetoothDevice btDeviceInfo}) async {
    _btDeviceInfo = btDeviceInfo;
    notifyListeners();
  }

  void setPaperSize({required String paperSize}) {
    _paperSize = paperSize;
  }

  void setWifiOrNetworkPrinter({String? ip, int? port}) {
    _printerIpAddress = ip ?? '';
    if (port != null) _printerPort = port;
  }

  void getSettingDocFromLocalStorage() async {
    _isLoading = true;
    // init bluetooth device info
    Map<String, dynamic> printerBTInfo = await PrinterStorage().getPrinter();
    setBluetoothDeviceInfo(
        btDeviceInfo: BluetoothDevice(
            name: printerBTInfo['name'],
            address: printerBTInfo['address'] ?? '',
            type: printerBTInfo['type']));
    //init Wifi/Network Printer
    Map<String, dynamic> printerWNInfo = await PrinterStorage().getWNPrinter();
    setWifiOrNetworkPrinter(
        ip: printerWNInfo['address'], port: printerWNInfo['port']);
    //init paper size
    String? printerPaperSize =
        await PrinterPaperSizeStorage().getPrinterPaperSize();
    setPaperSize(paperSize: printerPaperSize ?? '');
    //init setting data
    _data = SettingModel(
        ipAddress: await ConnectionStorage().getIpAddress(),
        language: await UserStorage().getLanguage(),
        printer: _btDeviceInfo.name,
        printerPaperSize: await PrinterPaperSizeStorage().getPrinterPaperSize(),
        printerAddress: _printerIpAddress,
        printerPort: _printerPort);
    _isLoading = false;
    notifyListeners();
  }

  Future<void> printEvent() async {
    //check printer address exist or not
    if (btDeviceInfo.address.isNotEmpty) {
      //check paper size
      late PaperSize paperSize;
      switch (_paperSize) {
        case '72mm':
          paperSize = PaperSize.mm72;
          break;
        case '80mm':
          paperSize = PaperSize.mm80;
          break;
        default:
          paperSize = PaperSize.mm58;
      }
      //set paper size
      receiptController!.paperSize = paperSize;
      //start printing
      return receiptController!
          .print(address: _btDeviceInfo.address, linesAfter: 1);
    } else {
      throw ("Printer not found.");
    }
  }

  Future<void> _startPrint(NetworkPrinter printer, Uint8List img) async {
    final i.Image? image = i.decodeImage(img);
    printer.image(image!, align: esc.PosAlign.center);
    printer.cut();
    printer.disconnect();
  }

  Future<void> printByIpAddressEvent() async {
    const esc.PaperSize paper = esc.PaperSize.mm80;
    final profile = await esc.CapabilityProfile.load();
    final printer = NetworkPrinter(paper, profile);
    // connect printer by ip address
    final PosPrintResult res =
        await printer.connect(_printerIpAddress, port: _printerPort);

    if (res == PosPrintResult.success) {
      screenshotController.capture(pixelRatio: 2.0).then((capturedImage) async {
        return _startPrint(printer, capturedImage!);
      });
    } else {
      throw (res.msg);
    }
  }

  void insertSetting({
    required SettingModel formDoc,
    required BuildContext context,
  }) async {
    if (formDoc.language == "km") {
      context.setLocale(const Locale('en', 'GB'));
    }

    if (formDoc.language == "en") {
      context.setLocale(const Locale('en', 'US'));
    }

    UserStorage().setLanguage(lang: formDoc.language);

    // Bluetooth Printer
    PrinterStorage().setPrinter(
        name: _btDeviceInfo.name ?? '',
        address: _btDeviceInfo.address,
        type: _btDeviceInfo.type ?? 0);

    if (formDoc.printerPaperSize != null) {
      PrinterPaperSizeStorage()
          .setPrinterPaperSize(paperSize: formDoc.printerPaperSize!);
    }

    // Wifi / Network Printer
    PrinterStorage().setWNPrinter(
        address: formDoc.printerAddress ?? '', port: formDoc.printerPort);

    Alert().show(
      message: 'Good job, your setting has been successfully updated.',
      alertIcon: CommonIcons.alertSuccess,
      alertBackGroundColor: CommonColors.successLight,
      context: context,
    );
  }
}
