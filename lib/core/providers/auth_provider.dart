import 'package:dart_meteor/dart_meteor.dart';
// import 'package:dart_meteor_web/dart_meteor_web.dart';
import 'package:flutter/widgets.dart';
import '../utils/alert.dart';
import '../utils/constants.dart';
import '../models/auth/login_model.dart';
import '../storages/auth_storage.dart';
import '../storages/user_storage.dart';
import '../../app.dart';

class AuthProvider with ChangeNotifier {
  bool _loading = false;
  bool get loading => _loading;
  bool _obsurceText = true;
  bool get obsurceText => _obsurceText;

  void logIn({
    required LogInModel formDoc,
    required BuildContext context,
  }) {
    _loading = true;
    meteor
        .loginWithPassword(formDoc.username!, formDoc.password!,
            delayOnLoginErrorSecond: 2)
        .then((result) async {
      _loading = false;
      AuthStorage().setUserLoginToken(userLoginToken: result);
      UserStorage().setUser(userDoc: meteor.userCurrentValue()!);
      UserStorage().setLanguage(lang: 'en');
      Navigator.of(context).pushReplacementNamed('/home');
      notifyListeners();
    }).catchError((err) {
      if (err is MeteorError) {
        Alert().show(
            message: err.message,
            alertIcon: CommonIcons.alertError,
            alertBackGroundColor: CommonColors.errorLight,
            context: context);
        _loading = false;
        notifyListeners();
      }
    });
    notifyListeners();
  }

  void toggleSuffixIcon() {
    _obsurceText = !_obsurceText;
    notifyListeners();
  }
}
