import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '/core/models/user/user_model.dart';
import '/core/utils/alert.dart';
import '/core/utils/constants.dart';
import '../../app.dart';
import '../storages/user_storage.dart';

class ProfileProvider with ChangeNotifier {
  UserModel _data = const UserModel(id: '', username: '');
  UserModel get data => _data;
  bool _isLoading = false;
  bool get isLoading => _isLoading;

  void getUserDocFromLocalStorage() async {
    String? imagePath;
    Map<String, dynamic> userDoc;
    _isLoading = true;
    userDoc = await UserStorage().getUser();
    imagePath = await UserStorage().getUserImagePath();
    _data = UserModel(
      id: userDoc['id'],
      username: userDoc['username'],
      fullName: userDoc['fullName'],
      code: userDoc['code'],
      imagePath: imagePath,
    );
    _isLoading = false;
    notifyListeners();
  }

  void updateProfile({required UserModel data, required BuildContext context}) {
    meteor.call('updateUser', args: [
      {'user': data.toJson()}
    ]).then((result) {
      Alert().show(
        message: 'Good job, your profile has been successfully updated.',
        alertIcon: CommonIcons.alertSuccess,
        alertBackGroundColor: CommonColors.successLight,
        context: context,
        duration: const Duration(seconds: 1),
      );
      //set user profile doc to local storage again
      UserStorage().setProfile(userDoc: data.toJson());
      getUserDocFromLocalStorage();
    }).catchError((error) {
      Alert().show(
          message: error.message,
          alertIcon: CommonIcons.alertError,
          alertBackGroundColor: CommonColors.errorLight,
          context: context);
    });
  }

  Future setUserImagePath({required ImageSource source}) async {
    final picker = ImagePicker();
    Map<String, dynamic> userDoc;
    userDoc = await UserStorage().getUser();
    final image = await picker.pickImage(source: source);
    if (image == null) return;
    _data = UserModel(
      id: userDoc['id'],
      username: userDoc['userName'],
      fullName: userDoc['fullName'],
      imagePath: image.path,
    );
    //set to local storage
    UserStorage().setUserImagePath(image.path);
    notifyListeners();
  }
}
