import 'package:dart_meteor/dart_meteor.dart';
// import 'package:dart_meteor_web/dart_meteor_web.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '/core/models/auth/reset_password_model.dart';
import '/core/utils/alert.dart';
import '/core/utils/constants.dart';
import '../../app.dart';

enum ResetPasswordBy { phone, email }

class ResetPasswordProvider with ChangeNotifier {
  String? _type;
  String? get type => _type;
  bool _loading = false;
  bool get loading => _loading;
  String? _userId = "";
  String? get userId => _userId;

  void setType({required ResetPasswordBy type}) {
    _type = describeEnum(type);
    notifyListeners();
  }

  void resetPassword({
    required ResetPasswordModel formDoc,
    required BuildContext context,
  }) {
    _loading = true;
    if (_type == 'email') {
      meteor.forgotPassword(formDoc.email!).then((result) {
        _loading = false;
        Alert().show(
            message:
                'Good job, please check your inbox and click in the received link to reset password',
            alertIcon: CommonIcons.alertSuccess,
            alertBackGroundColor: CommonColors.successLight,
            context: context);
        notifyListeners();
      }).catchError((err) {
        if (err is MeteorError) {
          _loading = false;
          Alert().show(
              message: err.message,
              alertIcon: CommonIcons.alertError,
              alertBackGroundColor: CommonColors.errorLight,
              context: context);
          notifyListeners();
        }
      });
    } else {
      meteor.call('findUserByTelephone', args: [
        {'telephone': formDoc.telephone}
      ]).then((result) {
        _loading = false;
        //go to verify screen
        _userId = result;
        Navigator.pushReplacementNamed(context, '/verify-account');
        notifyListeners();
      }).catchError((err) {
        if (err is MeteorError) {
          _loading = false;
          Alert().show(
              message: err.message,
              alertIcon: CommonIcons.alertError,
              alertBackGroundColor: CommonColors.errorLight,
              context: context);
          notifyListeners();
        }
      });
    }
    notifyListeners();
  }

  void resetNewPassword({
    required ResetPasswordModel formDoc,
    required BuildContext context,
  }) {
    _loading = true;
    meteor.call('changeAccountPassword', args: [
      {'userId': _userId, 'password': formDoc.password}
    ]).then((result) {
      _loading = false;
      Alert().show(
          message: 'Good job, your password has been changed',
          alertIcon: CommonIcons.alertSuccess,
          alertBackGroundColor: CommonColors.successLight,
          context: context);
      notifyListeners();
    }).catchError((err) {
      if (err is MeteorError) {
        _loading = false;
        Alert().show(
            message: err.message,
            alertIcon: CommonIcons.alertError,
            alertBackGroundColor: CommonColors.errorLight,
            context: context);
        notifyListeners();
      }
    });
    notifyListeners();
  }

  void clearState() {
    _userId = "";
    _type = "";
    notifyListeners();
  }
}
