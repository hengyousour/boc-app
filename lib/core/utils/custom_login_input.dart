import 'package:flutter/material.dart';
import '../utils/constants.dart';

// ignore: prefer_const_constructors
final kHintTextStyle = TextStyle(
  color: Colors.white54,
  fontFamily: CommonFonts.body,
);

// ignore: prefer_const_constructors
// final kLabelStyle =  TextStyle(
//   color: CommonColors.secondary,
//   fontWeight: FontWeight.bold,
//   fontFamily: CommonFonts.body,
// );

BoxDecoration kBoxDecorationStyle({required Color color}) {
  return BoxDecoration(
    color: color,
    borderRadius: BorderRadius.circular(5.0),
    // boxShadow: [
    //   BoxShadow(
    //     color: Colors.black12,
    //     blurRadius: 5.0,
    //     offset: Offset(0, 2),
    //   ),
    // ],
  );
}

BoxDecoration verifyPhoneTextFieldDecorationStyle({required Color color}) {
  return BoxDecoration(color: color, shape: BoxShape.circle
      // borderRadius: BorderRadius.circular(5.0),
      // boxShadow: [
      //   BoxShadow(
      //     color: Colors.black12,
      //     blurRadius: 5.0,
      //     offset: Offset(0, 2),
      //   ),
      // ],
      );
}
