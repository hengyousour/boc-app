// import 'dart:io';
// import 'dart:typed_data';
// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:screenshot/screenshot.dart';
// import 'package:share_plus/share_plus.dart';

// class ShareFeature {
//   static Future shareReport(
//       {@required ScreenshotController controller, String reportName}) async {
//     final Uint8List _image = await controller.capture();
//     if (_image == null) return;
//     //save & convert
//     final _directory = await getApplicationDocumentsDirectory();
//     final String fileName = DateTime.now().microsecondsSinceEpoch.toString();
//     final _imagePath = await File('${_directory.path}/$fileName.png').create();
//     final Uint8List pngBytes = _image.buffer.asUint8List();
//     await _imagePath.writeAsBytes(pngBytes);
//     //share
//     await Share.shareFiles([_imagePath.path]);
//   }
// }
