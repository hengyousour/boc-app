import 'package:flutter/material.dart';
import 'constants.dart';

class StaticTheme {
  static final theme = StaticTheme();
  final ThemeData _lightTheme = ThemeData(
    primaryColor: CommonColors.primaryLight,
    brightness: Brightness.light,
    appBarTheme: const AppBarTheme(
      elevation: 0.0,
      color: CommonColors.primaryLight,
      iconTheme: IconThemeData(color: CommonColors.secondaryLight),
      titleTextStyle: TextStyle(
        color: CommonColors.secondaryLight,
      ),
    ),
    //Note: For Date Time Picker theme
    colorScheme: const ColorScheme.light(
        primary: CommonColors.secondaryLight,
        secondary: CommonColors.primaryLight),
    iconTheme: const IconThemeData(color: CommonColors.secondaryLight),
    textTheme: const TextTheme(
        bodyMedium: TextStyle(
          fontFamily: CommonFonts.body,
          fontFamilyFallback: [CommonFonts.kantumruy],
        ),
        labelLarge: TextStyle(
          fontWeight: FontWeight.bold,
          fontFamily: CommonFonts.body,
          fontFamilyFallback: [CommonFonts.kantumruy],
        ),
        //text input
        titleMedium: TextStyle(
            fontFamily: CommonFonts.body,
            fontFamilyFallback: [CommonFonts.kantumruy],
            color: CommonColors.secondaryLight),
        titleLarge: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header6TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryLight),
        headlineSmall: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header5TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryLight),
        headlineMedium: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header4TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryLight),
        displaySmall: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header3TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryLight),
        displayMedium: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header5TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryLight),
        displayLarge: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header2TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryLight)),
    inputDecorationTheme: InputDecorationTheme(
      floatingLabelStyle:
          MaterialStateTextStyle.resolveWith((Set<MaterialState> states) {
        final Color color = states.contains(MaterialState.error)
            ? CommonColors.errorLight
            : CommonColors.secondaryLight;
        return TextStyle(color: color);
      }),
      prefixIconColor: MaterialStateColor.resolveWith(
          (Set<MaterialState> states) => (states.contains(MaterialState.focused)
              ? CommonColors.secondaryLight
              : Colors.grey)),
      suffixIconColor: MaterialStateColor.resolveWith(
          (Set<MaterialState> states) => (states.contains(MaterialState.focused)
              ? CommonColors.secondaryLight
              : Colors.grey)),
      errorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonColors.errorLight)),
      focusedErrorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonColors.errorLight)),
      errorStyle: const TextStyle(color: CommonColors.errorLight),
      enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonColors.secondaryLight)),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: CommonColors.secondaryLight),
      ),
    ),
    textSelectionTheme:
        const TextSelectionThemeData(cursorColor: CommonColors.secondaryLight),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: CommonColors.secondaryLight,
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
          foregroundColor: CommonColors.primaryLight,
          backgroundColor: CommonColors.secondaryLight,
          elevation: 0.0),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
          foregroundColor: CommonColors.secondaryLight,
          side: const BorderSide(color: CommonColors.secondaryLight),
          elevation: 0.0,
          padding: const EdgeInsets.all(20.0)),
    ),
  );

  final ThemeData _darkTheme = ThemeData(
    primaryColor: CommonColors.primaryDark,
    brightness: Brightness.dark,
    appBarTheme: const AppBarTheme(
      elevation: 0.0,
      color: CommonColors.primaryDark,
      iconTheme: IconThemeData(color: CommonColors.secondaryDark),
      titleTextStyle: TextStyle(
        color: CommonColors.secondaryDark,
      ),
    ),
    //Note: For Date Time Picker theme
    colorScheme: const ColorScheme.dark(
        primary: CommonColors.primaryDark,
        secondary: CommonColors.secondaryDark),
    iconTheme: const IconThemeData(color: CommonColors.secondaryDark),
    textTheme: const TextTheme(
        bodyMedium: TextStyle(
          fontFamily: CommonFonts.body,
          fontFamilyFallback: [CommonFonts.kantumruy],
        ),
        labelLarge: TextStyle(
          fontWeight: FontWeight.bold,
          fontFamily: CommonFonts.body,
          fontFamilyFallback: [CommonFonts.kantumruy],
        ),
        //text input
        titleMedium: TextStyle(
            fontFamily: CommonFonts.body,
            fontFamilyFallback: [CommonFonts.kantumruy],
            color: CommonColors.secondaryDark),
        titleLarge: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header6TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryDark),
        headlineSmall: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header5TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryDark),
        headlineMedium: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header4TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryDark),
        displaySmall: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header3TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryDark),
        displayMedium: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header5TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryDark),
        displayLarge: TextStyle(
            fontFamily: CommonFonts.header,
            fontSize: CommonTextSize.header2TextSize,
            fontFamilyFallback: [CommonFonts.kantumruy],
            fontWeight: FontWeight.bold,
            color: CommonColors.secondaryDark)),
    inputDecorationTheme: InputDecorationTheme(
      floatingLabelStyle:
          MaterialStateTextStyle.resolveWith((Set<MaterialState> states) {
        final Color color = states.contains(MaterialState.error)
            ? CommonColors.errorDark
            : CommonColors.secondaryDark;
        return TextStyle(color: color);
      }),
      prefixIconColor: MaterialStateColor.resolveWith(
          (Set<MaterialState> states) => (states.contains(MaterialState.focused)
              ? CommonColors.secondaryDark
              : Colors.grey)),
      suffixIconColor: MaterialStateColor.resolveWith(
          (Set<MaterialState> states) => (states.contains(MaterialState.focused)
              ? CommonColors.secondaryDark
              : Colors.grey)),
      errorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonColors.errorDark)),
      focusedErrorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonColors.errorDark)),
      errorStyle: const TextStyle(color: CommonColors.errorDark),
      enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: CommonColors.secondaryDark)),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: CommonColors.secondaryDark),
      ),
    ),
    textSelectionTheme:
        const TextSelectionThemeData(cursorColor: CommonColors.secondaryDark),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: CommonColors.secondaryDark,
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
          foregroundColor: CommonColors.primaryDark,
          backgroundColor: CommonColors.secondaryDark,
          elevation: 0.0),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
          foregroundColor: CommonColors.secondaryDark,
          elevation: 0.0,
          padding: const EdgeInsets.all(20.0)),
    ),
  );

  ThemeData phoneFieldTheme({required ThemeData theme}) => theme.copyWith(
        inputDecorationTheme: const InputDecorationTheme(
          border: OutlineInputBorder(),
          enabledBorder: OutlineInputBorder(),
        ),
      );

  ThemeData get lightTheme => _lightTheme;
  ThemeData get darkTheme => _darkTheme;
}
