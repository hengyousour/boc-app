import 'package:flutter_form_builder/flutter_form_builder.dart';

String getFormErrorText({required FormBuilderState formState}) {
  String errorText = '';

  for (int index = 0; index < formState.fields.length; index++) {
    String fieldName = formState.fields.keys.toList()[index];
    if (formState.fields[fieldName]!.hasError) {
      errorText = '[ $fieldName ] ${formState.fields[fieldName]!.errorText}';
      break;
    }
  }
  return errorText;
}
