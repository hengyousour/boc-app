import 'package:flutter/material.dart';
import 'package:unicons/unicons.dart';
import 'dart:math';

class CommonColors {
  //light
  static const primaryLight = Colors.white;
  static const secondaryLight = Colors.black;
  static const successLight = Color(0xFF34c759);
  static const infoLight = Color(0xFF1e96fc);
  static const warningLight = Color(0xFFff9500);
  static const errorLight = Color(0xFFff3b30);
  //dark
  static const primaryDark = Colors.black;
  static const secondaryDark = Colors.white;
  static const successDark = Color(0xFF34c759);
  static const infoDark = Color(0xFF1e96fc);
  static const warningDark = Color(0xFFff9500);
  static const errorDark = Color(0xFFff3b30);

  //randomcolor
  static final Random _random = Random();

  /// Returns a random color.
  static Color next() {
    return Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
  }
}

class CommonTextSize {
  static const header6TextSize = 18.0;
  static const header5TextSize = 24.0;
  static const header4TextSize = 26.0;
  static const header3TextSize = 28.0;
  static const header2TextSize = 30.0;
  static const header1TextSize = 32.0;
  // static const subTitle1TextSize = 20.0;
  static const bodyTextSizeWebAndDesktop = 16.0;
  static const bodyTextSizeTabletAndMobile = 14.0;
}

class CommonStyle {
  static const padding = 20.0;
  static const opacity = 0.50;
  static const hoverOpacity = 0.30;
  static const splashOpacity = 0.60;
}

class CommonFonts {
  static const header = 'Ubuntu';
  static const body = 'Bahnschrift';
  static const kantumruy = 'Kantumruy';
}

class CommonIcons {
  static const back = UniconsLine.angle_left_b;
  static const next = UniconsLine.angle_right_b;
  static const notification = UniconsLine.bell;
  static const ipAddress = UniconsLine.wifi_router;
  static const configIpAddress = UniconsLine.server_network;
  static const noConnection = UniconsLine.wifi_slash;
  static const edit = UniconsLine.pen;
  static const delete = UniconsLine.trash;
  static const alertSuccess = UniconsLine.grin;
  static const alertError = UniconsLine.meh;
  static const alertWaring = UniconsLine.exclamation_circle;
  static const alertInfo = UniconsLine.info_circle;
  static const camera = UniconsLine.camera;
  static const gallery = UniconsLine.image;
  static const menu = UniconsLine.subject;
  static const dashborad = UniconsLine.create_dashboard;
  static const user = UniconsLine.smile;
  static const fullName = UniconsLine.file_landscape_alt;
  static const password = UniconsLine.key_skeleton_alt;
  static const setting = UniconsLine.cog;
  static const qrCode = UniconsLine.qrcode_scan;
  static const logout = UniconsLine.left_arrow_from_left;
  static const darkMode = UniconsLine.moon;
  static const lightMode = UniconsLine.sun;
  static const hide = UniconsLine.eye_slash;
  static const show = UniconsLine.eye;
  static const gender = UniconsLine.venus;
  static const email = UniconsLine.at;
  static const location = UniconsLine.location_point;
  static const telephone = UniconsLine.phone;
  static const cross = UniconsLine.times;
  static const share = UniconsLine.share_alt;
  static const bluetooth = UniconsLine.bluetooth_b;
  static const print = UniconsLine.print;
}

class CommonBoxShadow {
  final defualt = [
    BoxShadow(
      offset: const Offset(0, 15),
      blurRadius: 32,
      color: Colors.black.withOpacity(0.12), // Black color with 12% opacity
    ),
  ];

  // final glowing = [
  //   BoxShadow(
  //     color: CommonColors.secondary.withOpacity(0.1),
  //     spreadRadius: 1,
  //     blurRadius: 16,
  //     offset: const Offset(8, 0),
  //   ),
  //   BoxShadow(
  //     color: CommonColors.primary.withOpacity(0.2),
  //     spreadRadius: 6,
  //     blurRadius: 16,
  //     offset: const Offset(-8, 0),
  //   ),
  //   BoxShadow(
  //     color: CommonColors.secondary.withOpacity(0.2),
  //     spreadRadius: 6,
  //     blurRadius: 16,
  //     offset: const Offset(8, 0),
  //   ),
  // ];
}
