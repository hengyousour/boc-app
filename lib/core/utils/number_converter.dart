class NumberConverter {
  static String removeZeroFromDecimal(double value) {
    if (value % 1 == 0) return value.toStringAsFixed(0).toString();
    return value.toString();
  }
}
