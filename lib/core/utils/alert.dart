import 'dart:math';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import '../widgets/responsive.dart';
import 'constants.dart';

class Alert {
  void show({
    required String? message,
    Color messageColor = Colors.black,
    required IconData alertIcon,
    Color alertIconColor = Colors.black,
    required Color alertBackGroundColor,
    double alertHeight = 80.0,
    double alertBorderRadius = 5.0,
    FlashPosition alertPosition = FlashPosition.top,
    required BuildContext context,
    Duration duration = const Duration(seconds: 5),
    flashStyle = FlashBehavior.floating,
  }) {
    showFlash(
      context: context,
      duration: duration,
      builder: (context, controller) {
        return Flash(
          controller: controller,
          behavior: flashStyle,
          // boxShadows: kElevationToShadow[4],
          borderRadius: BorderRadius.circular(alertBorderRadius),
          backgroundColor: alertBackGroundColor,
          position: alertPosition,
          margin: const EdgeInsets.all(5.0),
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          child: SizedBox(
            height: alertHeight,
            child: Stack(
              children: [
                Positioned(
                  top: -8,
                  left: -16,
                  child: ClipRRect(
                    child: Transform.rotate(
                      angle: 32 * pi / 180,
                      child: Icon(
                        alertIcon,
                        size: alertHeight * 1.5,
                        color: alertIconColor.withOpacity(0.1),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: FlashBar(
                    content: Text(
                      message!,
                      style: TextStyle(color: messageColor),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void showAlertDialog({
    Widget? title,
    EdgeInsetsGeometry titlePadding = const EdgeInsets.all(10.0),
    required Widget content,
    ShapeBorder? dialogShape,
    EdgeInsetsGeometry contentPadding = const EdgeInsets.all(0.0),
    required BuildContext context,
    bool barrierDismissible = true,
    Function? onDismiss,
    bool actionsEnable = true,
    String cancelBtnLabel = 'Cancel',
    Color cancelLabelColor = Colors.white,
    Color cancelBtnColor = Colors.grey,
    Color cancelBtnSplashColor = Colors.white,
    BorderRadiusGeometry? cancelBtnRadius,
    required Function onCancel,
    String agreeBtnLabel = 'Agree',
    Color agreeBtnColor = Colors.grey,
    Color agreeBtnSplashColor = Colors.white,
    Color agreeLabelColor = Colors.white,
    required Function onAgree,
    double buttonHeight = 48.0,
    EdgeInsetsGeometry buttonPadding = const EdgeInsets.all(10.0),
    BorderRadiusGeometry? agreeBtnRadius,
    String buttonMode = "normal",
    //mode : "normal" , "no-space", "above", "single"
  }) {
    showDialog(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (_) {
        return Responsive(
            mobile: Dialog(
              title: title,
              titlePadding: titlePadding,
              content: content,
              dialogShape: dialogShape,
              contentPadding: contentPadding,
              actionsEnable: actionsEnable,
              onCancel: onCancel,
              cancelBtnLabel: cancelBtnLabel,
              cancelLabelColor: cancelLabelColor,
              cancelBtnColor: cancelBtnColor,
              cancelBtnSplashColor: cancelBtnSplashColor,
              cancelBtnRadius: cancelBtnRadius,
              onAgree: onAgree,
              agreeBtnLabel: agreeBtnLabel,
              agreeLabelColor: agreeLabelColor,
              agreeBtnColor: agreeBtnColor,
              agreeBtnSplashColor: agreeBtnSplashColor,
              agreeBtnRadius: agreeBtnRadius,
              buttonHeight: buttonHeight,
              buttonPadding: buttonPadding,
              buttonMode: buttonMode,
            ),
            tablet: Row(
              children: [
                const Spacer(),
                Expanded(
                  flex: 4,
                  child: Dialog(
                    title: title,
                    titlePadding: titlePadding,
                    content: content,
                    dialogShape: dialogShape,
                    contentPadding: contentPadding,
                    actionsEnable: actionsEnable,
                    onCancel: onCancel,
                    cancelBtnLabel: cancelBtnLabel,
                    cancelLabelColor: cancelLabelColor,
                    cancelBtnColor: cancelBtnColor,
                    cancelBtnSplashColor: cancelBtnSplashColor,
                    cancelBtnRadius: cancelBtnRadius,
                    onAgree: onAgree,
                    agreeBtnLabel: agreeBtnLabel,
                    agreeLabelColor: agreeLabelColor,
                    agreeBtnColor: agreeBtnColor,
                    agreeBtnSplashColor: agreeBtnSplashColor,
                    agreeBtnRadius: agreeBtnRadius,
                    buttonHeight: buttonHeight,
                    buttonPadding: buttonPadding,
                    buttonMode: buttonMode,
                  ),
                ),
                const Spacer(),
              ],
            ),
            desktop: Row(
              children: [
                const Spacer(),
                Expanded(
                  child: Dialog(
                      title: title,
                      titlePadding: titlePadding,
                      content: content,
                      dialogShape: dialogShape,
                      contentPadding: contentPadding,
                      actionsEnable: actionsEnable,
                      onCancel: onCancel,
                      cancelBtnLabel: cancelBtnLabel,
                      cancelLabelColor: cancelLabelColor,
                      cancelBtnColor: cancelBtnColor,
                      cancelBtnSplashColor: cancelBtnSplashColor,
                      cancelBtnRadius: cancelBtnRadius,
                      onAgree: onAgree,
                      agreeBtnLabel: agreeBtnLabel,
                      agreeLabelColor: agreeLabelColor,
                      agreeBtnColor: agreeBtnColor,
                      agreeBtnSplashColor: agreeBtnSplashColor,
                      agreeBtnRadius: agreeBtnRadius,
                      buttonHeight: buttonHeight,
                      buttonPadding: buttonPadding,
                      buttonMode: buttonMode),
                ),
                const Spacer()
              ],
            ));
      },
    ).then((value) => onDismiss != null ? onDismiss() : null);
  }
}

class Dialog extends StatelessWidget {
  final Widget? title;
  final EdgeInsetsGeometry? titlePadding;
  final Widget content;
  final ShapeBorder? dialogShape;
  final EdgeInsetsGeometry? contentPadding;
  final bool? actionsEnable;
  final Function onCancel;
  final String? cancelBtnLabel;
  final Color? cancelLabelColor;
  final Color? cancelBtnColor;
  final Color? cancelBtnSplashColor;
  final BorderRadiusGeometry? cancelBtnRadius;
  final Function onAgree;
  final String? agreeBtnLabel;
  final Color? agreeLabelColor;
  final Color? agreeBtnColor;
  final Color? agreeBtnSplashColor;
  final BorderRadiusGeometry? agreeBtnRadius;
  final double? buttonHeight;
  final EdgeInsetsGeometry? buttonPadding;
  final String? buttonMode;
  // ignore: use_key_in_widget_constructors
  const Dialog({
    this.title,
    this.titlePadding,
    required this.content,
    this.dialogShape,
    this.contentPadding,
    this.actionsEnable,
    required this.onCancel,
    this.cancelBtnLabel,
    this.cancelLabelColor,
    this.cancelBtnColor,
    this.cancelBtnSplashColor,
    this.cancelBtnRadius,
    required this.onAgree,
    this.agreeBtnLabel,
    this.agreeLabelColor,
    this.agreeBtnColor,
    this.agreeBtnSplashColor,
    this.agreeBtnRadius,
    this.buttonHeight,
    this.buttonPadding,
    this.buttonMode,
  });
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return AlertDialog(
      title: (title != null) ? title : null,
      titlePadding: titlePadding,
      backgroundColor: theme.scaffoldBackgroundColor,
      contentPadding: contentPadding!,
      content: content,
      shape: dialogShape,
      buttonPadding: (buttonMode == "no-space")
          ? const EdgeInsets.all(0.0)
          : buttonPadding,
      actions: actionsEnable!
          ? [
              if (buttonMode == "normal" || buttonMode == "no-space")
                SizedBox(
                  width: MediaQuery.of(context).size.width * 1,
                  child: Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: buttonHeight,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                foregroundColor: cancelBtnSplashColor,
                                backgroundColor: cancelBtnColor,
                                padding: const EdgeInsets.all(0.0),
                                textStyle: const TextStyle(
                                    color: Colors.white, height: 1),
                                elevation: 0.0,
                                shadowColor: Colors.transparent,
                                shape: RoundedRectangleBorder(
                                  borderRadius: cancelBtnRadius ??
                                      BorderRadius.circular(5.0),
                                ),
                              ),
                              onPressed: onCancel as void Function()?,
                              child: Text(
                                cancelBtnLabel!,
                                style: TextStyle(
                                    color: cancelLabelColor,
                                    fontFamily: CommonFonts.body,
                                    fontSize: 16.0),
                              )),
                        ),
                      ),
                      if (buttonMode == "normal")
                        const SizedBox(
                          width: 10.0,
                        ),
                      Expanded(
                        child: SizedBox(
                          height: buttonHeight,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                foregroundColor: agreeBtnSplashColor,
                                backgroundColor: agreeBtnColor,
                                padding: const EdgeInsets.all(0.0),
                                textStyle: const TextStyle(color: Colors.white),
                                elevation: 0.0,
                                shadowColor: Colors.transparent,
                                shape: RoundedRectangleBorder(
                                  borderRadius: agreeBtnRadius ??
                                      BorderRadius.circular(5.0),
                                ),
                              ),
                              onPressed: onAgree as void Function()?,
                              child: Text(
                                agreeBtnLabel!,
                                style: TextStyle(
                                    color: agreeLabelColor,
                                    fontFamily: CommonFonts.body,
                                    fontSize: 16.0),
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              if (buttonMode == "above")
                SizedBox(
                  width: MediaQuery.of(context).size.width * 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(
                        height: buttonHeight,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              foregroundColor: agreeBtnSplashColor,
                              backgroundColor: agreeBtnColor,
                              padding: const EdgeInsets.all(0.0),
                              textStyle: const TextStyle(color: Colors.white),
                              elevation: 0.0,
                              shadowColor: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: agreeBtnRadius ??
                                    BorderRadius.circular(5.0),
                              ),
                            ),
                            onPressed: onAgree as void Function()?,
                            child: Text(
                              agreeBtnLabel!,
                              style: TextStyle(
                                  color: agreeLabelColor,
                                  fontFamily: CommonFonts.body,
                                  fontSize: 16.0),
                            )),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                        height: buttonHeight,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              foregroundColor: cancelBtnSplashColor,
                              backgroundColor: cancelBtnColor,
                              padding: const EdgeInsets.all(0.0),
                              textStyle: const TextStyle(color: Colors.white),
                              elevation: 0.0,
                              shadowColor: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: cancelBtnRadius ??
                                    BorderRadius.circular(5.0),
                              ),
                            ),
                            onPressed: onCancel as void Function()?,
                            child: Text(
                              cancelBtnLabel!,
                              style: TextStyle(
                                  color: cancelLabelColor,
                                  fontFamily: CommonFonts.body,
                                  fontSize: 16.0),
                            )),
                      ),
                    ],
                  ),
                ),
              if (buttonMode == "single")
                SizedBox(
                  width: MediaQuery.of(context).size.width * 1,
                  child: Expanded(
                    child: SizedBox(
                      height: buttonHeight,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            foregroundColor: agreeBtnSplashColor,
                            backgroundColor: agreeBtnColor,
                            padding: const EdgeInsets.all(0.0),
                            textStyle: const TextStyle(color: Colors.white),
                            elevation: 0.0,
                            shadowColor: Colors.transparent,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  agreeBtnRadius ?? BorderRadius.circular(5.0),
                            ),
                          ),
                          onPressed: onAgree as void Function()?,
                          child: Text(
                            agreeBtnLabel!,
                            style: TextStyle(
                                color: agreeLabelColor,
                                fontFamily: CommonFonts.body,
                                fontSize: 16.0),
                          )),
                    ),
                  ),
                )
            ]
          : [SizedBox(width: MediaQuery.of(context).size.width * 1)],
    );
  }
}
