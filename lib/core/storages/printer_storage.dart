import 'package:shared_preferences/shared_preferences.dart';

class PrinterStorage {
  // Create SharedPreferences
  final Future<SharedPreferences> _printerPrefs =
      SharedPreferences.getInstance();

  //Bluetooth Printer
  void setPrinter(
      {required String name,
      required String address,
      required int type}) async {
    SharedPreferences printerPrefs = await _printerPrefs;
    printerPrefs.setString('btPrinterName', name);
    printerPrefs.setString('btPrinterAddress', address);
    printerPrefs.setInt('btPrinterType', type);
  }

  Future<Map<String, dynamic>> getPrinter() async {
    SharedPreferences printerPrefs = await _printerPrefs;
    return {
      'name': printerPrefs.getString('btPrinterName'),
      'address': printerPrefs.getString('btPrinterAddress'),
      'type': printerPrefs.getInt('btPrinterType')
    };
  }

  //Wifi & Network Printer
  void setWNPrinter({required String address, required int port}) async {
    SharedPreferences printerPrefs = await _printerPrefs;
    printerPrefs.setString('printerAddress', address);
    printerPrefs.setInt('printerPort', port);
  }

  Future<Map<String, dynamic>> getWNPrinter() async {
    SharedPreferences printerPrefs = await _printerPrefs;
    return {
      'address': printerPrefs.getString('printerAddress'),
      'port': printerPrefs.getInt('printerPort'),
    };
  }

  void clearPrinter() async {
    SharedPreferences printerPrefs = await _printerPrefs;
    //Bluetooth Printer
    printerPrefs.remove('btPrinterName');
    printerPrefs.remove('btPrinterAddress');
    printerPrefs.remove('btPrinterType');
    //Wifi & Network Printer
    printerPrefs.remove('printerAddress');
    printerPrefs.remove('printerPort');
  }
}
