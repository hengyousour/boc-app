import '../../models/select-option/select_option_model.dart';

class StaticOptions {
  final List<SelectOptionModel> _languageOpts = [
    const SelectOptionModel(label: "KH", value: "km"),
    const SelectOptionModel(label: "US", value: "en"),
  ];

  List<SelectOptionModel> get languageOpts {
    return [..._languageOpts];
  }

  final List<SelectOptionModel> _genderOpts = [
    const SelectOptionModel(label: "Male", value: "M"),
    const SelectOptionModel(label: "Female", value: "F"),
  ];

  List<SelectOptionModel> get genderOpts {
    return [..._genderOpts];
  }
}
