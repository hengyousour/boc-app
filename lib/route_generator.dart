import 'package:flutter/material.dart';
import 'app.dart';
import 'core/screens/home.dart';
import 'core/screens/print_preview.dart';
import 'core/screens/profile.dart';
import 'core/screens/login.dart';
import 'core/screens/setting.dart';
import 'core/screens/dashboard.dart';
import 'core/screens/sign_up.dart';
import 'core/screens/verify_account.dart';
import 'core/screens/verified_account.dart';
import 'core/screens/reset_password.dart';
import 'core/screens/reset_password_by_phone.dart';
import 'core/screens/error_404.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/app':
        return MaterialPageRoute(builder: (_) => const App());
      case '/home':
        return MaterialPageRoute(builder: (_) => const Home());
      case '/login':
        return MaterialPageRoute(builder: (_) => const Login());
      case '/sign-up':
        return MaterialPageRoute(builder: (_) => const SignUp());
      case '/verify-account':
        return MaterialPageRoute(builder: (_) => const VerifyAccount());
      case '/verified-account':
        return MaterialPageRoute(builder: (_) => const VerifiedAccount());
      case '/reset-password':
        return MaterialPageRoute(builder: (_) => const ResetPassword());
      case '/reset-password-by-phone':
        return MaterialPageRoute(builder: (_) => const ResetPasswordByPhone());
      case '/dashboard':
        return MaterialPageRoute(builder: (_) => const Dashboard());
      case '/profile':
        return MaterialPageRoute(builder: (_) => const Profile());
      case '/setting':
        return MaterialPageRoute(builder: (_) => const Setting());
      case '/print-preview':
        return MaterialPageRoute(builder: (_) => const PrintPreview());
      default:
        return MaterialPageRoute(builder: (_) => const Error404());
    }
  }
}
