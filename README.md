# Boc App

> Boc app is a system management and it is using for store customers real time data and view reports at Battambang Optical Center.

# Features
- Log in,Log out
- Light & Dark Mode
- Dynamic Connection With MongoDB
- Customer Actions: Create, Edit, Delete, Show,Close, Cancel, Reactive, Filter (By Name & Date) with real time data & infinite scroll
- 3 Reports: can share to any social platforms & pagination (Customer, Customer Outstanding, Brief Information Of Referral Case)
# Notes

- In this app has only 2 roles ["admin-setting (Admin), staff"]
- Only user role "staff" can view Brief Information Of Referral Case Report
- Only user role "admin-setting" can Edit & Delete Customer
- User role "staff" can't Reactive on customer status Cancel  

# Lib File Structure
```
.
├── boc
│   ├── assets
│   │   └── json
│   │       ├── customers
│   │       │   └── data-404.json
│   │       └── dashboard
│   │           └── friendly-faces.json
│   ├── models
│   │   ├── customer
│   │   │   ├── customer_main_model.dart
│   │   │   ├── customer_main_model.g.dart
│   │   │   ├── customer_model.dart
│   │   │   ├── customer_model.g.dart
│   │   │   ├── eyes_detail_model.dart
│   │   │   ├── eyes_detail_model.g.dart
│   │   │   ├── refer_detail_model.dart
│   │   │   ├── refer_detail_model.g.dart
│   │   │   ├── va_ph_detail_model.dart
│   │   │   └── va_ph_detail_model.g.dart
│   │   ├── dashboard
│   │   │   └── dashboard_card_model.dart
│   │   ├── reports
│   │   │   ├── biorc
│   │   │   │   ├── biorc_data_content_report_model.dart
│   │   │   │   ├── biorc_data_content_report_model.g.dart
│   │   │   │   ├── biorc_data_report_model.dart
│   │   │   │   ├── biorc_data_report_model.g.dart
│   │   │   │   ├── biorc_form_report_model.dart
│   │   │   │   ├── biorc_form_report_model.g.dart
│   │   │   │   ├── biorc_report_model.dart
│   │   │   │   └── biorc_report_model.g.dart
│   │   │   ├── customer-list
│   │   │   │   ├── customer_list_form_report_model.dart
│   │   │   │   ├── customer_list_form_report_model.g.dart
│   │   │   │   ├── customer_list_report_model.dart
│   │   │   │   └── customer_list_report_model.g.dart
│   │   │   ├── customer-outstanding-list
│   │   │   │   ├── customer_outstanding_list_form_report_model.dart
│   │   │   │   └── customer_outstanding_list_form_report_model.g.dart
│   │   │   ├── report_filter_model.dart
│   │   │   └── report_filter_model.g.dart
│   │   ├── option_model.dart
│   │   └── option_model.g.dart
│   ├── providers
│   │   ├── reports
│   │   │   ├── biorc_report_provider.dart
│   │   │   ├── customer_list_report_provider.dart
│   │   │   ├── customer_outstanding_list_report_provider.dart
│   │   │   └── reports_provider.dart
│   │   ├── customer_overview_provider.dart
│   │   └── customer_provider.dart
│   ├── screens
│   │   ├── customer
│   │   │   ├── create_customer.dart
│   │   │   ├── customer.dart
│   │   │   └── edit_customer.dart
│   │   ├── dashboard
│   │   │   └── dashboard.dart
│   │   ├── reports
│   │   │   ├── biorc_report.dart
│   │   │   ├── customer_list_report.dart
│   │   │   ├── customer_outstanding_list_report.dart
│   │   │   └── reports.dart
│   │   └── drawer.dart
│   ├── utils
│   │   ├── boolean_model_converter.dart
│   │   ├── constants.dart
│   │   ├── custom_form_builder_style.dart
│   │   ├── date_model_converter.dart
│   │   ├── debouncer.dart
│   │   ├── icon_model_converter.dart
│   │   ├── static_options.dart
│   │   └── static_theme.dart
│   ├── widgets
│   │   ├── components
│   │   │   ├── alert_dialog_content.dart
│   │   │   ├── alert_dialog_title.dart
│   │   │   ├── custom_app_bar.dart
│   │   │   ├── report_content_header.dart
│   │   │   ├── report_filter.dart
│   │   │   ├── report_header.dart
│   │   │   ├── skeleton.dart
│   │   │   └── slide_action.dart
│   │   ├── customer
│   │   │   ├── create_customer_form.dart
│   │   │   ├── customer_skeleton_list.dart
│   │   │   └── edit_customer_form.dart
│   │   ├── dashboard
│   │   │   ├── custom_card.dart
│   │   │   └── overview_skeleton.dart
│   │   └── reports
│   │       ├── biorc_form.dart
│   │       ├── customer_list_form.dart
│   │       ├── customer_outstanding_list_form.dart
│   │       └── report_list.dart
│   └── route_generator.dart
├── core
│   ├── models
│   │   ├── auth
│   │   │   ├── login_model.dart
│   │   │   ├── login_model.g.dart
│   │   │   ├── reset_password_model.dart
│   │   │   ├── reset_password_model.g.dart
│   │   │   ├── sign_up_model.dart
│   │   │   ├── sign_up_model.g.dart
│   │   │   ├── user_profile_model.dart
│   │   │   └── user_profile_model.g.dart
│   │   ├── dashboard
│   │   │   └── dashboard_model.dart
│   │   ├── select-option
│   │   │   ├── select_option_model.dart
│   │   │   └── select_option_model.g.dart
│   │   ├── setting
│   │   │   ├── setting_model.dart
│   │   │   └── setting_model.g.dart
│   │   └── user
│   │       ├── email_model.dart
│   │       ├── email_model.g.dart
│   │       ├── user_model.dart
│   │       └── user_model.g.dart
│   ├── providers
│   │   ├── auth_provider.dart
│   │   ├── connection_provider.dart
│   │   ├── dashboard_provider.dart
│   │   ├── menu_provider.dart
│   │   ├── profile_provider.dart
│   │   ├── reset_password_provider.dart
│   │   ├── setting_provider.dart
│   │   ├── sign_up_provider.dart
│   │   ├── theme_provider.dart
│   │   └── verify_account_provider.dart
│   ├── screens
│   │   ├── connecting.dart
│   │   ├── dashboard.dart
│   │   ├── drawer.dart
│   │   ├── error_404.dart
│   │   ├── home.dart
│   │   ├── login.dart
│   │   ├── page_structure.dart
│   │   ├── print_preview.dart
│   │   ├── profile.dart
│   │   ├── reset_password_by_phone.dart
│   │   ├── reset_password.dart
│   │   ├── setting.dart
│   │   ├── sign_up.dart
│   │   ├── verified_account.dart
│   │   └── verify_account.dart
│   ├── services
│   │   ├── select_options
│   │   │   └── static_options.dart
│   │   └── login_services.dart
│   ├── storages
│   │   ├── auth_storage.dart
│   │   ├── connection_storage.dart
│   │   ├── printer_paper_size_storage.dart
│   │   ├── printer_storage.dart
│   │   └── user_storage.dart
│   ├── utils
│   │   ├── alert.dart
│   │   ├── block_ui.dart
│   │   ├── constants.dart
│   │   ├── core_convert_date_time.dart
│   │   ├── core_model_converter.dart
│   │   ├── custom_form_build_style.dart
│   │   ├── custom_login_input.dart
│   │   ├── device_orientation.dart
│   │   ├── get_form_error_text.dart
│   │   ├── number_converter.dart
│   │   ├── share_feature.dart
│   │   ├── static_theme.dart
│   │   └── string_converter.dart
│   └── widgets
│       ├── connection
│       │   └── connection_form.dart
│       ├── dashboard
│       │   ├── dashboard_grid_list.dart
│       │   └── dashboard_grid.dart
│       ├── login
│       │   └── login_form.dart
│       ├── profile
│       │   └── edit_profile.dart
│       ├── reset-new-password
│       │   └── reset_new_password_form.dart
│       ├── reset-password
│       │   └── reset_password_form.dart
│       ├── sign-up
│       │   └── sign_up_form.dart
│       ├── verify-account
│       │   ├── verified_account_form.dart
│       │   └── verify_account_form.dart
│       ├── app_bar_widget.dart
│       ├── block_ui_content.dart
│       ├── error_data.dart
│       ├── real_time_connection.dart
│       ├── responsive.dart
│       ├── share_widget.dart
│       ├── test_printing.dart
│       └── waiting_data.dart
├── app.dart
├── main.dart
└── route_generator.dart
```

# Changelog

## v.1.0.0

- init release

### Bug Fixes

- none


### Breaking Change

- none

# Usage

```bash
# Install dependencies
flutter pub get

# Build, watch for changes and debug the application
flutter run
```



